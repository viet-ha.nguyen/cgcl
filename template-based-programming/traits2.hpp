
#include <math.h>
#include <list>

// Class for 3D points, with float as coordinates
class Point_3d_float{
public:
  typedef float NT;
protected:
  float _x, _y, _z;
public:
  Point_3d_float(float xx, float yy, float zz) {_x=xx; _y=yy; _z=zz;}
  float x() const{return _x;}
  float y() const{return _y;}
  float z() const{return _z;}

  float distance(Point_3d_float& b){
    return sqrt( (_x-b.x())*(_x-b.x()) + (_y-b.y())*(_y-b.y()) + (_z-b.z())*(_z-b.z()) );
  }
};


class Point_3d_float_distance{
public:
  float operator()(Point_3d_float& a,  Point_3d_float& b){
    return a.distance(b);
  }
};


// The traits class defines the resources required by the class T_diameter_of_points
// Note that this traits classes uses a list of points, and not a vector (cf Traits1)
class Traits_for_point_3d_float{
public:

  typedef std::list<Point_3d_float> Container;
  typedef Point_3d_float_distance    Distance;

};


