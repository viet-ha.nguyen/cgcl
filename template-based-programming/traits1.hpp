#include <math.h>
#include <vector>

// Class for 2D points, with doubles as coordinates
class Point_2d_double{
public:
  typedef double NT;
protected:
  double _x, _y;
public:
  
  Point_2d_double(double xx, double yy) {_x=xx; _y=yy;}
  double x() const{return _x;}
  double y() const{return _y;}
  
  double distance(Point_2d_double& b){
    return sqrt( (_x-b.x())*(_x-b.x())+(_y-b.y())*(_y-b.y()) );
  }
};


class Point_2d_double_distance{
public:
  double operator()(Point_2d_double& a,  Point_2d_double& b){
    return a.distance(b);
  }
};


// The traits class defines the resources required by the class T_diameter_of_points
class Traits_for_point_2d_double{
public:

  typedef std::vector<Point_2d_double> Container;
  typedef Point_2d_double_distance     Distance;

};


