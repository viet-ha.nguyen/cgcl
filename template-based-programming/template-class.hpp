
/*
This is a generic class computing the diameter of a point set.

One plainly iterates over all pairs of points and computes the distance, recording the largest one.

The algorithm is oblivious to the point type,and the number type used for the coordinates, as the points could be in 2D, 3D, dD space.
In fact, one just needs an operator computing the distance between 2 points.

Note also that the input is a container, namely an object over which one can iterates. The container could be a list, a vector, etc.

These generic parameters are passed to the class by means of the so-called Traits class, from which we collect the types thanks to 'typedef typenames'.

*/

template <class Traits>
class T_diameter_of_points{
public:
  typedef typename Traits::Container Container; // container of points
  typedef typename Container::value_type Point; // the value_type of the container refers to points
  typedef typename Point::NT NT;                // the point class defines a number type, which can be anything (float, double, int, multi-precision, etc)
  
  typedef typename Traits::Distance Distance;// the functor used to compute the distance between 2 points

public:
  NT compute_diameter(Container& cont){
    typename Container::iterator it1b = cont.begin(), it1e = cont.end(), it2b = it1b, it2e = it1e;

    NT diam = 0;
    for(; it1b != it1e; it1b++){
      Point p1 = *it1b;
      for(; it2b != it2e; it2b++){
      Point p2 = *it2b;

      // For the pair of points (p1, p2): compute the distance using the operator() of the functor Distance()
      NT d = Distance()(p1, p2);
      if (d>diam) diam=d; // possibly update the diameter
      }
    }
    return diam;
  }


};
