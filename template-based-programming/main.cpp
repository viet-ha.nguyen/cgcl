/*

This example instantiates a generic class computing the diameter of a point set (max distance between pairs
of points), in 2 settings: for point on  a line, and for points in 3D.

In each case, the types are packed in a so-called traits class, which
is passed as parameter of the main algorithm.

*/

#include <iostream>

#include "template-class.hpp"
#include "traits1.hpp"
#include "traits2.hpp"

typedef T_diameter_of_points<Traits_for_point_2d_double> Diameter_2d_double;
typedef T_diameter_of_points<Traits_for_point_3d_float> Diameter_3d_float;

int main(){

  // Let us compute the diameter of a path in 2D
  Diameter_2d_double diam2;
  
  std::vector<Point_2d_double> C2;
  C2.push_back( Point_2d_double(0,0) );
  C2.push_back( Point_2d_double(5,0) );
  C2.push_back( Point_2d_double(10,0) );
  
  std::cout << "Diameter of path in 2D:" <<  diam2.compute_diameter(C2) << "\n";

  // And that of a square in 3D
  Diameter_3d_float diam3;

  std::list<Point_3d_float> C3;
  C3.push_back( Point_3d_float(0,0,0) );
  C3.push_back( Point_3d_float(1,0,0) );
  C3.push_back( Point_3d_float(1,1,0) );
  C3.push_back( Point_3d_float(0,1,0) );
  
  std::cout << "Diameter of square in 3D:" <<  diam3.compute_diameter(C3) << "\n";

return 1;
}
