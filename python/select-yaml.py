#! /usr/bin/python3

import argparse
from os import path
import subprocess
import glob
import yaml
from yaml import CLoader as Loader
from itertools import product

inter = "threshold_min"

# --oin directory prefix of yaml files
# --oout directory prefix of selected yaml files
# --N_main select all .yaml for a given number_of_main_vertices
# --mininter select all .yaml where the mininter is at most a given value

parser = argparse.ArgumentParser(description='My parser')
parser.add_argument('--oin',
                    dest="oin", help="Output directory prefix of yaml files")
parser.add_argument('--oout',
                    dest="oout", help="Output directory prefix of selected yaml files")
parser.add_argument("--N_main", type=int,
                    dest="N_main", help="Number of main vertices to select")
parser.add_argument("--mininter", type=float,
                    dest="mininter", help="Minimum intersection to select")
parser.add_argument("--repeat", type=int,
                    dest="repeat", help="Number of repeats required")
parser.add_argument("--gv", action="store_true", dest="gv",
                    default=False, help="To copy also the .gv")

# parse command line arguments
options = parser.parse_args()

# do some basic checks
if(path.exists(options.oout)):
    print("folder", options.oout, "already exists. Exit.")
    exit()
if(options.N_main==None):
    print("missing option --N_main. Exit.")
    exit()
if(options.mininter==None):
    print("missing option --mininter. Exit.")
    exit()
if(options.repeat==None):
    print("missing option --repeat. Exit.")
    exit()

# create destination folder (oout)
print("# create directory " + options.oout)
subprocess.run(["mkdir", options.oout])

# copy param.yaml and modify repeat
print("# copy param.yaml and modify repeat to " + str(options.repeat))
with open(options.oin + "/param.yaml") as f:
    param = yaml.safe_load(f)
param['repeat'] = options.repeat
with open(options.oout + "/param.yaml", "w") as f:
    yaml.dump(param, f)

# glob all yaml files and select
print("# copy the selected yaml files with N_main=" + str(options.N_main) +", mininter<=" + str(options.mininter))
for filenameoutput in glob.glob(options.oin + "/*.gv.output.yaml"):
    # read yaml file
    with open(filenameoutput, 'r') as f:
        data = yaml.load(f, Loader=Loader)
    # check conditions
    if (data['number_of_main_vertices'] == options.N_main) and (data[inter] <= options.mininter):
        # copy file
        newfilenameoutput = options.oout + filenameoutput[len(options.oin):]
        subprocess.run(["cp", filenameoutput, newfilenameoutput])
        if options.gv:
            subprocess.run(["cp", filenameoutput[:-12], newfilenameoutput[:-12]]) # copy gv


#
# check repeats and rename files
#

# return the gv filename from output directory prefix and spec
def get_filename_norepeat(o, N, num_c, radius, tm):
    return o\
        + "/data_N" + format(N, '04d')\
        + "_num_c" + str(num_c)\
        + "_radius" + str(radius)\
        + "_threshold" + str(tm)\
        + "_repeat"

# get param
N_list = param['N']
num_c_list = param['num_c']
radii_list = param['radii']
if('thresholds' in param.keys()):
    tm_list = param['thresholds']
elif('multis' in param.keys()):
    tm_list = param['multis']

# manage each exp_spec
print("# rename files and check repeats")
for atuple in list(product(N_list, num_c_list, radii_list, tm_list)):
    N = atuple[0]
    num_c = atuple[1]
    radius = atuple[2]
    tm = atuple[3]
    # glob such files and check that we have enough
    counter = 0
    for filenameoutput in glob.glob(get_filename_norepeat(options.oout, N, num_c, radius, tm) + "*.gv.output.yaml"):
        # rename or remove file
        if counter < options.repeat:
            # rename
            newfilenameoutput = get_filename_norepeat(options.oout, N, num_c, radius, tm) + format(counter, '02d') + ".gv.output.yaml2"
            subprocess.run(["mv", filenameoutput, newfilenameoutput])
            print(filenameoutput)
            if options.gv:
                subprocess.run(["mv", filenameoutput[:-12], newfilenameoutput[:-13]+"2"]) # move gv
            counter = counter+1
        else:
            # remove
            subprocess.run(["rm", filenameoutput])
            if options.gv:
                subprocess.run(["rm", filenameoutput[:-12]]) # remove gv
    # check that we have enough files
    if counter < options.repeat:
        print("Not enough files (" + str(counter) + ") for exp_spec: N=" + str(N)\
                                          + " num_c=" + str(num_c)\
                                          + " radius=" + str(radius)\
                                          + " threshold_or_multi=" + str(tm))
# rename yaml2 -> yaml
for filenameoutput in glob.glob(options.oout + "/data*.gv.output.yaml2"):
    subprocess.run(["mv", filenameoutput, filenameoutput[:-1]])
    if options.gv:
        subprocess.run(["mv", filenameoutput[:-13]+"2", filenameoutput[:-13]]) # move gv
