class Process_manager:

    def __init__(self, options, matlab_run_options):
        self.options = options
        self.matlab_run_options = matlab_run_options

    def run(self):

        processes = set()
        commands = []
        for matlab_r_option in self.matlab_run_options:
            cmd = "matlab -r \"%s\" -nodisplay &" % matlab_r_option
            if self.options.exe_type == "dry" or self.options.exe_type == "shell" :
                print("About to execute ",cmd)
                commands.append(cmd)
                continue
            ## INTERESTING PART
            else:
                # execute in parallel using self.options.max_processes processes
                processes.add(subprocess.Popen(["matlab", "-r", matlab_r_option, "-nodisplay"]))
                if len(processes) >= self.options.max_processes:
                    # in theory:wait for the completion of a child process
                    os.wait()
                    # A <- A\B
                    # poll() returns None if the process has not completed
                    # that is, remove from the set processes those which terminated
                    processes.difference_update([p for p in processes if p.poll()is not None])
        # wait for the remaining processes to terminate
        exit_codes = [p.wait() for p in processes]
