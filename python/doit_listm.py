#! /usr/bin/python3

import math
import pdb
import re  # regular expressions
import sys  # misc system
import os
from os import path
import subprocess
import multiprocessing as mp
import argparse
from itertools import product
import statistics
import glob
import yaml
from yaml import CLoader as Loader
#import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from mpl_toolkits import mplot3d

plt.rcParams.update({'figure.max_open_warning': 0})

ALGONAME="CHOCO_ENUM"

#
# CAUTION
# This version will work only for --multis_min and --multis_mean
#

# USAGE
# Generate the data (gen)
# ./doit.py --repeat 10 -N 10 20 --num_c 50 60 --radii 1  --threshold 10 -o myxp -g
# Call solver (run)
# ./doit.py -o myxp --algo BACKTRACK CUT_SET_CYCLE -r
# Compute the statistics (stats)
# ./doit.py -o myxp -s

#i########################################################################
# INPUT: specification for tests
#i########################################################################

class Experiment_spec:

    def __init__(self, atuple):
        self.options = options
        self.N = atuple[0]
        #self.nshift = atuple[1]
        self.num_c = atuple[1]
        self.radius = atuple[2]  # -1
        #self.min_dist = atuple[3]  # -1
        #self.threshold_or_multi = atuple[3]
        #self.min_intersections = atuple[4]
        #self.multi = atuple[5]

# Take the cartesian product of all options passed

class Experiments_spec:

    def __init__(self, options):
        self.options = options
        self.specs = []
        # product is the Cartesian product of experimental specifications
        tuples = list(product(self.options.num_vertices, self.options.num_colors, self.options.radii))
        for tuple in tuples:
            self.specs.append(Experiment_spec(tuple))

    def get_experiments_specs(self):
        return self.specs

#i########################################################################
# OUTPUT
#i########################################################################

# note: it is very important that the names are identical to options,
# because we call the constructor of Experiments_spec with it
class Param_spec:
    def __init__(self, data):
        self.num_vertices = data['N']
        self.num_colors = data['num_c']
        self.radii = data['radii']
        #self.min_distances = data['min_distance']
        self.multis_min = data['multis_min']
        self.multis_mean = data['multis_mean']
        self.howtocomputethreshold = "multis"
        self.thresholds = None
        self.minormean = 4
        self.repeat = data['repeat']

class Instance_spec:
    # One output.yaml
    def __init__(self, data, algos):
        # datas on the Conflict Graph
        self.running_time_build_cg = data['running_time_build_cg']
        self.num_c = data['num_c']
        self.radius = data['radius']
        #self.min_dist_threshold = data['min_dist_threshold']
        self.threshold = data['threshold']
        if('multi' in data.keys()):
            self.multi = data['multi']
        else:
            self.multi = None
        self.number_of_main_vertices = data['number_of_main_vertices']
        self.number_of_main_edges = data['number_of_main_edges']
        self.average_number_of_color_per_vertices = data['average_number_of_color_per_vertices']
        self.average_number_of_conflict_edges_per_edge = data['average_number_of_conflict_edges_per_edge']
        #### algo ####
        self.algo_time = {}
        self.algo_result = {}
        for a in algos:
            self.algo_time[a] = data[a+'_running_time']
            self.algo_result[a] = data[a+'_coloring_result']

        #self.algos_coherent = data['algos_coherent']
        # TODO: check that all the coloring_found are coherent

        self.min_intersections = data['min_intersections']
        self.min_intersections_edge = data['min_intersections_edge']
        self.min_intersections_face = data['min_intersections_face']
        self.min_intersections_star = data['min_intersections_star']
        self.mean_intersections_edge = data['mean_intersections_edge']
        self.mean_intersections_face = data['mean_intersections_face']
        self.mean_intersections_star = data['mean_intersections_star']


class Instances_spec:
    # All the repeats for one exp_spec
    def __init__(self, instances_repeats, algos):
        # instances_repeats is a list of Instance_spec
        # -------- init --------
        self.running_time_build_cg = []
        self.num_c = []
        self.radius = []
        #self.min_dist_threshold = []
        self.threshold = []
        self.multi = []
        self.number_of_main_vertices = []
        self.number_of_main_edges = []
        self.average_number_of_color_per_vertices = []
        self.average_number_of_conflict_edges_per_edge = []
        #### algo ####
        self.algo_time = {}
        self.algo_result = {}
        for a in algos:
            self.algo_time[a] = []
            self.algo_result[a] = []
        #self.algos_coherent = []
        #######################
        self.min_intersections = []
        # min setting
        self.min_intersections_edge = []
        self.min_intersections_face = []
        self.min_intersections_star = []
        # mean setting
        self.mean_intersections_edge = []
        self.mean_intersections_face = []
        self.mean_intersections_star = []
        # -------- fill --------
        for instance_spec in instances_repeats:
            self.running_time_build_cg.append(instance_spec.running_time_build_cg)
            self.num_c.append(instance_spec.num_c)
            self.radius.append(instance_spec.radius)
            #self.min_dist_threshold.append(instance_spec.min_dist_threshold)
            self.threshold.append(instance_spec.threshold)
            if instance_spec.multi is not None:
                self.multi.append(instance_spec.multi)
            self.number_of_main_vertices.append(instance_spec.number_of_main_vertices)
            self.number_of_main_edges.append(instance_spec.number_of_main_edges)
            self.average_number_of_color_per_vertices.append(instance_spec.average_number_of_color_per_vertices)
            self.average_number_of_conflict_edges_per_edge.append(instance_spec.average_number_of_conflict_edges_per_edge)
            for a in algos:
                self.algo_time[a].append(instance_spec.algo_time[a])
                self.algo_result[a].append(instance_spec.algo_result[a])
            self.min_intersections.append(instance_spec.min_intersections)
            self.min_intersections_edge.append(instance_spec.min_intersections_edge)
            self.min_intersections_face.append(instance_spec.min_intersections_face)
            self.min_intersections_star.append(instance_spec.min_intersections_star)
            self.mean_intersections_edge.append(instance_spec.mean_intersections_edge)
            self.mean_intersections_face.append(instance_spec.mean_intersections_face)
            self.mean_intersections_star.append(instance_spec.mean_intersections_star)

#i########################################################################
# FILE NAMES
#i########################################################################
# return the cin filename from output directory prefix and N
def get_cin_filename(oprefix, N, r):
    return oprefix + "/data_N" + format(N, '04d') + "_repeat" + format(r, '02d') + ".cin"

# return the gv filename from output directory prefix and spec
def get_gv_filename(oprefix, spec, m, r):
    return oprefix\
        + "/data_N" + format(spec.N, '04d')\
        + "_num_c" + str(spec.num_c)\
        + "_radius" + str(spec.radius)\
        + "_threshold" + str(m)\
        + "_repeat" + format(r, '02d')\
        + ".gv"

# return 
def get_output_filename(filenamegv):
    return filenamegv+".output.yaml"

#i########################################################################
# Main class
# Prepare Experiments_spec
#i########################################################################

class Project:

    def __init__(self, options):
        self.options = options
        # Generate exp_specs only for gen (otherwise not enough arguments)
        if self.options.gen:
            # Check that --thresholds xor --multis is set
            print("!! caution: args check removed")
            self.minormean = 4
            # Create the specifications for the experiments
            experiments = Experiments_spec(self.options)
            self.exp_specs = experiments.get_experiments_specs()
        if self.options.stats:
            # read parap.yaml
            f = open(self.options.oprefix+"/param.yaml", 'r')
            param = yaml.load(f, Loader=Loader)
            f.close()
            self.param = param
            self.param_spec = Param_spec(param)
            experiments = Experiments_spec(self.param_spec) # trick: self.param_scpec in stats is the same as self.options in gen
            self.exp_specs = experiments.get_experiments_specs()
            # color map viridis with specific colors for 0 and 1
            viridis = cm.get_cmap('viridis', 256)
            viridis_zo = viridis(np.linspace(0, 1, 256))
            pink = np.array([248/256, 24/256, 148/256, 1])
            orange = np.array([255/256, 153/256, 18/256, 1])
            viridis_zo[0] = pink
            viridis_zo[-1]= orange
            self.cmap_pcolor_zo = ListedColormap(viridis_zo)

    ##################################
    # gen
    ##################################

    # Save the parameters (created in gen, used in stats)
    def save_param(self):
        # Create the folder of data
        for mm in ["-cin","-min","-mean"]:
            if path.exists(self.options.oprefix+mm):
                print("folder", self.options.oprefix+mm, "already exists. Exit.")
                exit()
        if not self.options.dry:
            for mm in ["-cin","-min","-mean"]:
                subprocess.run(["mkdir", self.options.oprefix+mm])
        # Create param.txt
        if not self.options.dry:
            for mm in ["-min","-mean"]:
                fparam = open(self.options.oprefix+mm+"/param.yaml", "w")
                fparam.write("N: "+str(self.options.num_vertices)+"\n")
                fparam.write("num_c: "+str(self.options.num_colors)+"\n")    
                fparam.write("radii: "+str(self.options.radii)+"\n")
                if(mm == "-min"):
                    fparam.write("multis: "+str(self.options.multis_min)+"\n")
                elif(mm == "-mean"):
                    fparam.write("multis: "+str(self.options.multis_mean)+"\n")
                fparam.write("minormean: "+str(self.options.minormean)+"\n")
                fparam.write("repeat: "+str(self.options.repeat)+"\n")
                fparam.close()
    
    # Generate the data
    def gen_cin(self):
        print("# Generate the data .cin")
        # create repeat graphs for each value of N
        for N in self.options.num_vertices:
            for r in range(self.options.repeat):
                filenamecin = get_cin_filename(self.options.oprefix+"-cin", N, r)
                gen2D = "../example-voronoi/data/generate_random_Point_2.o"
                if not path.exists(gen2D):
                    print("please run 'make' in ../example-voronoi/data/. Exit.")
                    exit()
                cmd = [gen2D,
                 "--N", str(N),
                 "--range", str(100 * math.sqrt(N)),
                 "--cin", filenamecin]
                print("About to execute", cmd)
                if not self.options.dry:
                    subprocess.run(cmd)

    # build a ConflictGraph and export to .gv
    def build_all_CG(self):
        print("# Build all ConflictGraph (in parallel)")
        pool = mp.Pool(mp.cpu_count())
        # run the experiments
        for exp_spec in self.exp_specs:
            for r in range(self.options.repeat):
                self.build_one_CG(exp_spec, r, pool)
        # wait the executions to be finished
        pool.close()
        pool.join()

    def build_one_CG(self, exp_spec, r, pool):
        # create gv files according to multis_min and multis_mean lists
        filenamecin = get_cin_filename(self.options.oprefix+"-cin", exp_spec.N, r)

        cmd = ["../example-voronoi_listm/test-for-voronoi-polygons",
            "--cin", filenamecin,
            "--num_c", str(exp_spec.num_c),
            "--radius", str(exp_spec.radius),
            "--min_intersections", str(exp_spec.radius),]
        cmd = cmd + ["--multis_min"] + [str(m) for m in self.options.multis_min]
        cmd = cmd + ["--multis_mean"] + [str(m) for m in self.options.multis_mean]
        gv_min = [get_gv_filename(self.options.oprefix+"-min", exp_spec, m, r) for m in self.options.multis_min]
        cmd = cmd + ["--gv_min"] + gv_min
        gv_mean = [get_gv_filename(self.options.oprefix+"-mean", exp_spec, m, r) for m in self.options.multis_mean]
        cmd = cmd + ["--gv_mean"] + gv_mean
        cmd = cmd + ["--output_min"] + [get_output_filename(gv) for gv in gv_min]
        cmd = cmd + ["--output_mean"] + [get_output_filename(gv) for gv in gv_mean]

        print("About to execute", cmd)
        if not self.options.dry:
            #subprocess.run(cmd)
            pool.apply_async(subprocess.run, args=(cmd,))

    ##################################
    # run
    ##################################

    # Run one experiment
    def run_one_case(self, filenamegv, algo):
        filenameoutput = get_output_filename(filenamegv)
        cmd = ["./solver_output.o",
               "--gv", filenamegv,
               "--output", filenameoutput,
               "--algo", algo]
        print("About to execute", cmd)
        if not self.options.dry:
            subprocess.run(cmd)
    
    # Run algos sequentially on one gv file
    def run_algos(self, filenamegv):
        # run each algo
        for algo in self.options.algos:
            self.run_one_case(filenamegv, algo)

    # Run all experiments
    def run_cases(self):
        print("# Run the program (in parallel)")
        pool = mp.Pool(mp.cpu_count())
        # add list of algorithms to param
        if not self.options.dry:
            fparam = open(self.options.oprefix+"/param.yaml", "a+")
            fparam.write("algos: "+str(self.options.algos)+"\n")
            fparam.close()
        # run the experiments on each gv files (in parallel)
        for filenamegv in glob.glob(self.options.oprefix + "/*.gv"):
            pool.apply_async(self.run_algos, args=(filenamegv,))
        # wait the executions to be finished
        pool.close()
        pool.join()

    ##################################
    # stats
    ##################################

    # Parse output files
    def parse_output_files(self):
        self.instances = []
        for exp_spec in self.exp_specs:
            instances_repeats = []
            for r in range(self.param['repeat']):
                filenamegv = get_gv_filename(self.options.oprefix,exp_spec,r)
                filenameoutput = get_output_filename(filenamegv)
                # get data for one repeat
                f = open(filenameoutput, 'r')
                data = yaml.load(f, Loader=Loader)
                # TODO: f.close() ?
                instances_repeats.append(Instance_spec(data,self.param.get('algos')))
            # compile the datas from the repeats
            self.instances.append(Instances_spec(instances_repeats,self.param.get('algos')))
    
    ##################################
    # check algos coherent
    ##################################
    def algos_coherent(self, algosrun):
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            result = i_spec.coloring_result
            for algo in algosrun:
                k = algo+"_coloring_sesult"
                if(result!= i_spec.k):
                    print ("not coherent !")

    # Run the experiments
    def run(self):
        if self.options.gen:
            self.save_param()
            self.gen_cin()
            self.build_all_CG()
        elif self.options.run:
            self.run_cases()


#i########################################################################
# options
#i########################################################################

# ATT: % is the help="" must be double ie %%
parser = argparse.ArgumentParser(description='My parser')
parser.add_argument("-d", action="store_true", dest="dry",
                    default=False, help="Print commands but do not run")
parser.add_argument('-o', "--oprefix", dest="oprefix",
                    default=".", help="Output directory prefix")
parser.add_argument("-P", nargs='?', type=int, default=1, const=1,
                    dest="max_processes", help="Parallelism: number of processes")

parser.add_argument("-N",  nargs='*', type=int,
                    dest="num_vertices", help="Number of vertices in the DT")
parser.add_argument("--num_c", nargs='*', type=int, dest="num_colors", help="Number of colors for a VD point")
#parser.add_argument("--nshift",  nargs='*', type=int, dest="num_vertex_copies", help="Number of copies for a VD point")
parser.add_argument("--radii",  nargs='*', type=float,
                    dest="radii", help="Radii for random VD points")
#parser.add_argument("--min_distances",  nargs='*', type=float, dest="min_distances", help="Minimum distance of two VD points")
parser.add_argument("--thresholds",  nargs='*', type=float, dest="thresholds",
                    help="Threshold for the intersection area of two polygons in conflict")
parser.add_argument("--multis_min",  nargs='*', type=float, dest="multis_min",
                    help="Multiply by min intersection to get threshold for the intersection area of two polygons in conflict")
parser.add_argument("--multis_mean",  nargs='*', type=float, dest="multis_mean",
                    help="Multiply by mean intersection to get threshold for the intersection area of two polygons in conflict")
parser.add_argument("--minormean", nargs='?', type=int, dest="minormean",
                    default=0, help="Compute threshold multis with min 1 or mean 2 or min+mean 3 (in two separate folders in this last case)")
parser.add_argument("--repeat",  nargs='?', type=int, dest="repeat", help="Number of repetitions for each experiment")
parser.add_argument("--algos",  nargs='*', type=str,
                    dest="algos", help="Algorithm for solving the problem")

parser.add_argument("-g", action="store_true", dest="gen",
                    default=False, help="Generate the data")
parser.add_argument("-r", action="store_true", dest="run",
                    default=False, help="Run the program")
parser.add_argument("-s", action="store_true", dest="stats",
                    default=False, help="Compute the statistics")

#i########################################################################
# Main
#i########################################################################

options = parser.parse_args()
proj = Project(options)
proj.run()
