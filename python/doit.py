#! /usr/bin/python3

import math
import pdb
import re  # regular expressions
import sys  # misc system
import os
from os import path
import subprocess
import multiprocessing as mp
import argparse
from itertools import product
import statistics
import glob
import yaml
from yaml import CLoader as Loader
#import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from mpl_toolkits import mplot3d

plt.rcParams.update({'figure.max_open_warning': 0})

ALGONAME="CHOCO_ENUM"
#multis_min = [1.01, 1.5, 2, 3, 4, 5, 6, 7, 8, 9, 10]
#multis_mean = [0.1, 0.4, 0.7, 1, 1.3, 1.6, 1.9, 2.2, 3, 5]

# USAGE
# Generate the data (gen)
# ./doit.py --repeat 10 -N 10 20 --num_c 50 60 --radii 1  --threshold 10 -o myxp -g
# Call solver (run)
# ./doit.py -o myxp --algo BACKTRACK CUT_SET_CYCLE -r
# Compute the statistics (stats)
# ./doit.py -o myxp -s

#i########################################################################
# INPUT: specification for tests
#i########################################################################

class Experiment_spec:

    def __init__(self, atuple):
        self.options = options
        self.N = atuple[0]
        #self.nshift = atuple[1]
        self.num_c = atuple[1]
        self.radius = atuple[2]  # -1
        #self.min_dist = atuple[3]  # -1
        self.threshold_or_multi = atuple[3]
        #self.min_intersections = atuple[4]
        #self.multi = atuple[5]

# Take the cartesian product of all options passed

class Experiments_spec:

    def __init__(self, options):
        self.options = options
        self.specs = []
        # product is the Cartesian product of experimental specifications
        if(self.options.thresholds is not None):
            tuples = list(product(self.options.num_vertices, self.options.num_colors,
                              self.options.radii,  self.options.thresholds))
        elif(self.options.multis is not None):
            tuples = list(product(self.options.num_vertices, self.options.num_colors,
                              self.options.radii,  self.options.multis))
        for tuple in tuples:
            self.specs.append(Experiment_spec(tuple))

    def get_experiments_specs(self):
        return self.specs

#i########################################################################
# OUTPUT
#i########################################################################

# note: it is very important that the names are identical to options,
# because we call the constructor of Experiments_spec with it
class Param_spec:
    def __init__(self, data):
        self.num_vertices = data['N']
        self.num_colors = data['num_c']
        self.radii = data['radii']
        #self.min_distances = data['min_distance']
        if('thresholds' in data.keys()):
            self.howtocomputethreshold = "thresholds"
            self.thresholds = data['thresholds']
            self.multis = None
        elif('multis' in data.keys()):
            self.howtocomputethreshold = "multis"
            self.multis = data['multis']
            self.thresholds = None
        self.minormean = data['minormean']
        self.repeat = data['repeat']

class Instance_spec:
    # One output.yaml
    def __init__(self, data, algos):
        # datas on the Conflict Graph
        self.running_time_build_cg = data['running_time_build_cg']
        self.num_c = data['num_c']
        self.radius = data['radius']
        #self.min_dist_threshold = data['min_dist_threshold']
        self.threshold = data['threshold']
        if('multi' in data.keys()):
            self.multi = data['multi']
        else:
            self.multi = None
        self.number_of_main_vertices = data['number_of_main_vertices']
        self.number_of_main_edges = data['number_of_main_edges']
        self.average_number_of_color_per_vertices = data['average_number_of_color_per_vertices']
        self.average_number_of_conflict_edges_per_edge = data['average_number_of_conflict_edges_per_edge']
        #### algo ####
        self.algo_time = {}
        self.algo_result = {}
        for a in algos:
            self.algo_time[a] = data[a+'_running_time']
            self.algo_result[a] = data[a+'_coloring_result']

        #self.algos_coherent = data['algos_coherent']
        # TODO: check that all the coloring_found are coherent

        self.min_intersections = data['min_intersections']
        self.min_intersections_edge = data['min_intersections_edge']
        self.min_intersections_face = data['min_intersections_face']
        self.min_intersections_star = data['min_intersections_star']
        self.mean_intersections_edge = data['mean_intersections_edge']
        self.mean_intersections_face = data['mean_intersections_face']
        self.mean_intersections_star = data['mean_intersections_star']


class Instances_spec:
    # All the repeats for one exp_spec
    def __init__(self, instances_repeats, algos):
        # instances_repeats is a list of Instance_spec
        # -------- init --------
        self.running_time_build_cg = []
        self.num_c = []
        self.radius = []
        #self.min_dist_threshold = []
        self.threshold = []
        self.multi = []
        self.number_of_main_vertices = []
        self.number_of_main_edges = []
        self.average_number_of_color_per_vertices = []
        self.average_number_of_conflict_edges_per_edge = []
        #### algo ####
        self.algo_time = {}
        self.algo_result = {}
        for a in algos:
            self.algo_time[a] = []
            self.algo_result[a] = []
        #self.algos_coherent = []
        #######################
        self.min_intersections = []
        # min setting
        self.min_intersections_edge = []
        self.min_intersections_face = []
        self.min_intersections_star = []
        # mean setting
        self.mean_intersections_edge = []
        self.mean_intersections_face = []
        self.mean_intersections_star = []
        # -------- fill --------
        for instance_spec in instances_repeats:
            self.running_time_build_cg.append(instance_spec.running_time_build_cg)
            self.num_c.append(instance_spec.num_c)
            self.radius.append(instance_spec.radius)
            #self.min_dist_threshold.append(instance_spec.min_dist_threshold)
            self.threshold.append(instance_spec.threshold)
            if instance_spec.multi is not None:
                self.multi.append(instance_spec.multi)
            self.number_of_main_vertices.append(instance_spec.number_of_main_vertices)
            self.number_of_main_edges.append(instance_spec.number_of_main_edges)
            self.average_number_of_color_per_vertices.append(instance_spec.average_number_of_color_per_vertices)
            self.average_number_of_conflict_edges_per_edge.append(instance_spec.average_number_of_conflict_edges_per_edge)
            for a in algos:
                self.algo_time[a].append(instance_spec.algo_time[a])
                self.algo_result[a].append(instance_spec.algo_result[a])
            self.min_intersections.append(instance_spec.min_intersections)
            self.min_intersections_edge.append(instance_spec.min_intersections_edge)
            self.min_intersections_face.append(instance_spec.min_intersections_face)
            self.min_intersections_star.append(instance_spec.min_intersections_star)
            self.mean_intersections_edge.append(instance_spec.mean_intersections_edge)
            self.mean_intersections_face.append(instance_spec.mean_intersections_face)
            self.mean_intersections_star.append(instance_spec.mean_intersections_star)

#i########################################################################
# FILE NAMES
#i########################################################################
# return the cin filename from output directory prefix and N
def get_cin_filename(oprefix, N, r):
    return oprefix + "/data_N" + format(N, '04d') + "_repeat" + format(r, '02d') + ".cin"

# return the gv filename from output directory prefix and spec
def get_gv_filename(oprefix, spec, r):
    return oprefix\
        + "/data_N" + format(spec.N, '04d')\
        + "_num_c" + str(spec.num_c)\
        + "_radius" + str(spec.radius)\
        + "_threshold" + str(spec.threshold_or_multi)\
        + "_repeat" + format(r, '02d')\
        + ".gv"

# return 
def get_output_filename(filenamegv):
    return filenamegv+".output.yaml"

#i########################################################################
# Main class
# Prepare Experiments_spec
#i########################################################################

class Project:

    def __init__(self, options):
        self.options = options
        # Generate exp_specs only for gen (otherwise not enough arguments)
        if self.options.gen:
            # Check that --thresholds xor --multis is set
            if((self.options.thresholds is None and self.options.multis is None) or (self.options.thresholds is not None and self.options.multis is not None)):
                print("Exactly one of --thresholds or --multis must be given. Exit.")
                exit()
            if(self.options.thresholds is not None):
                self.howtocomputethreshold = "thresholds" # use list of thresholds
            elif(self.options.multis is not None):
                self.howtocomputethreshold = "multis" # use list of multis
                # Check that --minormean is set
                if(self.options.minormean == 0):
                    print("With option --multis you need to give --minormean x with x in {1,2,3}. Exit.")
                    exit()
            # in any case, set minormean (1=min 2=mean 0=nothing)
            self.minormean = self.options.minormean
            # Create the specifications for the experiments
            experiments = Experiments_spec(self.options)
            self.exp_specs = experiments.get_experiments_specs()
        if self.options.stats:
            # read parap.yaml
            f = open(self.options.oprefix+"/param.yaml", 'r')
            param = yaml.load(f, Loader=Loader)
            f.close()
            self.param = param
            self.param_spec = Param_spec(param)
            experiments = Experiments_spec(self.param_spec) # trick: self.param_scpec in stats is the same as self.options in gen
            self.exp_specs = experiments.get_experiments_specs()
            # color map viridis with specific colors for 0 and 1
            viridis = cm.get_cmap('viridis', 256)
            viridis_zo = viridis(np.linspace(0, 1, 256))
            pink = np.array([248/256, 24/256, 148/256, 1])
            orange = np.array([255/256, 153/256, 18/256, 1])
            viridis_zo[0] = pink
            viridis_zo[-1]= orange
            self.cmap_pcolor_zo = ListedColormap(viridis_zo)

    ##################################
    # gen
    ##################################

    # Save the parameters (created in gen, used in stats)
    def save_param(self):
        # Create the folder of data
        if self.minormean != 3:
            if path.exists(self.options.oprefix):
                print("folder", self.options.oprefix, "already exists. Exit.")
                exit()
            if not self.options.dry:
                subprocess.run(["mkdir", self.options.oprefix])
        else:
            for mm in ["-cin","-min","-mean"]:
                if path.exists(self.options.oprefix+mm):
                    print("folder", self.options.oprefix+mm, "already exists. Exit.")
                    exit()
            if not self.options.dry:
                for mm in ["-cin","-min","-mean"]:
                    subprocess.run(["mkdir", self.options.oprefix+mm])
        # Create param.txt
        if not self.options.dry:
            ######### minormean=3 hack 1/2
            if self.minormean==3:
                self.options.oprefix = self.options.oprefix+"-min"
            #########
            fparam = open(self.options.oprefix+"/param.yaml", "w")
            fparam.write("N: "+str(self.options.num_vertices)+"\n")
            fparam.write("num_c: "+str(self.options.num_colors)+"\n")    
            #fparam.write("nshift: "+str(self.options.num_vertex_copies)+"\n")
            fparam.write("radii: "+str(self.options.radii)+"\n")
            #fparam.write("min_distance: "+str(self.options.min_distances)+"\n")
            if(self.howtocomputethreshold == "thresholds"):
                fparam.write("thresholds: "+str(self.options.thresholds)+"\n")
            elif(self.howtocomputethreshold == "multis"):
                fparam.write("multis: "+str(self.options.multis)+"\n")
            #fparam.write("min_intersections: "+str(self.options.min_intersections)+"\n")
            fparam.write("minormean: "+str(self.options.minormean)+"\n")
            fparam.write("repeat: "+str(self.options.repeat)+"\n")
            fparam.close()
            ######## minormean=3 hack 2/2
            if self.minormean==3:
                self.options.oprefix = self.options.oprefix[:-4]
                subprocess.run(["cp",self.options.oprefix+"-min/param.yaml",self.options.oprefix+"-mean/param.yaml"])
            ########
    
    # Generate the data
    def gen_cin(self):
        print("# Generate the data .cin")
        # create repeat graphs for each value of N
        ######### minormean=3 hack 1/2
        if self.minormean==3:
            self.options.oprefix = self.options.oprefix+"-cin"
        #########
        for N in self.options.num_vertices:
            for r in range(self.options.repeat):
                filenamecin = get_cin_filename(self.options.oprefix, N, r)
                gen2D = "../example-voronoi/data/generate_random_Point_2.o"
                if not path.exists(gen2D):
                    print("please run 'make' in ../example-voronoi/data/. Exit.")
                    exit()
                cmd = [gen2D,
                 "--N", str(N),
                 "--range", str(100 * math.sqrt(N)),
                 "--cin", filenamecin]
                print("About to execute", cmd)
                if not self.options.dry:
                    subprocess.run(cmd)
        ######### minormean=3 hack 2/2
        if self.minormean==3:
            self.options.oprefix = self.options.oprefix[:-4]
        #########

    # build a ConflictGraph and export to .gv
    def build_all_CG(self):
        print("# Build all ConflictGraph (in parallel)")
        pool = mp.Pool(mp.cpu_count())
        # run the experiments
        for exp_spec in self.exp_specs:
            for r in range(self.options.repeat):
                self.build_one_CG(exp_spec, r, pool)
        # wait the executions to be finished
        pool.close()
        pool.join()

    def build_one_CG(self, exp_spec, r, pool):
        if(self.howtocomputethreshold == "thresholds"):
            filenamecin = get_cin_filename(self.options.oprefix, exp_spec.N, r)
            filenamegv = get_gv_filename(self.options.oprefix, exp_spec, r)
            filenameoutput = get_output_filename(filenamegv)
            cmd = ["../example-voronoi/test-for-voronoi-polygons",
                "--cin", filenamecin,
                "--num_c", str(exp_spec.num_c), #"--nshift", str(exp_spec.nshift),
                "--radius", str(exp_spec.radius),
                #"--min_dist_threshold", str(exp_spec.min_dist),
                "--threshold", str(exp_spec.threshold_or_multi),
                "--gv", filenamegv,
                "--output", filenameoutput,
                "--min_intersections", str(exp_spec.radius)]
        elif(self.howtocomputethreshold == "multis"):
            if self.minormean != 3:
                # create one gv file
                filenamecin = get_cin_filename(self.options.oprefix, exp_spec.N, r)
                filenamegv = get_gv_filename(self.options.oprefix, exp_spec, r)
                filenameoutput = get_output_filename(filenamegv)
                cmd = ["../example-voronoi/test-for-voronoi-polygons",
                    "--cin", filenamecin,
                    "--num_c", str(exp_spec.num_c), #"--nshift", str(exp_spec.nshift),
                    "--radius", str(exp_spec.radius),
                    #"--min_dist_threshold", str(exp_spec.min_dist),
                    "--multi", str(exp_spec.threshold_or_multi),
                    "--minormean", str(self.minormean),
                    "--gv", filenamegv,
                    "--output", filenameoutput,
                    "--min_intersections", str(exp_spec.radius)]
            else:
                # create two gv files
                filenamecin = get_cin_filename(self.options.oprefix+"-cin", exp_spec.N, r)
                filenamegv = get_gv_filename(self.options.oprefix+"-min", exp_spec, r)
                filenameoutput = get_output_filename(filenamegv)
                filenamegv2 = get_gv_filename(self.options.oprefix+"-mean", exp_spec, r)
                filenameoutput2 = get_output_filename(filenamegv2)
                cmd = ["../example-voronoi/test-for-voronoi-polygons",
                    "--cin", filenamecin,
                    "--num_c", str(exp_spec.num_c),
                    "--radius", str(exp_spec.radius),
                    "--multi", str(exp_spec.threshold_or_multi),
                    "--minormean", str(self.minormean), # =3
                    "--gv", filenamegv,
                    "--output", filenameoutput,
                    "--gv2", filenamegv2,
                    "--output2", filenameoutput2,
                    "--min_intersections", str(exp_spec.radius)]
        print("About to execute", cmd)
        if not self.options.dry:
            #subprocess.run(cmd)
            pool.apply_async(subprocess.run, args=(cmd,))

    ##################################
    # run
    ##################################

    # Run one experiment
    def run_one_case(self, filenamegv, algo):
        filenameoutput = get_output_filename(filenamegv)
        cmd = ["./solver_output.o",
               "--gv", filenamegv,
               "--output", filenameoutput,
               "--algo", algo]
        print("About to execute", cmd)
        if not self.options.dry:
            subprocess.run(cmd)
    
    # Run algos sequentially on one gv file
    def run_algos(self, filenamegv):
        # run each algo
        for algo in self.options.algos:
            self.run_one_case(filenamegv, algo)

    # Run all experiments
    def run_cases(self):
        print("# Run the program (in parallel)")
        pool = mp.Pool(mp.cpu_count())
        # add list of algorithms to param
        if not self.options.dry:
            fparam = open(self.options.oprefix+"/param.yaml", "a+")
            fparam.write("algos: "+str(self.options.algos)+"\n")
            fparam.close()
        # run the experiments on each gv files (in parallel)
        for filenamegv in glob.glob(self.options.oprefix + "/*.gv"):
            pool.apply_async(self.run_algos, args=(filenamegv,))
        # wait the executions to be finished
        pool.close()
        pool.join()

    ##################################
    # stats
    ##################################

    # Parse output files
    def parse_output_files(self):
        self.instances = []
        for exp_spec in self.exp_specs:
            instances_repeats = []
            for r in range(self.param['repeat']):
                filenamegv = get_gv_filename(self.options.oprefix,exp_spec,r)
                filenameoutput = get_output_filename(filenamegv)
                # get data for one repeat
                f = open(filenameoutput, 'r')
                data = yaml.load(f, Loader=Loader)
                # TODO: f.close() ?
                instances_repeats.append(Instance_spec(data,self.param.get('algos')))
            # compile the datas from the repeats
            self.instances.append(Instances_spec(instances_repeats,self.param.get('algos')))
    
    ##################################
    # check algos coherent
    ##################################
    def algos_coherent(self, algosrun):
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            result = i_spec.coloring_result
            for algo in algosrun:
                k = algo+"_coloring_sesult"
                if(result!= i_spec.k):
                    print ("not coherent !")

    ##################################
    # PLOTS (begin)
    ##################################

    # Plot: running_time_build_cg = f(N)
    def plot_running_time_build_cg_f_N(self):
        plot = {} # dict N -> list of running_time_build_cg
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get(exp_spec.N)==None:
                plot[exp_spec.N] = [] # create empty list
            plot[exp_spec.N].append(statistics.mean(i_spec.running_time_build_cg)) # mean of the repeats
        x = list(plot.keys())
        y = list(map(statistics.mean,list(plot.values()))) # mean of other parameters
        fig, ax = plt.subplots()
        ax.plot(x,y)
        ax.set_xlabel('N')
        ax.set_ylabel('running_time_build_cg')
        ax.set_title('Running time to build ConflictGraph wrt number of points in DT')
        fig.savefig(self.options.oprefix+"/plot_running_time_build_cg_f_N.pdf")
        #plt.show()


    # Plot: percent of pos/neg = f(N,num_c) 
    def plot_percent_of_pos_f_N_numc(self):
        plot = {} # dict N, num_c -> list of coloring_found
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get((exp_spec.num_c,exp_spec.N))==None:
                plot[(exp_spec.num_c,exp_spec.N)] = [] # create empty list
            coloring_found = []
            for c in range(len(i_spec.algo_result[ALGONAME])):
                if i_spec.algo_result[ALGONAME][c]>0:
                    coloring_found.append(1)
                else:
                    coloring_found.append(0)
            plot[(exp_spec.num_c,exp_spec.N)].append(statistics.mean(coloring_found)) # mean of the repeats
        A = []
        for N in self.param_spec.num_vertices:
            a = []
            for c in self.param_spec.num_colors:
                a.append(statistics.mean(plot[(c,N)]))
            A.append(a)
        X = self.param_spec.num_colors
        Y = self.param_spec.num_vertices
        # plot with pcolor
        fig, ax = plt.subplots()
        c = ax.pcolor(A, vmin=0, vmax=1, cmap=self.cmap_pcolor_zo)
        ax.set_xticks([i+0.5 for i in range(len(X))])
        ax.set_xticklabels(X)
        ax.set_yticks([i+0.5 for i in range(len(Y))])
        ax.set_yticklabels(Y)
        ax.set_xlabel('num_c')
        ax.set_ylabel('N')
        ax.set_title('Percent of pos wrt number of colors and N')
        fig.colorbar(c, ax=ax)
        fig.savefig(self.options.oprefix+"/plot_percent_of_pos_f_N_numc.pdf")
        #plt.show()

    # Plot: number_of_main_vertices = f(N)
    def plot_number_of_main_vertices_f_N(self):
        plot = {} # dict N -> list of numberof main vertices
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get(exp_spec.N)==None:
                plot[exp_spec.N] = [] # create empty list
            plot[exp_spec.N].append(statistics.mean(i_spec.number_of_main_vertices)) # mean of the repeats
        x = list(plot.keys())
        y = list(map(statistics.mean,list(plot.values()))) # mean of other parameters
        fig, ax = plt.subplots()
        ax.plot(x,y)
        ax.set_xlabel('N')
        ax.set_ylabel('number_of_main_vertices')
        ax.set_title('Number of main vertices wrt number of points in DT')
        fig.savefig(self.options.oprefix+"/plot_number_of_main_vertices_f_N.pdf")
        #plt.show()

    # Plot: running_time_build_cg = f(N,num_c)
    def plot_running_time_build_cg_f_N_numc(self):
        X = self.param_spec.num_vertices
        Y = self.param_spec.num_colors
        X, Y = np.meshgrid(X, Y)
        plot = {} # dict N,num_c -> list of running_time_build_cg
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get((exp_spec.N,exp_spec.num_c))==None:
                plot[(exp_spec.N,exp_spec.num_c)] = [] # create empty list
            plot[(exp_spec.N,exp_spec.num_c)].append(statistics.mean(i_spec.running_time_build_cg)) # mean of the repeats
        Z=[]
        for c in self.param_spec.num_colors:
            z = []
            for N in self.param_spec.num_vertices:
                z.append(statistics.mean(plot[(N,c)]))
            Z.append(z)
        Z = np.array(Z)
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        ax.plot_wireframe(X, Y, Z)
        #ax.contour3D(X, Y, Z, 50, cmap='binary')
        ax.set_zlabel('running_time_build_cg')
        ax.set_xlabel('N')
        ax.set_ylabel('num_colors')
        ax.set_title('Running time to build CG wrt N and number of colors')
        fig.savefig(self.options.oprefix+"/plot_running_time_build_cg_f_N_numc.pdf")
        #plt.show()
    
    # Plot: frac[sol/all possibilities] wrt (N, numc)
    def plot_frac_sol_f_N_numc(self):
        plot = {} # dict N,num_c -> list of coloring_result
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get((exp_spec.N,exp_spec.num_c))==None:
                plot[(exp_spec.N,exp_spec.num_c)] = [] # create empty list
            plot[(exp_spec.N,exp_spec.num_c)].append(statistics.mean(np.divide(i_spec.algo_result[ALGONAME], np.power(i_spec.num_c,i_spec.number_of_main_vertices, dtype = float)))) # mean of the repeats
        A = []
        for c in self.param_spec.num_colors:
            a = []
            for N in self.param_spec.num_vertices:
                a.append(statistics.mean(plot[(N,c)]))
            A.append(a)
        X = self.param_spec.num_vertices
        Y = self.param_spec.num_colors
        fig, ax = plt.subplots() # clear

        c = ax.pcolor(A, norm=LogNorm(vmin=1/1000000, vmax=1),cmap=self.cmap_pcolor_zo) #(A, vmin=0, vmax=1)
        fig.colorbar(c, ax=ax)

        the_table = plt.table(cellText=[[round(f,10) for f in a] for a in A],
                      rowLabels=Y,
                      colLabels=X,
                      #loc='bottom',
                      bbox=[0, -0.8, 1, 0.5]
                      )
        plt.subplots_adjust(bottom=0.5)
        ax.set_xticks([i+0.5 for i in range(len(X))])
        ax.set_xticklabels(X)
        ax.set_yticks([i+0.5 for i in range(len(Y))])
        ax.set_yticklabels(Y)
        ax.set_xlabel('N')
        ax.set_ylabel('num_c')
        ax.set_title('Fraction of solutions/possibilites wrt N and number of colors')
        fig.savefig(self.options.oprefix+"/plot_frac_sol_f_N_numc.pdf")
        #plt.show()


    #########################################
     # plot with thresholds
    #########################################

    # Plot: percent of pos/neg = f(N,threshold)
    def plot_percent_of_pos_f_threshold_N(self):
        plot = {} # dict N,threshold -> list of coloring_found
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get((exp_spec.N,exp_spec.threshold_or_multi))==None:
                plot[(exp_spec.N,exp_spec.threshold_or_multi)] = [] # create empty list
            coloring_found = []
            for c in range(len(i_spec.algo_result[ALGONAME])):
                if i_spec.algo_result[ALGONAME][c]>0:
                    coloring_found.append(1)
                else:
                    coloring_found.append(0)
            plot[(exp_spec.N,exp_spec.threshold_or_multi)].append(statistics.mean(coloring_found)) # mean of the repeats
        A = []
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            for threshold in self.param_spec.thresholds:
                a = []
                for N in self.param_spec.num_vertices:
                    a.append(statistics.mean(plot[(N,threshold)]))
                A.append(a)
        elif(self.param_spec.howtocomputethreshold == "multis"):
            for threshold in self.param_spec.multis:
                a = []
                for N in self.param_spec.num_vertices:
                    a.append(statistics.mean(plot[(N,threshold)]))
                A.append(a)
        X = self.param_spec.num_vertices
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            Y = self.param_spec.thresholds
        elif(self.param_spec.howtocomputethreshold == "multis"):
            Y = self.param_spec.multis

        fig, ax = plt.subplots()
        c = ax.pcolor(A, vmin=0, vmax=1, cmap=self.cmap_pcolor_zo)
        ax.set_xticks([i+0.5 for i in range(len(X))])
        ax.set_xticklabels(X)
        ax.set_yticks([i+0.5 for i in range(len(Y))])
        ax.set_yticklabels(Y)
        ax.set_xlabel('N')
        ax.set_ylabel(self.param_spec.howtocomputethreshold)

        ax.set_title('Percent of pos wrt number of points in DT and '+self.param_spec.howtocomputethreshold)
        fig.colorbar(c, ax=ax)
        fig.savefig(self.options.oprefix+"/plot_percent_of_pos_f_"+self.param_spec.howtocomputethreshold+"_N.pdf")
        #plt.show()

    # Plot: percent of pos/neg = f(num_c,threshold) (should fix a value N)
    def plot_percent_of_pos_f_threshold_numc(self):
        plot = {} # dict num_c,threshold -> list of coloring_found
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get((exp_spec.num_c,exp_spec.threshold_or_multi))==None:
                plot[(exp_spec.num_c,exp_spec.threshold_or_multi)] = [] # create empty list
            coloring_found = []
            for c in range(len(i_spec.algo_result[ALGONAME])):
                if i_spec.algo_result[ALGONAME][c]>0:
                    coloring_found.append(1)
                else:
                    coloring_found.append(0)
            plot[(exp_spec.num_c,exp_spec.threshold_or_multi)].append(statistics.mean(coloring_found)) # mean of the repeats
        A = []
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            for threshold in self.param_spec.thresholds:
                a = []
                for c in self.param_spec.num_colors:
                    a.append(statistics.mean(plot[(c,threshold)]))
                A.append(a)
        elif(self.param_spec.howtocomputethreshold == "multis"):
            for threshold in self.param_spec.multis:
                a = []
                for c in self.param_spec.num_colors:
                    a.append(statistics.mean(plot[(c,threshold)]))
                A.append(a)
        X = self.param_spec.num_colors
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            Y = self.param_spec.thresholds
        elif(self.param_spec.howtocomputethreshold == "multis"):
            Y = self.param_spec.multis

        #plt.subplots() # clear
        fig, ax = plt.subplots() # clear

        c = ax.pcolor(A, norm=LogNorm(vmin=1/1000000, vmax=1),cmap=self.cmap_pcolor_zo) #(A, vmin=0, vmax=1)
        fig.colorbar(c, ax=ax)

        the_table = plt.table(cellText=[[round(f,10) for f in a] for a in A],
                      rowLabels=Y,
                      colLabels=X,
                      #loc='bottom',
                      bbox=[0, -0.8, 1, 0.5]
                      )
        plt.subplots_adjust(bottom=0.5)

        #fig, ax = plt.subplots()
        #c = ax.pcolor(A, vmin=0, vmax=1, cmap=self.cmap_pcolor_zo)
        ax.set_xticks([i+0.5 for i in range(len(X))])
        ax.set_xticklabels(X)
        ax.set_yticks([i+0.5 for i in range(len(Y))])
        ax.set_yticklabels(Y)
        #ax.set_xlabel('num_c')
        #ax.set_ylabel(self.param_spec.howtocomputethreshold)

        #ax.set_title('Percent of pos wrt number of colors and'+ self.param_spec.howtocomputethreshold)
        #fig.colorbar(c, ax=ax)
        fig.savefig(self.options.oprefix+"/plot_percent_of_pos_f_"+ self.param_spec.howtocomputethreshold +"_numc.pdf")
        #plt.show()

    # Plot: percent of pos/neg = f(radius,threshold) (should fix values N, num_c)
    def plot_percent_of_pos_f_threshold_radius(self):
        plot = {} # dict radius,threshold -> list of coloring_found
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get((exp_spec.radius,exp_spec.threshold_or_multi))==None:
                plot[(exp_spec.radius,exp_spec.threshold_or_multi)] = [] # create empty list
            coloring_found = []
            for c in range(len(i_spec.algo_result[ALGONAME])):
                if i_spec.algo_result[ALGONAME][c]>0:
                    coloring_found.append(1)
                else:
                    coloring_found.append(0)
            plot[(exp_spec.radius,exp_spec.threshold_or_multi)].append(statistics.mean(coloring_found)) # mean of the repeats
        A = []
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            for threshold in self.param_spec.thresholds:
                a = []
                for r in self.param_spec.radii:
                    a.append(statistics.mean(plot[(r,threshold)]))
                A.append(a)
        elif(self.param_spec.howtocomputethreshold == "multis"):
            for threshold in self.param_spec.multis:
                a = []
                for r in self.param_spec.radii:
                    a.append(statistics.mean(plot[(r,threshold)]))
                A.append(a)
        X = self.param_spec.radii
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            Y = self.param_spec.thresholds
        elif(self.param_spec.howtocomputethreshold == "multis"):
            Y = self.param_spec.multis

        fig, ax = plt.subplots()
        c = ax.pcolor(A, vmin=0, vmax=1, cmap=self.cmap_pcolor_zo)
        ax.set_xticks([i+0.5 for i in range(len(X))])
        ax.set_xticklabels(X)
        ax.set_yticks([i+0.5 for i in range(len(Y))])
        ax.set_yticklabels(Y)
        ax.set_xlabel('radius')
        ax.set_ylabel(self.param_spec.howtocomputethreshold)
        ax.set_title('Percent of pos wrt radius and'+ self.param_spec.howtocomputethreshold)
        fig.colorbar(c, ax=ax)
        fig.savefig(self.options.oprefix+"/plot_percent_of_pos_f_"+ self.param_spec.howtocomputethreshold +"_radius.pdf")
        #plt.show()

    

    # Plot: percent of conflict edges = f(N,threshold)
    def plot_percent_of_conflict_edges_f_threshold_N(self):
        plot = {} # dict N,threshold -> list of percent of number of conflict edges per edge
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            num_c = statistics.mean(i_spec.num_c)
            if plot.get((exp_spec.N,exp_spec.threshold_or_multi))==None:
                plot[(exp_spec.N,exp_spec.threshold_or_multi)] = [] # create empty list
                value = statistics.mean(i_spec.average_number_of_conflict_edges_per_edge)/(num_c**2)
            plot[(exp_spec.N,exp_spec.threshold_or_multi)].append(value) # mean of the repeats
     
        A = []
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            for threshold in self.param_spec.thresholds:
                a = []
                for N in self.param_spec.num_vertices:
                    a.append(statistics.mean(plot[(N,threshold)]))
                A.append(a)
        elif(self.param_spec.howtocomputethreshold == "multis"):
            for threshold in self.param_spec.multis:
                a = []
                for N in self.param_spec.num_vertices:
                    a.append(statistics.mean(plot[(N,threshold)]))
                A.append(a)
        X = self.param_spec.num_vertices
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            Y = self.param_spec.thresholds
        elif(self.param_spec.howtocomputethreshold == "multis"):
            Y = self.param_spec.multis
    
        fig, ax = plt.subplots()
        c = ax.pcolor(A, vmin=0, vmax=1,cmap=self.cmap_pcolor_zo)
        ax.set_xticks([i+0.5 for i in range(len(X))])
        ax.set_xticklabels(X)
        ax.set_yticks([i+0.5 for i in range(len(Y))])
        ax.set_yticklabels(Y)
        ax.set_xlabel('N')
        ax.set_ylabel(self.param_spec.howtocomputethreshold)
        ax.set_title('Percent of conflict edges per edge wrt number of points in DT and'+ self.param_spec.howtocomputethreshold)
        fig.colorbar(c, ax=ax)
        fig.savefig(self.options.oprefix+"/plot_percent_of_conflict_edges_f_"+self.param_spec.howtocomputethreshold +"_N.pdf")
        #plt.show()
        


    # Plot: frac[sol/all possibilities] wrt (n, thres) 
    def plot_frac_sol_f_N_threshold(self):
        plot = {} # dict N,threshold -> list of coloring_result
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get((exp_spec.N,exp_spec.threshold_or_multi))==None:
                plot[(exp_spec.N,exp_spec.threshold_or_multi)] = [] # create empty list
            plot[(exp_spec.N,exp_spec.threshold_or_multi)].append(statistics.mean(np.divide(i_spec.algo_result[ALGONAME], np.power(i_spec.num_c,i_spec.number_of_main_vertices, dtype = float)))) # mean of the repeats
        A = []
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            for threshold in self.param_spec.thresholds:
                a = []
                for N in self.param_spec.num_vertices:
                    a.append(statistics.mean(plot[(N,threshold)]))
                A.append(a)
        elif(self.param_spec.howtocomputethreshold == "multis"):
            for threshold in self.param_spec.multis:
                a = []
                for N in self.param_spec.num_vertices:
                    a.append(statistics.mean(plot[(N,threshold)]))
                A.append(a)

        X = self.param_spec.num_vertices
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            Y = self.param_spec.thresholds
        elif(self.param_spec.howtocomputethreshold == "multis"):
            Y = self.param_spec.multis

        fig, ax = plt.subplots() # clear

        c = ax.pcolor(A, norm=LogNorm(vmin=1/1000000, vmax=1),cmap=self.cmap_pcolor_zo) #(A, vmin=0, vmax=1)
        fig.colorbar(c, ax=ax)

        the_table = plt.table(cellText=[[round(f,10) for f in a] for a in A],
                      rowLabels=Y,
                      colLabels=X,
                      #loc='bottom',
                      bbox=[0, -0.8, 1, 0.5]
                      )
        plt.subplots_adjust(bottom=0.5)
        ax.set_xticks([i+0.5 for i in range(len(X))])
        ax.set_xticklabels(X)
        ax.set_yticks([i+0.5 for i in range(len(Y))])
        ax.set_yticklabels(Y)
        ax.set_xlabel('N')
        ax.set_ylabel(self.param_spec.howtocomputethreshold)
        ax.set_title('Fraction of solutions/possibilites wrt N and'+ self.param_spec.howtocomputethreshold)
        fig.savefig(self.options.oprefix+"/plot_frac_sol_f_N_"+ self.param_spec.howtocomputethreshold +".pdf")
        #plt.show()

    # Plot: frac[sol/all possibilities] wrt (num_c, thres)
    def plot_frac_sol_f_numc_threshold(self):
        plot = {} # dict num_c, threshold -> list of coloring_result
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            if plot.get((exp_spec.num_c,exp_spec.threshold_or_multi))==None:
                plot[(exp_spec.num_c,exp_spec.threshold_or_multi)] = [] # create empty list
            plot[(exp_spec.num_c, exp_spec.threshold_or_multi)].append(statistics.mean(np.divide(i_spec.algo_result[ALGONAME], np.power(i_spec.num_c,i_spec.number_of_main_vertices, dtype = float)))) # mean of the repeats
        A = []
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            for threshold in self.param_spec.thresholds:
                a = []
                for c in self.param_spec.num_colors:
                    a.append(statistics.mean(plot[(c, threshold)]))
                A.append(a)
        elif(self.param_spec.howtocomputethreshold == "multis"):
            for threshold in self.param_spec.multis:
                a = []
                for c in self.param_spec.num_colors:
                    a.append(statistics.mean(plot[(c, threshold)]))
                A.append(a)

        X = self.param_spec.num_colors
        if(self.param_spec.howtocomputethreshold == "thresholds"):
            Y = self.param_spec.thresholds
        elif(self.param_spec.howtocomputethreshold == "multis"):
            Y = self.param_spec.multis
        
        #plt.subplots() # clear
        fig, ax = plt.subplots() # clear

        c = ax.pcolor(A, norm=LogNorm(vmin=1/1000000, vmax=1),cmap=self.cmap_pcolor_zo) #(A, vmin=0, vmax=1)
        fig.colorbar(c, ax=ax)

        the_table = plt.table(cellText=[[round(f,10) for f in a] for a in A],
                      rowLabels=Y,
                      colLabels=X,
                      #loc='bottom',
                      bbox=[0, -0.8, 1, 0.5]
                      )
        plt.subplots_adjust(bottom=0.5)
        
        ax.set_xticks([i+0.5 for i in range(len(X))])
        ax.set_xticklabels(X)
        ax.set_yticks([i+0.5 for i in range(len(Y))])
        ax.set_yticklabels(Y)
        #ax.set_xlabel('num_c')
        #ax.set_ylabel(self.param_spec.howtocomputethreshold)
        #ax.set_title('Fraction of solutions wrt number of colors and'+self.param_spec.howtocomputethreshold)
        fig.savefig(self.options.oprefix+"/plot_frac_sol_f_numc_"+ self.param_spec.howtocomputethreshold+".pdf")
        #plt.show()

    ##################################
    # plot min intersections        ##
    ##################################
    # auxiliary function of plot_min_intersections,
    # used to merge all the lists together
    # (input plot_dict is modified by side effect)
    # returns the length of the lists (all are extended to the same size)
    def intersections_merge(self,param_list,plot_dict):
        max_max_len = 0
        for n in param_list:
            list_of_lists = plot_dict[n]
            # compute maximum length of lists
            max_len = 0
            for l in list_of_lists:
                max_len = max(max_len,len(l))
            # init sum
            max_max_len = max(max_max_len,max_len)
            sum_of_lists = [0]*max_len
            # sum
            for l in list_of_lists:
                for i in range(len(l)):
                    sum_of_lists[i] += l[i]
            # mean
            n_lists = len(list_of_lists)
            plot_dict[n] = [e/n_lists for e in sum_of_lists]
        # extend all lists to same length
        for n in param_list:
            plot_dict[n].extend([0]*(max_max_len-len(plot_dict[n])))
        return max_max_len

    # auxiliary function of plot_min_intersections,
    # used to create the bar plot
    # * type is edge, face or star
    # * param is N or num_c
    def min_intersections_plot(self,param_list,plot_dict,max_max_len,step,str_type,str_param):
        colors = plt.cm.viridis(np.linspace(0, 1, len(param_list)))
        y_offset = np.zeros(max_max_len)
        plt.subplots() # clear
        for i,n in enumerate(param_list):
            plt.bar(np.arange(max_max_len), plot_dict[n], 1, bottom=y_offset, color=colors[i]) # 1 is barwidth
            y_offset = y_offset + plot_dict[n]
        # TODO: add caption for color code (use param_list and str_param)
        #plt.title("Average minimum interesection area per "+str_type)
        #plt.xlabel("area with step="+str(step))
        #plt.ylabel("average number of "+str_type+" (cumulated for each value of "+str_param+")")
        plt.legend(labels=list(map(lambda x:"c"+"="+str(x),param_list))) # lambda x:str_param+
        plt.savefig(self.options.oprefix+"/plot_min_intersections_"+str_type+"_"+str_param+".pdf")

    # Plot: histogram of distribution of min intersection sizes (edge, face, star)
    # ASSUMPTION: only one radius, which is equal to the step
    def plot_min_intersections(self):
        # parse data (N and num_c)
        plot_edge_N = {} # dict: N -> list of arrays min_inter_edge
        plot_edge_num_c = {} # dict: num_c -> list of arrays min_inter_edge
        plot_face_N = {}
        plot_face_num_c = {}
        plot_star_N = {}
        plot_star_num_c = {}
        for n in self.param_spec.num_vertices:
            plot_edge_N[n] = []
            plot_face_N[n] = []
            plot_star_N[n] = []
        for c in self.param_spec.num_colors:
            plot_edge_num_c[c] = []
            plot_face_num_c[c] = []
            plot_star_num_c[c] = []
        step = self.param_spec.radii[0] # caution: ASSUMPTION used here
        for exp_spec,i_spec in zip(self.exp_specs,self.instances):
            plot_edge_N[exp_spec.N].extend(i_spec.min_intersections_edge)
            plot_face_N[exp_spec.N].extend(i_spec.min_intersections_face)
            plot_star_N[exp_spec.N].extend(i_spec.min_intersections_star)
            plot_edge_num_c[exp_spec.num_c].extend(i_spec.min_intersections_edge)
            plot_face_num_c[exp_spec.num_c].extend(i_spec.min_intersections_face)
            plot_star_num_c[exp_spec.num_c].extend(i_spec.min_intersections_star)
        # merge all lists into one: sum and divide
        max_max_len_edge_N = self.intersections_merge(self.param_spec.num_vertices,plot_edge_N)
        max_max_len_face_N = self.intersections_merge(self.param_spec.num_vertices,plot_face_N)
        max_max_len_star_N = self.intersections_merge(self.param_spec.num_vertices,plot_star_N)
        max_max_len_edge_num_c = self.intersections_merge(self.param_spec.num_colors,plot_edge_num_c)
        max_max_len_face_num_c = self.intersections_merge(self.param_spec.num_colors,plot_face_num_c)
        max_max_len_star_num_c = self.intersections_merge(self.param_spec.num_colors,plot_star_num_c)
        # bar plots
        self.min_intersections_plot(self.param_spec.num_vertices,plot_edge_N,max_max_len_edge_N,step,"edge","N")
        self.min_intersections_plot(self.param_spec.num_vertices,plot_face_N,max_max_len_face_N,step,"face","N")
        self.min_intersections_plot(self.param_spec.num_vertices,plot_star_N,max_max_len_star_N,step,"star","N")
        self.min_intersections_plot(self.param_spec.num_colors,plot_edge_num_c,max_max_len_edge_num_c,step,"edge","num_c")
        self.min_intersections_plot(self.param_spec.num_colors,plot_face_num_c,max_max_len_face_num_c,step,"face","num_c")
        self.min_intersections_plot(self.param_spec.num_colors,plot_star_num_c,max_max_len_star_num_c,step,"star","num_c")


    # auxiliary function of plot_mean_intersections,
    # used to create the bar plot
    # * type is edge, face or star
    # * param is N or num_c
    def mean_intersections_plot(self,param_list,plot_dict,max_max_len,step,str_type,str_param):
        colors = plt.cm.viridis(np.linspace(0, 1, len(param_list)))
        y_offset = np.zeros(max_max_len)
        plt.subplots() # clear
        for i,n in enumerate(param_list):
            plt.bar(np.arange(max_max_len), plot_dict[n], 1, bottom=y_offset, color=colors[i]) # 1 is barwidth
            y_offset = y_offset + plot_dict[n]
        # TODO: add caption for color code (use param_list and str_param)
        #plt.title("Average mean interesection area per "+str_type)
        #plt.xlabel("area with step="+str(step))
        #plt.ylabel("average number of "+str_type+" (cumulated for each value of "+str_param+")")
        plt.legend(labels=list(map(lambda x:"c"+"="+str(x),param_list)))
        plt.savefig(self.options.oprefix+"/plot_mean_intersections_"+str_type+"_"+str_param+".pdf")

    def plot_mean_intersections(self):
        # parse data (N and num_c)
        plot_edge_N = {} # dict: N -> list of arrays min_inter_edge
        plot_edge_num_c = {} # dict: num_c -> list of arrays min_inter_edge
        plot_face_N = {}
        plot_face_num_c = {}
        plot_star_N = {}
        plot_star_num_c = {}
        for n in self.param_spec.num_vertices:
            plot_edge_N[n] = []
            plot_face_N[n] = []
            plot_star_N[n] = []
        for c in self.param_spec.num_colors:
            plot_edge_num_c[c] = []
            plot_face_num_c[c] = []
            plot_star_num_c[c] = []
        step = self.param_spec.radii[0] # caution: ASSUMPTION used here
        for exp_spec,i_spec in zip(self.exp_specs,self.instances):
            plot_edge_N[exp_spec.N].extend(i_spec.mean_intersections_edge)
            plot_face_N[exp_spec.N].extend(i_spec.mean_intersections_face)
            plot_star_N[exp_spec.N].extend(i_spec.mean_intersections_star)
            plot_edge_num_c[exp_spec.num_c].extend(i_spec.mean_intersections_edge)
            plot_face_num_c[exp_spec.num_c].extend(i_spec.mean_intersections_face)
            plot_star_num_c[exp_spec.num_c].extend(i_spec.mean_intersections_star)
        # merge all lists into one: sum and divide
        max_max_len_edge_N = self.intersections_merge(self.param_spec.num_vertices,plot_edge_N)
        max_max_len_face_N = self.intersections_merge(self.param_spec.num_vertices,plot_face_N)
        max_max_len_star_N = self.intersections_merge(self.param_spec.num_vertices,plot_star_N)
        max_max_len_edge_num_c = self.intersections_merge(self.param_spec.num_colors,plot_edge_num_c)
        max_max_len_face_num_c = self.intersections_merge(self.param_spec.num_colors,plot_face_num_c)
        max_max_len_star_num_c = self.intersections_merge(self.param_spec.num_colors,plot_star_num_c)
        # bar plots
        self.mean_intersections_plot(self.param_spec.num_vertices,plot_edge_N,max_max_len_edge_N,step,"edge","N")
        self.mean_intersections_plot(self.param_spec.num_vertices,plot_face_N,max_max_len_face_N,step,"face","N")
        self.mean_intersections_plot(self.param_spec.num_vertices,plot_star_N,max_max_len_star_N,step,"star","N")
        self.mean_intersections_plot(self.param_spec.num_colors,plot_edge_num_c,max_max_len_edge_num_c,step,"edge","num_c")
        self.mean_intersections_plot(self.param_spec.num_colors,plot_face_num_c,max_max_len_face_num_c,step,"face","num_c")
        self.mean_intersections_plot(self.param_spec.num_colors,plot_star_num_c,max_max_len_star_num_c,step,"star","num_c")


    ########################################################################################
    # Plot: scatter o=pos/x=neg = f(num_c,threshold) with colors for multis (should fix N)##
    ########################################################################################
    def plot_scatter_pos_f_threshold_numc(self):
        plt.subplots() # clear
        if(self.param_spec.howtocomputethreshold=="multis"):
            cmin = self.param_spec.multis[0]
            cmax = self.param_spec.multis[-1]
        elif(self.param_spec.howtocomputethreshold=="thresholds"):
            cmin = self.param_spec.thresholds[0]
            cmax = self.param_spec.thresholds[-1]
        norm = plt.Normalize(cmin, cmax)
        viridis = cm.get_cmap('viridis', 256)
        xpos = []
        ypos = []
        cpos = []
        xneg = []
        yneg = []
        cneg = []
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            for i in range(len(i_spec.algo_result[ALGONAME])):
                if i_spec.algo_result[ALGONAME][i]>0:
                    xpos.append(i_spec.threshold[i]) # x = threshold
                    ypos.append(exp_spec.num_c) # y = num_c
                    cpos.append(norm(exp_spec.threshold_or_multi)) # color = multi
                else:
                    xneg.append(i_spec.threshold[i])
                    yneg.append(exp_spec.num_c)
                    cneg.append(norm(exp_spec.threshold_or_multi))
        plt.scatter(xpos,ypos,c=cpos,marker='o',cmap=viridis)
        plt.scatter(xneg,yneg,c=cneg,marker='x',cmap=viridis)
        plt.title("Plot of postive (o) / negative (x) wrt threshold, num_c")
        plt.xlabel("threshold")
        plt.ylabel("num_c")
        #plt.legend(labels=["positive","negative"],loc=4)
        if(self.param_spec.howtocomputethreshold=="multis"):
            recs = [mpatches.Rectangle((0,0),1,1,fc=viridis(norm(m))) for m in self.param_spec.multis]
            classes = self.param_spec.multis
            plt.legend(recs,classes)
        elif(self.param_spec.howtocomputethreshold=="thresholds"):
            recs = [mpatches.Rectangle((0,0),1,1,fc=viridis(norm(t))) for t in self.param_spec.thresholds]
            classes = self.param_spec.thresholds
            plt.legend(recs,classes)
        plt.savefig(self.options.oprefix+"/plot_scatter_pos_f_threshold_numc.pdf")
        #plt.show()


    # Plot: scatter o=pos/x=neg = f(N,threshold) with colors for multis (should fix num_c)
    def plot_scatter_pos_f_threshold_N(self):
        plt.subplots() # clear
        if(self.param_spec.howtocomputethreshold=="multis"):
            cmin = self.param_spec.multis[0]
            cmax = self.param_spec.multis[-1]
        elif(self.param_spec.howtocomputethreshold=="thresholds"):
            cmin = self.param_spec.thresholds[0]
            cmax = self.param_spec.thresholds[-1]
        norm = plt.Normalize(cmin, cmax)
        viridis = cm.get_cmap('viridis', 256)
        xpos = []
        ypos = []
        cpos = []
        xneg = []
        yneg = []
        cneg = []
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            for i in range(len(i_spec.algo_result[ALGONAME])):
                if i_spec.algo_result[ALGONAME][i]>0:
                    xpos.append(i_spec.threshold[i]) # x = threshold
                    ypos.append(exp_spec.N) # y = N
                    cpos.append(norm(exp_spec.threshold_or_multi)) # color = multi
                else:
                    xneg.append(i_spec.threshold[i])
                    yneg.append(exp_spec.N)
                    cneg.append(norm(exp_spec.threshold_or_multi))
        plt.scatter(xpos,ypos,c=cpos,marker='o',cmap=viridis)
        plt.scatter(xneg,yneg,c=cneg,marker='x',cmap=viridis)
        plt.title("Plot of postive (o) / negative (x) wrt threshold, N")
        plt.xlabel("threshold")
        plt.ylabel("N")
        #plt.legend(labels=["positive","negative"],loc=4)
        if(self.param_spec.howtocomputethreshold=="multis"):
            recs = [mpatches.Rectangle((0,0),1,1,fc=viridis(norm(m))) for m in self.param_spec.multis]
            classes = self.param_spec.multis
            plt.legend(recs,classes)
        elif(self.param_spec.howtocomputethreshold=="thresholds"):
            recs = [mpatches.Rectangle((0,0),1,1,fc=viridis(norm(t))) for t in self.param_spec.thresholds]
            classes = self.param_spec.thresholds
            plt.legend(recs,classes)
        plt.savefig(self.options.oprefix+"/plot_scatter_pos_f_threshold_N.pdf")
        #plt.show()

    # (N,num_c) -> marker, used for scatter plots
    def NCmarker(self,iN,iC,lN,lC):
        markers=[".",",","o","v","^","<",">","1","2","3","4","8","s","p","P","*","h","H","+","x","X","D","d","|","_"]
        return markers[(iN*lC+iC)%len(markers)]

    # Plot: scatter pos=1/neg=0 = f(threshold) with colors for (N, num_c), multis = symbols
    def plot_scatter_pos_01_f_threshold(self):
        fig,ax = plt.subplots() # clear
        if(self.param_spec.howtocomputethreshold=="multis"):
            cmin = self.param_spec.multis[0]
            cmax = self.param_spec.multis[-1]
        elif(self.param_spec.howtocomputethreshold=="thresholds"):
            cmin = self.param_spec.thresholds[0]
            cmax = self.param_spec.thresholds[-1]
        norm = plt.Normalize(cmin, cmax)
        viridis = cm.get_cmap('viridis', 256)
        # map (N,c) -> list of three lists x,y,c
        plot = {}
        for N in self.param_spec.num_vertices:
            for num_c in self.param_spec.num_colors:
                plot[(N,num_c)] = [[],[],[]]
        # fill the lists of plot
        for exp_spec, i_spec in zip(self.exp_specs,self.instances):
            for i in range(len(i_spec.algo_result[ALGONAME])):
                plot[(exp_spec.N,exp_spec.num_c)][0].append(i_spec.threshold[i]) # x = threshold
                plot[(exp_spec.N,exp_spec.num_c)][2].append(norm(exp_spec.threshold_or_multi)) # color = multi
                if i_spec.algo_result[ALGONAME][i]>0:
                    plot[(exp_spec.N,exp_spec.num_c)][1].append(1) # y = 1
                else:
                    plot[(exp_spec.N,exp_spec.num_c)][1].append(0) # y = 0
        # do the plot
        N_list = self.param_spec.num_vertices
        lN = len(N_list)
        C_list = self.param_spec.num_colors
        lC = len(C_list)
        for N in self.param_spec.num_vertices:
            for num_c in self.param_spec.num_colors:
                X = plot[(N,num_c)][0]
                Y = plot[(N,num_c)][1]
                C = plot[(N,num_c)][2]
                iN = N_list.index(N)
                iC = C_list.index(num_c)
                m = self.NCmarker(iN,iC,lN,lC)
                plt.scatter(X,Y,c=C,cmap=viridis,marker=m)
        plt.title("Plot of postive/negative wrt threshold")
        plt.xlabel("threshold")
        plt.ylabel("0=negative 1=positive")
        # legend multis
        if(self.param_spec.howtocomputethreshold=="multis"):
            recs = [mpatches.Rectangle((0,0),1,1,fc=viridis(norm(m))) for m in self.param_spec.multis]
            classes = self.param_spec.multis
            legend1 = plt.legend(recs,classes,loc=4)
        elif(self.param_spec.howtocomputethreshold=="thresholds"):
            recs = [mpatches.Rectangle((0,0),1,1,fc=viridis(norm(t))) for t in self.param_spec.thresholds]
            classes = self.param_spec.thresholds
            legend1 = plt.legend(recs,classes,loc=4)
        # legend (N,num_c)
        marks = []
        for N in self.param_spec.num_vertices:
            for num_c in self.param_spec.num_colors:
                iN = N_list.index(N)
                iC = C_list.index(num_c)
                m = self.NCmarker(iN,iC,lN,lC)
                lab = "(N="+str(N)+",C="+str(num_c)+")"
                marks.append(mlines.Line2D([], [], marker=m, label=lab, linestyle='None')) #color='blue', markersize=15,
        legend2 = plt.legend(handles=marks,bbox_to_anchor=(1.05, 1), loc='upper left')
        ax.add_artist(legend1)
        ax.add_artist(legend2)
        # save
        plt.savefig(self.options.oprefix+"/plot_scatter_pos_01_f_threshold.pdf", bbox_inches = 'tight')
        #plt.show()

    # Plot: distribution of frac[sol/all possibilities]
    def plot_violin_frac_sol(self):
        data = [] # repeats -> list of fraction sol
        label = [] # init label x-axis
        for i_spec in self.instances:
            data.append(np.divide(i_spec.algo_result[ALGONAME],\
                        np.power(i_spec.num_c,i_spec.number_of_main_vertices, dtype = float)))
            #label.append([statistics.mean(i_spec.num_c), statistics.mean(i_spec.multi)])
        
        fig, ax = plt.subplots() # clear
        #ax.set_title('Distribution of fraction #sol/search space')
        #ax.set_ylabel('Fraction values')
        
        ax.violinplot(data, showmeans=True)

        # set axis label
        #ax.xaxis.set_tick_params(direction='out')
        #ax.xaxis.set_ticks_position('bottom')
        #ax.set_xticks(np.arange(1, len(label) + 1))
        #ax.set_xticklabels(label)
        #ax.set_xlim(left, len(label) + 1, right)
        #ax.set_xlabel('(c, m)')
        #set_axis_style(ax, label)
        

        plt.subplots_adjust(bottom=0.15, wspace=0.05)
    
        fig.savefig(self.options.oprefix+"/plot_violin_frac_sol_"+ self.param_spec.howtocomputethreshold +".pdf")
        #plt.show()


    ##################################
    # PLOTS (end)
    ##################################

    # Compute the statistics
    def compute_stats(self):
        #basic plots
        # print("plot running_time_build_cg_f_N")
        # self.plot_running_time_build_cg_f_N()
        # print("plot_number_of_main_vertices_f_N")
        # self.plot_number_of_main_vertices_f_N()
        # print("plot_running_time_build_cg_f_N_numc")
        # self.plot_running_time_build_cg_f_N_numc()
        # print("plot_percent_of_pos_f_N_numc")
        # self.plot_percent_of_pos_f_N_numc()
        # print("plot_frac_sol_f_N_numc")
        # self.plot_frac_sol_f_N_numc()
        
        #plots with thresholds or multis (functions depend on self.param_spec.howtocomputethreshold)
        # print("plot_percent_of_pos_f_threshold_N")
        # self.plot_percent_of_pos_f_threshold_N()
        # print("plot_percent_of_conflict_edges_f_threshold_N")
        # self.plot_percent_of_conflict_edges_f_threshold_N()
        print("plot_percent_of_pos_f_threshold_numc")
        self.plot_percent_of_pos_f_threshold_numc()
        # print("plot_percent_of_pos_f_threshold_radius")
        # self.plot_percent_of_pos_f_threshold_radius()
        # print("plot_frac_sol_f_N_threshold")
        # self.plot_frac_sol_f_N_threshold()
        print("plot_frac_sol_f_numc_threshold")
        self.plot_frac_sol_f_numc_threshold()
        
        #plots min intersections
        if(self.param_spec.minormean != None): # min or mean
            print("plot_min_intersections (edge, face, star)")
            self.plot_min_intersections()
            print("plot_mean_intersections (edge, face, star)")
            self.plot_mean_intersections()

        # scatter
        # print("plot_scatter_pos_f_threshold_numc")
        # self.plot_scatter_pos_f_threshold_numc()
        # print("plot_scatter_pos_f_threshold_N")
        # self.plot_scatter_pos_f_threshold_N()
        # print("plot_scatter_pos_01_f_threshold")
        # self.plot_scatter_pos_01_f_threshold()

        # violin plot
        print("plot_violin_frac_sol")
        self.plot_violin_frac_sol()
        

    # Statistics
    def stats(self):
        print("# Parse output files")
        self.parse_output_files()
        print("# Compute the statistics")
        self.compute_stats()
        return

    # Run the experiments
    def run(self):
        if self.options.gen:
            self.save_param()
            self.gen_cin()
            self.build_all_CG()
        elif self.options.run:
            self.run_cases()
        elif self.options.stats:
            self.stats()

#i########################################################################
# options
#i########################################################################

# ATT: % is the help="" must be double ie %%
parser = argparse.ArgumentParser(description='My parser')
parser.add_argument("-d", action="store_true", dest="dry",
                    default=False, help="Print commands but do not run")
parser.add_argument('-o', "--oprefix", dest="oprefix",
                    default=".", help="Output directory prefix")
parser.add_argument("-P", nargs='?', type=int, default=1, const=1,
                    dest="max_processes", help="Parallelism: number of processes")

parser.add_argument("-N",  nargs='*', type=int,
                    dest="num_vertices", help="Number of vertices in the DT")
parser.add_argument("--num_c", nargs='*', type=int, dest="num_colors", help="Number of colors for a VD point")
#parser.add_argument("--nshift",  nargs='*', type=int, dest="num_vertex_copies", help="Number of copies for a VD point")
parser.add_argument("--radii",  nargs='*', type=float,
                    dest="radii", help="Radii for random VD points")
#parser.add_argument("--min_distances",  nargs='*', type=float, dest="min_distances", help="Minimum distance of two VD points")
parser.add_argument("--thresholds",  nargs='*', type=float, dest="thresholds",
                    help="Threshold for the intersection area of two polygons in conflict")
parser.add_argument("--multis",  nargs='*', type=float, dest="multis",
                    help="Multiply by min intersection to get threshold for the intersection area of two polygons in conflict")
parser.add_argument("--minormean", nargs='?', type=int, dest="minormean",
                    default=0, help="Compute threshold multis with min 1 or mean 2 or min+mean 3 (in two separate folders in this last case)")
parser.add_argument("--repeat",  nargs='?', type=int, dest="repeat", help="Number of repetitions for each experiment")
parser.add_argument("--algos",  nargs='*', type=str,
                    dest="algos", help="Algorithm for solving the problem")

parser.add_argument("-g", action="store_true", dest="gen",
                    default=False, help="Generate the data")
parser.add_argument("-r", action="store_true", dest="run",
                    default=False, help="Run the program")
parser.add_argument("-s", action="store_true", dest="stats",
                    default=False, help="Compute the statistics")

#i########################################################################
# Main
#i########################################################################

options = parser.parse_args()
proj = Project(options)
proj.run()
