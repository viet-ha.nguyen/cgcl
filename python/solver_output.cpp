#include "../src/datastructure.hpp"
#include "../src/coloring.hpp"
#include "../src/import.hpp"

// Command line arguments
#include <boost/program_options.hpp>
namespace po = boost::program_options;

// Timer
#include <boost/timer/timer.hpp>
#include <boost/chrono/chrono.hpp>

// Run bash command (std::system)
#include <stdlib.h>

// constants for the algorithms
#define BACKTRACK 1
#define BACKTRACK_LOCAL 2
#define BACKTRACK_GLOBAL 3
#define CUT_SET_CYCLE 4
#define MINISAT 5
#define CHOCO 6
#define BACKTRACK_ENUM 11
#define MINISAT_ENUM 15
#define CHOCO_ENUM 16
#define FIND 100
#define ENUM 101
#define NONE 102

// main
// command line arguments:
// * --gv string, filepath to the data .gv file containing a conflictgraph
// * --output string, filepath to the .txt file to write informations about the execution of the algorithm
// * --algo NAME, coloring algorithm to be run on the conflict graph

int main(int argc, char* argv[]){

//--------step 0: parse command line arguments-------------------
std::cout << "# parse command line arguments" << std::endl;

// Declare the supported options
po::options_description desc("Allowed options");
desc.add_options()
  ("help", "produce help message")
  ("gv", po::value<std::string>() , "path to the data .gv file containing a conflictgraph")
  ("output", po::value<std::string>() , "coloring algorithm to be run on the conflict graph")
  ("algo", po::value<std::string>() , "algorithm chosen for solving the problem (e.g. BACKTRACK)");

po::variables_map vm;
po::store(po::parse_command_line(argc, argv, desc), vm);
po::notify(vm);

// help
if(vm.count("help")) {
  std::cout << desc << "\n";
  return 0;
}

// gv
std::string filenamegv;
if (vm.count("gv")) {
  filenamegv = vm["gv"].as<std::string>();
  std::cout << "filenamegv=" << filenamegv << std::endl;
} else {
  std::cout << "data .gv is not given, stop" << std::endl;
  return 0;
}

// output
std::string filenameoutput;
if (vm.count("output")) {
  filenameoutput = vm["output"].as<std::string>();
  std::cout << "filenameoutput=" << filenameoutput << std::endl;
} else {
  filenameoutput = filenamegv+".output.txt";
  std::cout << "filenameoutput=" << filenameoutput << " (default)" << std::endl;
}

// algo
int algo;
std::string algoName;
if (vm.count("algo")) {
  algoName = vm["algo"].as<std::string>();
  if(algoName=="BACKTRACK"){algo=BACKTRACK;}
  else if(algoName=="BACKTRACK_LOCAL"){algo=BACKTRACK_LOCAL;}
  else if(algoName=="BACKTRACK_GLOBAL"){algo=BACKTRACK_GLOBAL;}
  else if(algoName=="CUT_SET_CYCLE"){algo=CUT_SET_CYCLE;}
  else if(algoName=="CHOCO"){algo=CHOCO;}
  else if(algoName=="MINISAT"){algo=MINISAT;}
  else if(algoName=="BACKTRACK_ENUM"){algo=BACKTRACK_ENUM;}
  else if(algoName=="CHOCO_ENUM"){algo=CHOCO_ENUM;}
  else if(algoName=="MINISAT_ENUM"){algo=MINISAT_ENUM;}
  else{algo=0;}
  std::cout << "algo=" << algo << " (" << algoName << ")" << std::endl;
} else {
  algo = 0;
  std::cout << "algo=" << algo << " (default: no algorithm will be run)" << std::endl;
}


//--------step1: import CG from gv file -------------------
ConflictGraph cg;
import_conflict_graph_graphviz(filenamegv, cg);

//--------step2: call the solver -------------------
bool coloring_found;
float coloring_count;
int find_or_enum;
boost::timer::cpu_timer rtime; // timer start
switch(algo){
  case BACKTRACK:
    find_or_enum = FIND;
    std::cout << "## coloring : BACKTRACK" << std::endl;
    coloring_found = conflictColoring_backtrack(cg);
    break;
  case BACKTRACK_LOCAL:
    find_or_enum = FIND;
    std::cout << "## coloring : BACKTRACK_LOCAL" << std::endl;
    coloring_found = conflictColoring_backtrack_degree_local(cg);
    break;
  case BACKTRACK_GLOBAL:
    find_or_enum = FIND;
    std::cout << "## coloring : BACKTRACK_GLOBAL" << std::endl;
    coloring_found = conflictColoring_backtrack_degree_global(cg);
    break;
  case CUT_SET_CYCLE:
    find_or_enum = FIND;
    std::cout << "## coloring : CUT_SET_CYCLE" << std::endl;
    coloring_found = conflictColoring_cut_set_cycle(cg);
    break;
  case CHOCO:
    std::cout << "## coloring : CHOCO" << std::endl;
    {
      std::string cmdchocofind = "../choco/run_choco_output.sh "+filenamegv+" "+filenameoutput+" 1";  // 1=find
      std::system(cmdchocofind.c_str());
    }
    return 0;
  case MINISAT:
    std::cout << "## coloring : MINISAT" << std::endl;
    {
      std::string cmdminisatfind = "../minisat/minisat_output.o "+filenamegv+" "+filenameoutput+" 1";  // 1=find
      std::system(cmdminisatfind.c_str());
    }
    return 0;
  case CHOCO_ENUM:
    std::cout << "## coloring : CHOCO_ENUM" << std::endl;
    {
      std::string cmdchocoenum = "../choco/run_choco_output.sh "+filenamegv+" "+filenameoutput+" 2"; // 2=enum
      std::system(cmdchocoenum.c_str());
    }
    return 0;
  case BACKTRACK_ENUM:
    find_or_enum = ENUM;
    std::cout << "## coloring : BACKTRACK_ENUM" << std::endl;
    coloring_count = conflictColoring_backtrack_enum_count(cg);
    break;
  case MINISAT_ENUM:
    std::cout << "## coloring : MINISAT_ENUM" << std::endl;
    {
      std::string cmdminisatenum = "../minisat/minisat_output.o "+filenamegv+" "+filenameoutput+" 2";  // 2=enum      std::system(cmdminisatenum.c_str());
      std::system(cmdminisatenum.c_str());

    }
    return 0;
  default:
    find_or_enum = NONE;
    std::cout << "## coloring : no algorithm to run" << std::endl;
    break;
}
rtime.stop(); // timer stop

//--------step2: algos and coloring -------------------

// useful variables
vertex_iter vi, vi_end;

// open file
std::ofstream ofs;
ofs.open(filenameoutput, std::ofstream::app);

// write algo and running time
// "user" see https://theboostcpplibraries.com/boost.timer 
// "seconds" see https://stackoverflow.com/a/17320036
typedef boost::chrono::duration<double> sec;
sec seconds = boost::chrono::nanoseconds(rtime.elapsed().user);

ofs << algoName << "_running_time:" << seconds.count() << "\n";

// write algorithm output
switch(find_or_enum){
  case FIND:
    ofs << algoName << "_coloring_result: " << coloring_found << "\n";
    break;
  case ENUM:
    ofs << algoName << "_coloring_result: " << coloring_count << "\n";
    break;
  default: // NONE
    ofs << "no algorithm\n";
    break;
}
// empty line
ofs << "\n";

// close
ofs.close();

return 0;
}
