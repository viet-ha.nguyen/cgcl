The cg_builder requires:
* a graph (use add_vertex and add_edge)
* a map from vertex to its conformation container

Example:

```cpp
Graph g;
std::map<Graph_vertex_desc,std::list<Polygon_2>> map_c;
ConflictGraph cg;
cg_builder.build_ConflictGraph(g, map_c, cg);
```