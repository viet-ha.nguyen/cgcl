#include <map>
#include <vector>
#include "../src/datastructure.hpp"
#include "../src/import.hpp"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
using namespace boost;

template <class Traits>
class T_ConflictGraph_builder{
public:

// Main_graph_type is assumed to be a boostgraph
// we have a map from vertex descriptors to containers of conformations
typedef typename Traits::Main_graph_type Main_graph_type;
typedef typename Traits::Map_Vertex_descriptor_2_Conformation_Container Map_Vertex_descriptor_2_Conformation_Container;
typedef typename Map_Vertex_descriptor_2_Conformation_Container::mapped_type Conformation_container;
typedef typename Conformation_container::value_type Conformation_type;

// short names
typedef typename graph_traits<Main_graph_type>::vertex_descriptor Main_vertex_desc;
typedef typename graph_traits<Main_graph_type>::vertex_iterator Main_vertex_iter;
typedef typename graph_traits<Main_graph_type>::edge_descriptor Main_edge_desc;
typedef typename graph_traits<Main_graph_type>::edge_iterator Main_edge_iter;

// A class checking whether 2 conformations have a conflict
// Assumption: this class is a functor with an operator ()
typedef typename Traits::Conflict_checker Conflict_checker;

public:
  // cg is assumed to be an empty ConflictGraph
  void build_ConflictGraph(Main_graph_type& g, Map_Vertex_descriptor_2_Conformation_Container map_vd2cc, ConflictGraph& cg){
    // Create an instance of the class Conflict_checker
    Conflict_checker cc = Conflict_checker();
    // usefull variables
    vertex_desc vd;
    int num_c, intconfu, intconfv;
    Main_vertex_desc u, v;
    Main_vertex_iter vi, vi_end;
    Main_edge_iter ei, ei_end;
    typename Conformation_container::iterator uci, uci_end, vci, vci_end;

    // map from vertices of g to vertices of cg
    std::map<Main_vertex_desc,vertex_desc> mapv_g2cg;

    // 1. create the vertices of cg
    for(tie(vi,vi_end)=vertices(g); vi != vi_end; ++vi){
      // get the number of colors
      num_c = map_vd2cc[*vi].size();
      // add the vertex
      vd = add_conflict_vertex(num_c, cg);
      // remember the correspondence
      mapv_g2cg[*vi] = vd;
    }

    // 2. create the (conflict) edges of cg
    for(tie(ei,ei_end)=edges(g); ei != ei_end; ++ei){
      // get the endpoint vertices of *ei
      u = source(*ei, g);
      v = target(*ei, g);
      // iterate over pairs of conformations
      uci = map_vd2cc[u].begin();
      uci_end = map_vd2cc[u].end();
      intconfu = 0;
      for(; uci != uci_end; ++uci){
        vci = map_vd2cc[v].begin();
        vci_end = map_vd2cc[v].end();
        intconfv = 0;
        for(; vci != vci_end; ++vci){
          if(cc.operator()(*uci, *vci)){
            add_conflict_edge(mapv_g2cg[u], intconfu, mapv_g2cg[v], intconfv, cg);
          }
          intconfv++;
        }
        intconfu++;
      }
    }
  }
};
