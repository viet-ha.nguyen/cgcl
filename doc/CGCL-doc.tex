\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage{verbatim}
\usepackage[colorlinks]{hyperref}
\usepackage{enumitem}
\usepackage{subfig}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{fig-macros} % macros for the figures

\title{Conflict Graph Coloring Library (CGCL)\\Documentation}
%\author{Your Name Here}
\date{2020} 

\begin{document}

\maketitle

\tableofcontents

\section{Theory}

\subsection{Conflict and Fullfil Colorings}

Let a graph $G = (V,E)$ where $V$ is the set of vertices and $E$ is the set of edges.  
Let $k \geq 1$ be an integer.
We denote by $[k]$ the set of integers $\{1,2,\dots,k\}$.
Every vertex $v \in V$ is given a set of colors $\mathcal{C}_v \subset [k]$.
For an edge $uv$, a complete bipartite graph the complete bipartite graph $K_{uv}$ is defined as follows:
$V(K_{uv}) = \{(u,i), i \in \mathcal{C}_u\} \cup \{(v,j), j \in \mathcal{C}_v\}$,
$E(K_{uv}) = \{(u,i)(v,j)| \forall i \in \mathcal{C}_u, \forall j \in \mathcal{C}_v\}$.
We say a conflict graph $M_{uv}$ for every $uv \in E$ which is a subgraph of $K_{uv}$.
Denote $\mathcal{M}$ the set of conflict graphs (SoC $\mathcal{M}$) of all edges in $E$.
For a coloring $c$, an edge $uv$ is conflicting if $(u,c(u))(v,c(v)) \in M_{uv}$ and fulfilled otherwise.
An $\mathcal{M}$-conflict coloring is a coloring of $G$ with no conflicting edges.
We define the following problem which decides if given a set $\mathcal{M}$,
a graph $G$ admits a $\mathcal{M}$-conflict coloring.
See Figure~\ref{fig:theory} for an example.

\medskip

\noindent \fbox{\parbox{\textwidth}{ 
\textsc{Conflict Coloring.} \\
\underline{Input}: A graph $G = (V,E)$,a SoC $\mathcal{M}$.. \\
\underline{Question}: Does there exist a $\mathcal{M}$-conflict coloring $c$ of $G$?
}}\\[.2em]

If a graph has no conflict coloring,
we generalize the conflict coloring to find a conflict coloring
which maximizes the number of fulfilled edges.

\medskip

\noindent \fbox{\parbox{\textwidth}{ 
\textsc{Fulfill Coloring.} \\
\underline{Input}: A graph $G = (V,E)$, an integer $k$,
 a $k$-SoC $\mathcal{M}$, and a positive integer $q$. \\
\underline{Question}: Does there exist a $k$-coloring $c$ of $G$
with at least $q$ $\mathcal{M}$-fulfilled edges ?
}}\\[.2em]

\begin{figure}[h]
  \centerline{
	\input{fig-theory.tex}
  }
  \caption{
  	A graph $G$ on four vertices (left) and a SoC $\mathcal{M}$ of $G$ (right)
  	where each vertex is given a subset of colors (yellow, blue, red).
  	A $\mathcal{M}$-conflict coloring $c$ of $G$ is
  	$c(a)=c(d)=$ ``yellow'', $c(b)=c(c)$``blue''.
  	Coloring $c$ is a conflict coloring of graph $G$.
  }
  \label{fig:theory}
\end{figure}

\subsection{Cut set cycle method}

\textcolor{red}{TODO}
\begin{figure}[h]
  \centerline{
	\input{fig-ccs.tex}
  }
  \caption{Given a graph $G$ in Figure \ref{fig:theory}, let a cycle cut set of $G$ is $\{a \}$ (red vertex in the left figure) which leaves a subtree on the remaining vertices. The cut is adjacent to $b, c$, then we obtain a tree by adding new edges between a copy of the cut to $b, c$ (in the right figure).}
  \label{fig:ccs}
\end{figure}
\section{Architecture}

CGCL is written mostly in C++,
it uses \href{https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/table_of_contents.html}{boost graph library}.
The main sources are in \verb|src| folder:
\begin{itemize}
	\item \verb|src/datastructure.hpp| contains
	the datastructure to encode conflict graphs
	(see Section~\ref{s:datastructure}).
	\item \verb|src/import.cpp| and \verb|import.hpp| contain
	functions to construct conflict graphs 
	and print or export them to graphviz
	(see Section~\ref{s:import}).
	\item \verb|src/coloring.cpp| and \verb|coloring.hpp| contain
	functions to compute a conflict coloring
	(see Section~\ref{s:coloring}).
\end{itemize}
The other folders are:
\begin{itemize}
	\item \verb|examples/| contains example \verb|.cpp| files using CGCL
	(see Section~\ref{s:examples}).
	\item \verb|choco/| uses the CSP solver \href{https://choco-solver.org/}{Choco} (Java) to compute conflict coloring
	(see Section~\ref{s:choco}).
	\item \verb|minisat/| uses the SAT solver \href{http://minisat.se/}{Minisat} to compute conflict coloring
	(see Section~\ref{s:minisat}).
	\item \verb|time_analysis/| compares the time of the different solvers/algorithms
	(see Section~\ref{s:time_analysis}).
\end{itemize}

\section{Data structure}
\label{s:datastructure}

The file \verb|src/datastructure.hpp| contains all types,
structures of variables in the program.

\medskip

Our graphs are
\verb|typedef adjacency_list<setS, vecS, undirectedS> Graph|:
encoding with adjacency lists;
where \texttt{setS} is the set of edges (without multiple edges),
and \texttt{vecS} is the set of vertices.
For convenience, we define the following types:
\begin{itemize}
	\item \verb|vertex_desc| boost graph vertex descriptor,
	\item \verb|vertex_iter| boost graph vertex iterator,
	\item \verb|edge_desc| boost graph edge descriptor,
	\item \verb|edge_iter| boost graph edge iterator,
	\item \verb|out_edge_iter| boost graph out edges iterator.
\end{itemize}

The class \texttt{ConflictGraph} contains \verb|main_g| as
a main graph $G$ (type \verb|MGraph|), \verb|conflict_g| as
a set of conflict graphs (type \verb|CGraph|) and the function \verb|void clear()| which is to clear a ConflictGraph.\\
The graph \verb|conflict_g| contains one vertex for each couple
(vertex,color) of the main graph.
Properties of these graphs allow to go from vertices of
the main graph to corresponding vertices of the conflict graph and conversely.
Given a vertex descriptor \verb|v| of \verb|main_g|, we have:
\begin{itemize}
	\item \verb|main_g[v].num_colors| is its number of colors (int);
	\item \verb|main_g[v].conflict_vertices| contains
	a \verb|std::vector| of vertex descriptors of the
	\verb|main_g[v].num_colors| corresponding vertices in
	the conflict graph, at position \verb|i| we have the vertex
	 of the conflict graph corresponding to \verb|(v,i)|;
	\item \verb|main_g[v].color| is the current color of the vertex.
	\item \verb|main_g[v].color_max| remembers the color of the vertex \verb|v| in the current maximum conflict coloring.
\end{itemize}

Given a vertex descriptor \verb|v| of \verb|conflict_g|, we have:
\begin{itemize}
	\item \verb|conflict_g[v].main_vertex| is the vertex
	descriptor of the corresponding vertex in the main graph;
    \item \verb|conflict_g[v].color_index| is the color of the
    vertex in the conflict graph, {\em i.e.} vertex \verb|v|
    corresponds to
    \verb|(conflict_g[v].main_vertex,conflict_g[v].color_index)|.
\end{itemize}

An example is given on Figure~\ref{fig:datastructure}.

\begin{figure}[h]
  \centerline{
	\input{fig-datastructure.tex}
  }
  \caption{
    % verb does not work in caption :-(
  	Example of \texttt{ConflictGraph G}
  	with \texttt{G.main\_g} on the left
  	and \texttt{G.conflict\_g} on the right.
  	The properties of vertex $d$ of the main graph,
  	and vertex $d_2$ of the conflict graph
  	(corresponding to $d$ with color $2$),
  	are given.
  	Labels $a$, $b$, $c$, $d$, $d_0$ $d_1$, $d_2$
  	are vertex descriptors (type \texttt{vertex\_desc}).
  	For example,
  	\texttt{G.main\_g[}$d$\texttt{].conflict\_vertices[2]}
  	gives the vertex descriptor of $d_2$,
  	and \texttt{G.conflict\_g[}$d_2$\texttt{].main\_vertex} gives the vertex descriptor of $d$.
  	The color of vertex $d$ is stored in
  	\texttt{G.main\_g[}$d$\texttt{].color}.
  }
  \label{fig:datastructure}
\end{figure}

\section{Import}
\label{s:import}

The file \verb|src/import.cpp| contains functions to construct, import and export conflict graphs.
Use \verb|#include "import.hpp"| to be able to call these functions.

\bigskip\noindent
\verb|void print_conflict_graph(ConflictGraph)|\\
Print main graph and conflict graph in terminal (\verb|std::cout|).

\bigskip\noindent
\verb|vertex_desc add_conflict_vertex(int, ConflictGraph&)|\\
Add a vertex to the main graph.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item number of colors in conflict graph,
	\item the ConflictGraph.
\end{itemize}
Returns a vertex descriptor of the new vertex in the main graph.

\bigskip\noindent
\verb|std::pair<edge_desc,bool> add_conflict_edge(vertex_desc, int, vertex_desc, int, ConflictGraph&)|\\
Add an edge $(v,c)(u,d)$ to the conflict graph (and $uv$ to the main graph if it does not yet exist).\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a vertex descriptor $v$ in main graph,
	\item a color $c$ (of $v$),
	\item a vertex descriptor $u$ in main graph,
	\item a color $d$ (of $u$),
	\item the ConflictGraph.
\end{itemize}
Returns the result of \verb|boost::add_edge| to the conflict graph, see \href{https://www.boost.org/doc/libs/1_58_0/libs/graph/doc/adjacency_list.html}{boost documentation}.

\bigskip\noindent
\verb|int generate_random_conflict_graph_erdos_renyi(ConflictGraph&, int, float, int, float)|\\
Generate a random conflict graph.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph (empty),
	\item number of vertices in main graph,
	\item probability of each edge in main graph,
	\item number of colors per vertex of the main graph,
	\item probability of each edge in the conflict graph (bipartite).
\end{itemize}
Returns the number of edges in the main graph.

\bigskip\noindent
\verb|int export_conflict_graph_graphviz(ConflictGraph, std::string)|\\
Write a graph (the conflict graph) to a graphviz \verb|.gv|
file in \href{https://graphviz.org/doc/info/lang.html}{dot format}.
Node id is of the form \verb|vx:cy| for vertex $(x,y)$ of the conflict graph ($x$ and $y$ are integers).\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph,
	\item path to the \verb|.gz| file.
\end{itemize}

\bigskip\noindent
\verb|int export_main_graph_graphviz(ConflictGraph, std::string)|\\
Write a graph (the main graph) to a graphviz \verb|.gv|
file in \href{https://graphviz.org/doc/info/lang.html}{dot format}.
Node id is of the form \verb|vx| for vertex $x$ of the main graph ($x$ is integer).\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph,
	\item path to the \verb|.gz| file.
\end{itemize}

\bigskip\noindent
\verb|int generate_random_conflict_tree_prufer(ConflictGraph&, int, int, float)|\\
Generate a random conflict tree using \href{https://fr.wikipedia.org/wiki/Codage_de_Pr%C3%BCfer}{Pr\"{u}fer sequences}.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph (empty),
	\item number of vertices in main graph,
	\item number of colors per vertex of the main graph,
	\item probability of each edge in the conflict graph (bipartite).
\end{itemize}
Returns the number of edges in the main graph (it is the number of vertices minus 1).

\bigskip\noindent
\verb|bool import_conflict_graph_graphviz(std::string, ConflictGraph&)|\\
Generate a conflict graph from dot file (\verb|.gv|)
 which contains the information of the conflict graph.\\
Arguments:
\begin{itemize}[nosep,label=--]
\item the path to the dot file,
\item a ConflictGraph (empty).
\end{itemize}
Returns true if the ConflictGraph is successfully generated.\\

\bigskip\noindent
\verb|bool copy_conflict_edge(vertex_desc, vertex_desc, ConflictGraph, vertex_desc, vertex_desc, ConflictGraph&)|\\
To copy the conflict graph of an edge to some edge.\\
Arguments:
\begin{itemize}[nosep,label=--]
\item in the first three arguments, there are two \verb|vertex_desc|
 corresponding to two endpoints of an edge we want to copy in a ConflictGraph.
\item in the last three argument, there are two \verb|vertex_desc| 
corresponding to two endpoints of a edge whose conflict graph will be copied) in a ConflictGraph.
\end{itemize}
Returns true if the conflict graph of the second edge is copied. 

\bigskip\noindent
\verb|bool create_subgraph(ConflictGraph, std::unordered_set<vertex_desc>, ConflictGraph&, std::map<vertex_desc,vertex_desc>&)|\\
To create a subgraph which is a copy of a subgraph of a ConflictGraph 
(the copy is of type \verb|ConflictGraph|).\\
Arguments:
\begin{itemize}[nosep,label=--]
\item a ConflictGraph,
\item a subset of vertices of the ConflictGraph above,
\item a ConflictGraph (empty) (will be built to the copy subgraph),
\item a map from \verb|vertex_desc| of vertices of the subset  to \verb|vertex_desc| of vertices of the second ConflictGraph.
\end{itemize}
Returns true if the subgraph is successfully generated.

\section{Coloring}
\label{s:coloring}

The file \verb|src/coloring.hpp| contains functions to
check if a graph admits a conflict coloring for {\sc Conflict coloring} problem, 
or enumerates conflict colorings, or
find a conflict coloring that maximizes the number of fulfilled edges for {\sc Fulfill coloring}.
Use \verb|#include "coloring.hpp"|  to call these functions.

\subsection{Enumeration}

\medskip\noindent
\verb|bool enum_stop(ConflictGraph&)|\\
To stop the enumeration of solution. It is used in \verb|conflictColoring_backtrack|, to stop this function once a conflict coloring found.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns \verb|true| if a coloring has been found
and stop the function \verb|conflictColoring_backtrack|.

\bigskip\noindent
\verb|bool enum_printAll(ConflictGraph&)|\\
To print all solutions. It is used in \verb|conflictColoring_backtrack|.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns \verb|false| after printing a conflict coloring 
in order to continue the enumeration
 in the function \verb|conflictColoring_backtrack|.

\bigskip\noindent
\verb|bool conflictColoring_backtrack_enum(ConflictGraph&)|\\
To enumerate all conflict colorings using backtrack algorithm. 
This is the initialization part, the recursive part is \\
\verb|conflictColoring_backtrack_enum_rec(vi,vi_end,G,enum_printAll)|.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns \verb|false| after printing all conflict coloring and reset \verb|.color| to -1.

\bigskip\noindent
\verb|template<typename some_vertex_iter = vertex_iter>|\\
\verb|bool conflictColoring_backtrack_enum_rec(some_vertex_iter vi, some_vertex_iter vi_end,|\\
\verb|ConflictGraph& G, bool(*f)(ConflictGraph& G))|\\
Find a conflict coloring using backtrack algorithm (recursive part).\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item vertex iterator (current)
	\item vertex iterator end
	\item the ConflictGraph
	\item a function taking as input the conflict graph,
	and doing something with the solution (in \verb|.color| property),
	returning \verb|true| then the enumeration stops,
	or \verb|false| then the enumeration continues.
\end{itemize}
returns:
\begin{itemize}[nosep,label=--]
	\item \verb|true| if a coloring has been found,
	and sets \verb|.color| property of vertices in main graph to the valid conflict coloring
	\item \verb|false| if no coloring has been found,
	and reset \verb|.color| to -1
\end{itemize}
the iterators are generic, because we may follow the order of a boost graph iterator, or a custom vector.

\subsection{Split on connected components}

\bigskip\noindent
\verb|bool conflictColoring_splitcc(ConflictGraph&, bool(*)(ConflictGraph&))|\\
To find a conflict coloring by doing one by one connected component of the graph.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph,
	\item a conflict coloring algorithm.
\end{itemize} 
returns \verb|false| once a connected component has no conflict colorings,
 and returns \verb|true| if all connected component are conflict colorable.

\subsection{Backtrack algorithms}

\bigskip\noindent
\verb|bool conflictColoring_backtrack(ConflictGraph&)|\\
To split the call of \verb|conflictColoring_backtrack_cc| on each connected component of a graph.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns \verb|true| if a coloring has been found
and sets \verb|.color| property of vertices in the main graph to
a valid conflict coloring;
or \verb|false| if no coloring has been found and reset \verb|.color| to -1.

\bigskip\noindent
\verb|bool conflictColoring_backtrack_cc(ConflictGraph&)|\\
To find a conflict coloring using backtrack algorithm
 which follows the order of vertices as the input of the main graph (connected graph). 
 This is the initialization part, the recursive part is \\
\verb|conflictColoring_backtrack_enum_rec(vi,vi_end,G,enum_stop)|.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns \verb|true| if a coloring has been found
and sets \verb|.color| property of vertices in the main graph to
a valid conflict coloring;
or \verb|false| if no coloring has been found and reset \verb|.color| to -1.

\bigskip\noindent
\verb|bool conflictColoring_backtrack_degree_global(ConflictGraph&)|\\
To split the call \verb|bool conflictColoring_backtrack_degree_global_cc|
 on each connected component of a graph.
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns the same thing as the function \verb|conflictColoring_backtrack|.

\bigskip\noindent
\verb|bool conflictColoring_backtrack_degree_global_cc(ConflictGraph&)|\\
This is another initialization part beside the previous function \verb|conflictColoring_backtrack|,
 the recursive part is still the same \verb|conflictColoring_backtrack_rec|.\\
In this functions, the backtrack algorithm 
follows the order of vertices 
in decreasing degree associated to the main graph.
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns the same thing as the first function.

\bigskip\noindent
\verb|bool conflictColoring_backtrack_degree_local(ConflictGraph&)|\\
To split the call \verb|bool conflictColoring_backtrack_degree_local_cc| 
on each connected component of a graph.
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns the same thing as the function \verb|conflictColoring_backtrack|.

\bigskip\noindent
\verb|bool conflictColoring_backtrack_degree_local_cc(ConflictGraph&)|\\
This is another initialization part beside functions \verb|conflictColoring_backtrack| 
and \verb|conflictColoring_backtrack_degree_global|, 
and the same recusive part 
\verb|conflictColoring_backtrack_rec|.\\
In this functions, the backtrack algorithm 
follows the order of vertices 
in decreasing degree of a spanning tree
 whose root is the vertex of highest degree (in the main graph).
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns the same thing as the two mentioned functions.

\subsection{Fulfill (max) coloring}

\bigskip\noindent
\verb|int conflictColoring_max(ConflictGraph& )|\\
Compute a maximum conflict coloring by checking all possible colorings.
It is implemented recursively using
\verb|conflictColoring_backtrack_max_rec|.\\
Arguments :
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize}
Returns the maximum number of fulfilled edges,
and sets property \verb|.color| of vertices in the main graph to
such a coloring (the implementation uses \verb|.color|
 to explore all colorings, 
 and \verb|.color_max| to remember the best conflict coloring so far).

\bigskip\noindent
\verb|int conflictColoring_maxTree(ConflictGraph&)|\\
Find a maximum conflict coloring (fulfill coloring) in tree graphs 
(in polynomial time).
 This is the initialization part,
  the recusive part is 
 \verb|conflictColoring_maxTree_rec|.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph (which is a tree).
\end{itemize} 
returns the maximum number of fulfilled edges,
and sets property \verb|.color_max| such a coloring 
and also \verb|.color| if this coloring is a conflict coloring.

\subsection{Cut set cycle}

Global variables in order to pass additional arguments to \verb|cut_2_tree|
which is the functor given to con\verb|flictColoring_backtrack_enum_rec|
which has a fixed prototype: \verb|bool(*)(ConflictGraph&)|
\begin{itemize}[nosep,label=--]
	\item \verb|std::map<vertex_desc,vertex_desc> GLOBAL_map_tree2cut;|
	\item \verb|ConflictGraph GLOBAL_G_tree;|
\end{itemize}

\bigskip\noindent
\verb|bool conflictColoring_cut_set_cycle(ConflictGraph&)|\\
To split the call of \verb|conflictColoring_cut_set_cycle_cc| (described below) on connected components using the cut set cycle technique.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns \verb|true| if there exist a conflict coloring
 and set property \verb|.color| such a coloring; 
 \verb|false| once there is a connected component has no conflict colorings.

\bigskip\noindent
\verb|bool conflictColoring_cut_set_cycle_cc(ConflictGraph&)|\\
To find a conflict coloring on a graph (connected graph)
using the cut set cycle technique.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns \verb|true| if there exist a conflict coloring
and set property \verb|.color| such a coloring;
\verb|false| otherwise and set \verb|.color| to -1.

\bigskip\noindent
\verb|bool cut_2_tree(ConflictGraph&)|\\
Complete a coloring in a tree given a sub-coloring of the cut set.\\
Argument:
\begin{itemize}[nosep,label=--]
	\item a conflictGraph \verb|G_cut| ($\iff$ sub-coloring)
	\item a conflictGraph \verb|GLOABAL_G_tree| (a tree)
	\item a map \verb|GLOBAL_map_tree2cut| from vertex descriptors of \verb|GLOABAL_G_tree| to \verb|G_cut|.
\end{itemize}
This is the functor passed by \verb|conflictColoring_cut_set_cycle_cc|
to \verb|conflictColoring_backtrack_enum|,
which tries to extend the solutions on the cut set.
For each sub-coloring found on the cut set,
it calls the function \verb|pre_conflictColoring_Forest|
to complete coloring

\subsection{Conflict pre-Coloring}

\bigskip\noindent
\verb|bool pre_conflictColoring_Forest(ConflictGraph&)|\\
To find a conflict coloring given a sub-coloring (a subset of vertices in which any of them is already given a color, then called conflict pre-coloring) by splitting the call of \verb|conflictColoring_Tree| (described below) on connected components (trees).\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph.
\end{itemize} 
returns \verb|true| if there exist a conflict coloring and set property \verb|.color| such a coloring; \verb|false| once there is a connected component which has no conflict pre-coloring.  

\bigskip\noindent
\verb|bool pre_conflictColoring_Tree(ConflictGraph&, vertex_desc)|\\
To find a conflict pre-coloring on a tree (using a polynomial time algorithm). This is the initialization part, the recursive part is \verb|bool pre_conflictColoring_Tree_rec(vertex_desc, vertex_desc, ConflictGraph&)|\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item a ConflictGraph (a tree),
	\item the root of the tree.
\end{itemize} 
returns \verb|true| if there exist a conflict pre-coloring and set property \verb|.color| such a coloring; \verb|false| once there is a vertex in which it has no color in order to fulfill all edges from this vertex to all its children.  

\bigskip\noindent
\verb|std::vector<bool> pre_conflictColoring_Tree_rec(vertex_desc, vertex_desc, ConflictGraph&)|\\
Computes a conflict coloring on tree recursively given a coloring in a subset of vertices.\\
Arguments:
\begin{itemize}[nosep,label=--]
	\item vertex descriptor of the current vertex
	\item vertex descriptor of the parent
	\item the conflict graph which is assumed to be a tree
\end{itemize} 
returns an array of \verb|num_colors| (of the current vertex) booleans,
indicating which colors are possible (\verb|true|) or impossible (\verb|false|) for this vertex.
remark: \verb|current==parent| for the root.

\section{Examples}
\label{s:examples}

Examples are given in the folder \verb|examples/|,
with a \verb|makefile| to compile them
(executables have extension \verb|.o|).
See the comments in each example files for details,
and the \verb|makefile| for compilation instructions.

Some programs generate graphs in \href{https://graphviz.org/doc/info/lang.html}{graphviz/dot format} with extension \verb|.gv|.
To create \verb|my_graph.pdf| from \verb|my_graph.gv|
you can run \verb|./graphviz2pdf.sh my_graph.gv|
which simply calls the required command \verb|dot| with appropriate arguments
(you may need to give execution right to the script with the command
\verb|chmod u+x graphviz2pdf.sh|).

\subsection{Example of manual graph}

The file \verb|example_manual_graph.cpp| gives an example to construct
a graph manually, {\em i.e.} using the functions
\verb|add_conflict_vertex| and
\verb|add_conflict_edge|, and call
\verb|conflictColoring_backtrack| then print the conflict coloring.

\medskip\noindent
To compile: \verb|make example_manual_graph|\\
To run: \verb|./example_manual_graph.o|

\subsection{Example of random graph for conflict coloring problem}

The file \verb|example_random_graph.cpp| gives an example to construct
a random graph with Erdos Renyi model, {\em i.e.} using the functions
\verb|generate_random_conflict_graph_erdos_renyi| 
that returns the number of edges of the main graph $G$,
\verb|export_conflict_graph_graphviz| 
to print the conflict graph to \verb|.gv| file, and call
\verb|conflictColoring_backtrack| then print the conflict coloring.

\medskip\noindent
To compile: \verb|make example_random_graph|\\
To run: \verb|./example_random_graph.o v pm c pc my_graph.gv|\\
where
\begin{itemize}[nosep,label=--]
	\item \verb|v| is the number of vertices in main graph (int),
	\item \verb|pm| is the probability of each edge in main graph ($0\leq$float$\leq 1$),
	\item \verb|c| is the number of colors per vertex in the main graph (int),
	\item \verb|pc| is the probability of each edge in the conflict graph ($0\leq$float$\leq 1$),
	\item \verb|my_graph.gv| is a path to a \verb|.gv| file to be created (string).
\end{itemize}

\subsection{Example of random graph for fulfill coloring problem}

The file \verb|example_random_graph_max.cpp| is exactly the same as  \verb|example_random_graph.cpp| but there is only one difference is that it calls 
\verb|conflictColoring_backtrack_max|.

\medskip\noindent
To compile: \verb|make example_random_graph_max|\\
To run: \verb|./example_random_graph_max.o v pm c pc my_graph.gv|

\subsection{Example of random tree for fulfill coloring problem}

The file \verb|example_random_tree.cpp| gives an example
to construct a random (connected) tree,
it returns the number of edges of the tree by using function
\verb|generate_random_conflict_tree_prufer|;
then uses \verb|export_conflict_graph_graphviz| 
to print the conflict graph of the tree;
and calls \verb|conflictColoring_maxTree| 
to find a maximum conflict coloring.

\medskip\noindent
To compile: \verb|make example_random_tree|\\
To run: \verb|./example_random_tree.o v c pc my_tree.gv|\\
where
\begin{itemize}[nosep,label=--]
	\item \verb|v| is the number of vertices in main tree (int),
	\item \verb|c| is the number of colors per vertex in the main tree (int),
	\item \verb|pc| is the probability of each edge in the conflict graph ($0\leq$float$\leq 1$),
	\item \verb|my_tree.gv| is a path to a \verb|.gv| file to be created (string).
\end{itemize}

\subsection{Example of a graph imported from a dot file for coloring problem}

The file \verb|example_graph_from_dotFile.cpp| gives an example
where the input is a conflict graph contained in a dot file
(generated previously by using the function \verb|export_conflict_graph_graphviz|)
imported with the function \verb|generate_graph_from_dotFile|;
then it calls various conflict coloring algorithms 
to find a conflict coloring
if exists (and also enumerate the solutions).

\medskip\noindent
To compile: \verb|make example_graph_from_dotFile|\\
To run: \verb|./example_graph_from_dotFile.o my_graph.gv|

\subsection{Example to check a conflict coloring}

The file \verb|example_graph_from_dotFile.cpp| gives an example
to check a conflict coloring.\\
\textcolor{red}{TODO:} create functions in \verb|coloring.cpp|, and give the \verb|.gv| file as a command line argument.

\section{Choco}
\label{s:choco}

The folder \verb|choco/| contains a modelization in Java of conflict coloring
for the CSP solver \href{https://choco-solver.org/}{Choco}.

The file \verb|Color.java| takes as argument a path to \verb|.gv| file, and:
\begin{itemize}
	\item read the graph using \verb|java.util.regex|,
	\item for each vertex, create a Choco \verb|IntVar| with a range of its number of colors,
	\item for each edge \verb|(u,v)| in the main graph,
	create a list of pairs of conflicts \verb|(u,c)(v,d)| in a \verb|Tuples| object,
	\item add the constraints to the model with
	\verb|model.not(model.table(u,v,tuples)).post()|,
	\item call the solver with \verb|model.getSolver().solve()|.
\end{itemize}

You can create the compiled \verb|.class| with \verb|make|, and run it with \verb|./run.sh my_graph.gv|
(which calls \verb|java| for you with the correct arguments;
you may need to give execution permission with \verb|chmod u+x run.sh|).
Note that the \verb|.jar| of Choco is already placed in the folder and linked to \verb|-classpath| in \verb|run.sh|.
Remark that one can also enumerate the solution with a loop on the call to \verb|.solve()|.

\section{Minisat}
\label{s:minisat}

The folder \verb|minisat/| contains a modelization in C++
of conflict coloring for the SAT solver \href{http://minisat.se/}{Minisat}.
Requires the install of command \verb|minisat|
(available in most package managers).

The file \verb|minisat.cpp| takes as argument a path \verb|.gv| file, and:
\begin{itemize}
	\item generate a \verb|ConflictGraph| with \verb|import_conflict_graph_graphviz|,
	\item create a \verb|.cnf| file
	(\href{https://people.sc.fsu.edu/~jburkardt/data/cnf/cnf.html}{DIMACS} format for Minisat) with:
	\begin{itemize}
		\item one Boolean variable per vertex \verb|(v,c)| of the conflict graph,
		\item one clause per vertex in the main graph:
		a disjunction of its colors to enforce it to have at least one color,
		\item one clause per conflict edge:
		a disjunction of the negated endpoints to avoid the conflict,
	\end{itemize}
	\item run \verb|minisat| via a call to the C++ \verb|system| function,
	and use \verb|WEXITSTATUS| on the returned value to get the result
	(this may change with your version of \verb|system|?).
\end{itemize}

You can create the compiled \verb|.o| with \verb|make minisat|,
and run it with \verb|./minisat.o my_graph.gv|.

The file \verb|minisat-atmostone.cpp| is the same with additional clauses to enforce exactly one color per vertex.

\section{Time analysis}
\label{s:time_analysis}

The folder \verb|time_analysis/| contains a set of programs in order to
compare the running time of conflict coloring algorithms and solvers.
It requires \href{https://www.gnu.org/software/octave/}{GNU Octave} to be installed
(command \verb|octave|, available in most package managers),
in order to generate plots and tables from the experimental data.

\subsection{Generate samples}

The user must first generate some sample \verb|.gv| files
for the experiment, in a new folder.
The instances are typically generated in groups of 10 for each setting of parameters.
See an example in \verb|xp-10-0.5-c-0.3/gen-10-0.5-c-0.3.cpp|.

\subsection{Programs \texttt{conflictColoring\_*.cpp}}

User just need to call \verb|make|
(and also \verb|make| in the \verb|choco/| folder).

For each algorithm and solver, we have a \verb|.cpp|
file designed, given some sample \verb|.gv| file as input, to:
\begin{itemize}
	\item when the algorithm/solver terminates:
	print the decision (\verb|0| or \verb|1|) and running time \verb|t| (float, in seconds),
	\verb|0,12.345,| for example,
	\item if a signal \verb|SIGINT| is received (corresponding to the timeout):
	print \verb|-1,-1,|.
\end{itemize}
Note that Choco uses the \verb|.jar| in \verb|choco/| folder,
hence we have the files \verb|Color_1bit.java|
and \verb|run_1bit.sh| in this folder.

\subsection{Run the experiment}

The main program is \verb|time_analysis.sh|,
which will take as argument the folder containing \verb|.gv| files,
for example \verb|./time_analysis.sh xp-10-0.5-c-0.3|
(you may need to give execution rights with \verb|chmod u+x time_analysis.sh|).
User may read Section~\ref{ss:octave} before running the experiment.

It performs the following tasks:
\begin{itemize}
	\item Loop on the sample files and run all algorithms and solvers on each sample
	(uses the bash \verb|timeout| command to send a \verb|SIGINT| signal after some chosen timeout),
	concatenate the results in \verb|.data| files
	(one file per algorithm/solver),
	\item concatenate all the results into a single \verb|data.csv| file
	(an example is given in \verb|xp-10-0.5-c-0.3/data.csv|),
	\item run GNU Octave to generate tables and plots.
\end{itemize}

\subsection{Plot of results by GNU Octave}
\label{ss:octave}

User needs to copy the \verb|template-result.m|
as \verb|result.m| in its experiment folder,
and adapt the variables setting in the upper part,
for example (\verb|xp-10-0.5-c-0.3/result.m|):

\begin{verbatim}
name_xp = "xp-10-0.5-c-0.3"; % printed on graphics
pname = "number of colors"; % name of the parameter axis
p = []; % vector of values in parameter
for i = 10:5:50 % from 10 to 50, increasing by 5
  p = [p i];
endfor
ninstances = 10; % number of instances for each parameter value
timeout = 30;
\end{verbatim}

This program will then:
\begin{itemize}
	\item load \verb|data.csv|,
	\item check the coherence of decisions among all algorithms and solvers
	(call to function in \verb|coherence.m|),
	\item compute mean times among each group of \verb|ninstances|
	(call to function in \verb|meantime.m|),
	\item compute the percent of positive decisions
	(call to function in \verb|posnegpercent.m|),
	\item generate a {\LaTeX} table of results in
	file \verb|name_xp-table.tex|,
	which is then automatically compiled to \verb|name_xp-table.pdf|
	(call to function in \verb|printTable.m|),
	\item generate a plot of results in 
	file \verb|name_xp-plot.pdf|
	(call to function in \verb|plotData.m|).
\end{itemize}

\end{document}
