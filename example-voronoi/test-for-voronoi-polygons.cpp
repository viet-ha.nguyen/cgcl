#include "Voronoi_based_conflict_graph_builder.hpp"

// main
// command line arguments: see --help

int main(int argc, char* argv[]){

//--------step 0: parse command line arguments-------------------
std::cout << "# parse command line arguments" << std::endl;

// Declare the supported options
po::options_description desc("Allowed options");
desc.add_options()
  ("help", "produce help message")
  //("nshift", po::value<int>() , "number of random VD points")
  ("num_c", po::value<int>() , "number of conformation for each DT::point")
  ("radius", po::value<double>() , "radius of the random VD points")
  //("min_dist_threshold", po::value<double>() , "minimum distance between two VD points")
  ("threshold", po::value<double>() , "threshold for the intersection area of two polygons in conflict")
  ("cin", po::value<std::string>() , "filepath to the .cin file containing 2D DT points")
  ("gv", po::value<std::string>() , "filepath to the .gv file where the conflict graph will be written")
  ("output", po::value<std::string>() , "filepath to the .yaml file to put informations on the conflict graph")
  ("polytikz", po::value<std::string>() , "filepath to a .tex file to draw polygons in tikz (optional)")
  ("min_intersections", po::value<double>() , "step to plot min intersections (optional)")
  ("multi", po::value<double>() , "need to decide value of threshold through min or mean intersections (optional)")
  ("minormean", po::value<int>() , "when multi is set, use 1=min or 2=mean (optional but required when --multi) or 3=min+mean (requires --gv2 --output2)")
  ("gv2", po::value<std::string>() , "filepath2 to the .gv file for mean when minormean=3")
  ("output2", po::value<std::string>() , "filepath2 to the .yaml file for mean when minormean=3");
  

po::variables_map vm;
po::store(po::parse_command_line(argc, argv, desc), vm);
po::notify(vm);

// help
if(vm.count("help")) {
  std::cout << desc << "\n";
  return 0;
}

/*
// nshift
if (vm.count("nshift")) {
  T_Voronoi_based_conflict_graph_builder<void>::nshift = vm["nshift"].as<int>();
  std::cout << "nshift=" <<   T_Voronoi_based_conflict_graph_builder<void>::nshift << std::endl;
} else {
  T_Voronoi_based_conflict_graph_builder<void>::nshift = 2;
  std::cout << "nshift=" << T_Voronoi_based_conflict_graph_builder<void>::nshift << " (default)" << std::endl;
}
*/

// num_c
if (vm.count("num_c")) {
  T_Voronoi_based_conflict_graph_builder<void>::num_c = vm["num_c"].as<int>();
  std::cout << "num_c=" <<   T_Voronoi_based_conflict_graph_builder<void>::num_c << std::endl;
} else {
  T_Voronoi_based_conflict_graph_builder<void>::num_c = 2;
  std::cout << "num_c=" << T_Voronoi_based_conflict_graph_builder<void>::num_c << " (default)" << std::endl;
}

// radius
if (vm.count("radius")) {
  T_Voronoi_based_conflict_graph_builder<void>::radius = vm["radius"].as<double>();
  std::cout << "radius=" << T_Voronoi_based_conflict_graph_builder<void>::radius << std::endl;
} else {
  T_Voronoi_based_conflict_graph_builder<void>::radius = 1;
  std::cout << "radius=" << T_Voronoi_based_conflict_graph_builder<void>::radius << " (default)" << std::endl;
}

// // min_dist_threshold
// if (vm.count("min_dist_threshold")) {
//   T_Voronoi_based_conflict_graph_builder<void>::min_dist_threshold = vm["min_dist_threshold"].as<double>();
//   std::cout << "min_dist_threshold=" << T_Voronoi_based_conflict_graph_builder<void>::min_dist_threshold << std::endl;
// } else {
//   T_Voronoi_based_conflict_graph_builder<void>::min_dist_threshold = -1;
//   std::cout << "min_dist_threshold=" << T_Voronoi_based_conflict_graph_builder<void>::min_dist_threshold << " (default)" << std::endl;
// }

// threshold
if (vm.count("threshold")) {
  Conflict_checker_for_pairs_of_voronoi_polygons::threshold = vm["threshold"].as<double>();
  std::cout << "threshold=" << Conflict_checker_for_pairs_of_voronoi_polygons::threshold << std::endl;
} else {
  Conflict_checker_for_pairs_of_voronoi_polygons::threshold = -1;
  std::cout << "threshold=" << Conflict_checker_for_pairs_of_voronoi_polygons::threshold << " (will be computed from min_intersection)" << std::endl;
  // check that user calls min_intersections in this case
  if(vm.count("min_intersections")==0){
    std::cout << "WARNING: threshold not set and min_intersection not called => using default threshold = 1" << std::endl;
    Conflict_checker_for_pairs_of_voronoi_polygons::threshold = 1;
  }
}

// cin
std::string filenamecin;
if (vm.count("cin")) {
  filenamecin = vm["cin"].as<std::string>();
  std::cout << "filenamecin=" << filenamecin << std::endl;
} else {
  filenamecin = "data/mydata_Point_2.cin";
  std::cout << "filenamecin=" << filenamecin << " (default)" << std::endl;
}

// gv
std::string filenamegv;
if (vm.count("gv")) {
  filenamegv = vm["gv"].as<std::string>();
  std::cout << "filenamegv=" << filenamegv << std::endl;
} else {
  filenamegv = "data/mydata_Point_2.cin.gv";
  std::cout << "filenamegv=" << filenamegv << " (default)" << std::endl;
}

// output
std::string filenameoutput;
if (vm.count("output")) {
  filenameoutput = vm["output"].as<std::string>();
  std::cout << "filenameoutput=" << filenameoutput << std::endl;
} else {
  filenameoutput = filenamegv+".output.txt";
  std::cout << "filenameoutput=" << filenameoutput << " (default)" << std::endl;
}

//--------construct the Delaunay triangulation-------------------
std::cout << "## compute the Delauney triangulation from data" << std::endl;
std::ifstream ifs(filenamecin);
assert(ifs);
std::istream_iterator<DT::Point> begin(ifs);
std::istream_iterator<DT::Point> end;
T_Voronoi_based_conflict_graph_builder<void>::dt.insert(begin, end);
ifs.close();
// draw the DT
//CGAL::draw(T_Voronoi_based_conflict_graph_builder<void>::dt);

//--------step1: Conflict Graph construction starts here-------------------

//-------- init --------
T_Voronoi_based_conflict_graph_builder<void> vor_cg_builder;

//-------- build main graph --------
std::cout << "# build the main graph from the Delauney triangulation" << std::endl;
vor_cg_builder.build_main_graph();

//-------- timer start --------
boost::timer::cpu_timer rtime;

//-------- build conformations for every vertex --------
std::cout << "# compute conformations (random polygons)" << std::endl;
// with version 1 (ncolor)
vor_cg_builder.build_random_polygon_ncolor();

//-------- timer pause --------
rtime.stop();

//-------- (optional) compute min intersections --------
std::array<double, 2> set_threshold;
// std::array<std::string,2> inter;
// inter.at(0) = "mininter";
// inter.at(1) = "meaniter"; 
double multi = -1;

// parse --min_intersections
if (vm.count("min_intersections")) {
  double step = vm["min_intersections"].as<double>();
  if (vm.count("multi")){
    multi = vm["multi"].as<double>();
    std::cout << "# multi=" << multi << std::endl;
  }
  else{
    multi = 1;
    std::cout << "multi=" << multi << " (defaut)" << std::endl;
  }
  std::cout << "# compute min intersections with step=" << step << " and multi=" << multi << std::endl;
  vor_cg_builder.min_intersections(step, filenameoutput, set_threshold);
}

//
// THE NEXT PART OF THE CODE IS SPLITTED IN TWO PARTS (if/else):
// IF: BUILD ONE GV
// ELSE: BUIDL TWO GV (minormean=3)
//

// the ConflictGraph
ConflictGraph cg;

if(Conflict_checker_for_pairs_of_voronoi_polygons::threshold == -1
  && vm.count("min_intersections")
  && vm.count("minormean")
  && vm["minormean"].as<int>() != 3)
{
  //
  // BUILD ONLY ONE .gv FILE (AND ONE .yaml)
  //

  //-------- set threshold and do things --------
  // there are two choices of threshold: by defaut threshold is given by user,
  // or threshold is defined from min_intersection (local threshold)
  int minormean = 0; // default minormean uses min
  if (Conflict_checker_for_pairs_of_voronoi_polygons::threshold == -1 && vm.count("min_intersections")){  
    if(vm.count("minormean")){
      minormean = vm["minormean"].as<int>() - 1; // trick to match set_threshold.at()
    }
    // set threshold
    Conflict_checker_for_pairs_of_voronoi_polygons::threshold = multi*set_threshold.at(minormean);
    std::cout << "# the computed threshold is threshold=" << Conflict_checker_for_pairs_of_voronoi_polygons::threshold << std::endl;
  }

  // build cg and export infos
  //-------- timer resume --------
  rtime.resume();

  //-------- build conflict graph --------
  std::cout << "# compute the conflict edges" << std::endl;
  cg.clear();
  vor_cg_builder.build_conflict_graph(cg);

  //-------- timer stop --------
  rtime.stop();

  //--------step2: export the ConflictGraph--------
  std::cout << "## export ConflictGraph to " << filenamegv << " (dot -Tpdf mygraph.gv -o mygraph.pdf)" << std::endl;
  export_conflict_graph_graphviz(cg, filenamegv);

  //--------step3: information of Conflict Graph -------------------
  std::cout << "## ConflictGraph infos in " << filenameoutput << std::endl;

  // open file
  std::ofstream ofs;
  ofs.open(filenameoutput, std::ofstream::app);

  // write running time
  // "user" see https://theboostcpplibraries.com/boost.timer 
  // "seconds" see https://stackoverflow.com/a/17320036
  typedef boost::chrono::duration<double> sec;
  sec seconds = boost::chrono::nanoseconds(rtime.elapsed().user);
  ofs << "running_time_build_cg: " << seconds.count() << "\n"; // time in seconds

  // write parameters
  //ofs << "nshift: " << T_Voronoi_based_conflict_graph_builder<void>::nshift << "\n";
  ofs << "num_c: " << T_Voronoi_based_conflict_graph_builder<void>::num_c << "\n";
  ofs << "radius: " << T_Voronoi_based_conflict_graph_builder<void>::radius << "\n";
  ofs << "multi: " << multi << "\n";
  //ofs << "min_dist_threshold: " << T_Voronoi_based_conflict_graph_builder<void>::min_dist_threshold << "\n";
  ofs << "threshold: " << Conflict_checker_for_pairs_of_voronoi_polygons::threshold << "\n";
  //ofs << inter.at(i) << ": " << set_threshold.at(i) << "\n";

  // write basic info to output file
  ofs << "number_of_main_vertices: " << boost::num_vertices(cg.main_g) << "\n";
  ofs << "number_of_main_edges: " << boost::num_edges(cg.main_g) << "\n";
  ofs << "average_number_of_color_per_vertices: " << ( boost::num_vertices(cg.main_g)==0 ? 0 : (double)boost::num_vertices(cg.conflict_g) / (double)boost::num_vertices(cg.main_g) ) << "\n";
  ofs << "average_number_of_conflict_edges_per_edge: " << ( boost::num_edges(cg.main_g)==0 ? 0 : (double)boost::num_edges(cg.conflict_g) / (double)boost::num_edges(cg.main_g) ) << "\n";

  /*
  // write list of colors (only useful for nshift)
  vertex_iter vi, vi_end;
  std::vector<int> list_num_c;
  for(tie(vi,vi_end)=vertices(cg.main_g); vi != vi_end; ++vi){
    list_num_c.push_back(cg.main_g[*vi].num_colors);
  }
  std::sort(list_num_c.begin(),list_num_c.end()); // sort the list
  ofs << "ordered_list_number_of_color: ";
  for(auto it = list_num_c.begin(); it != list_num_c.end(); ++it){
    ofs << *it << " ";
  }
  ofs << "\n";
  */

  // empty line
  ofs << "\n";

  // close
  ofs.close();
}
else{
  //
  // BUILD TWO .gv FILES (AND TWO .yaml)
  //

  // prepare variables to make a for loop (min and mean)
  std::array<double,2> mm_threshold;
  mm_threshold[0] = multi*set_threshold.at(0); // min threshold
  mm_threshold[1] = multi*set_threshold.at(1); // mean threshold
  std::array<std::string,2> mm_gv;
  mm_gv[0] = filenamegv; // min gv filepath
  if(vm.count("gv2")){
    mm_gv[1] = vm["gv2"].as<std::string>(); // mean gv filepath
  }
  else{
    std::cout << "WARNING: --gv2 required when --minormean=3 (both min and mean). Using again the value of --gv instead." << std::endl;
    mm_gv[1] = filenamegv;
  }
  std::array<std::string,2> mm_output;
  mm_output[0] = filenameoutput; // min yaml filepath
  if(vm.count("output2")){
    mm_output[1] = vm["output2"].as<std::string>(); // mean yaml filepath
  }
  else{
    std::cout << "WARNING: --output2 required when --minormean=3 (both min and mean). Using again the value of --outpu instead." <<  std::endl;
    mm_output[1] = filenameoutput;
  }

  // copy min_intersections infos from output to output2
  // source: https://stackoverflow.com/a/46536544
  std::ifstream src;
  std::ofstream dst;
  src.open(mm_output[0], std::ios::in | std::ios::binary);
  dst.open(mm_output[1], std::ios::out | std::ios::binary);
  dst << src.rdbuf();
  src.close();
  dst.close();

  // loop on min (0) and mean (1)
  for(int mm=0; mm<2; mm++){

    //-------- set threshold and do things --------
    Conflict_checker_for_pairs_of_voronoi_polygons::threshold = mm_threshold[mm];
    std::cout << "# (mm=" << mm << ") the computed threshold is threshold=" << Conflict_checker_for_pairs_of_voronoi_polygons::threshold << std::endl;

    // build cg and export infos
    //-------- timer resume --------
    rtime.resume();

    //-------- build conflict graph --------
    std::cout << "# (mm=" << mm << ") compute the conflict edges" << std::endl;
    cg.clear();
    vor_cg_builder.build_conflict_graph(cg);

    //-------- timer stop --------
    rtime.stop();

    //--------step2: export the ConflictGraph--------
    std::cout << "## (mm=" << mm << ") export ConflictGraph to " << mm_gv[mm] << " (dot -Tpdf mygraph.gv -o mygraph.pdf)" << std::endl;
    export_conflict_graph_graphviz(cg, mm_gv[mm]);

    //--------step3: information of Conflict Graph -------------------
    std::cout << "## (mm=" << mm << ") ConflictGraph infos in " << mm_output[mm] << std::endl;
    std::cout << "## caution: running for mean is wrong (includes the running time for min)" << std::endl;

    // open file
    std::ofstream ofs;
    ofs.open(mm_output[mm], std::ofstream::app);

    // write running time
    // "user" see https://theboostcpplibraries.com/boost.timer 
    // "seconds" see https://stackoverflow.com/a/17320036
    typedef boost::chrono::duration<double> sec;
    sec seconds = boost::chrono::nanoseconds(rtime.elapsed().user);
    ofs << "running_time_build_cg: " << seconds.count() << "\n"; // time in seconds

    // write parameters
    //ofs << "nshift: " << T_Voronoi_based_conflict_graph_builder<void>::nshift << "\n";
    ofs << "num_c: " << T_Voronoi_based_conflict_graph_builder<void>::num_c << "\n";
    ofs << "radius: " << T_Voronoi_based_conflict_graph_builder<void>::radius << "\n";
    ofs << "multi: " << multi << "\n";
    //ofs << "min_dist_threshold: " << T_Voronoi_based_conflict_graph_builder<void>::min_dist_threshold << "\n";
    ofs << "threshold: " << Conflict_checker_for_pairs_of_voronoi_polygons::threshold << "\n";
    //ofs << inter.at(i) << ": " << set_threshold.at(i) << "\n";

    // write basic info to output file
    ofs << "number_of_main_vertices: " << boost::num_vertices(cg.main_g) << "\n";
    ofs << "number_of_main_edges: " << boost::num_edges(cg.main_g) << "\n";
    ofs << "average_number_of_color_per_vertices: " << ( boost::num_vertices(cg.main_g)==0 ? 0 : (double)boost::num_vertices(cg.conflict_g) / (double)boost::num_vertices(cg.main_g) ) << "\n";
    ofs << "average_number_of_conflict_edges_per_edge: " << ( boost::num_edges(cg.main_g)==0 ? 0 : (double)boost::num_edges(cg.conflict_g) / (double)boost::num_edges(cg.main_g) ) << "\n";

    // empty line
    ofs << "\n";

    // close
    ofs.close();
  }
}

//-------- (optional) export polygons to tikz -------------------

// parse --polytikz string
if (vm.count("polytikz")) {
  std::string filenametikz = vm["polytikz"].as<std::string>();
  std::cout << "## export polygons in tikz to filenametikz=" << filenametikz << std::endl;
  vor_cg_builder.export_all_polygons_tikz(cg, filenametikz);
}

// done
return 0;
}
