#include <iostream>
#include <string>
#include <fstream> 
#include <stdlib.h> // rand

// Command line arguments
#include <boost/program_options.hpp>
namespace po = boost::program_options;


// arguments:
// * length int: the length of the grid 
// * width int: the width of the grid 
// * cin string: .cin file contains points 
int main(int argc, char* argv[]){
    //--------step 0: parse command line arguments-------------------
    
    // Declare the supported options
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "produce help message")
    ("length", po::value<int>() , "the length of the grid")
    ("width", po::value<int>() , "the width of the grid")
    ("cin", po::value<std::string>(), ".cin file to save the points");
   
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    // help
    if(vm.count("help")) {
    std::cout << desc << "\n";
    return 0;
    }

    // length
    int l;
    if (vm.count("length")) {
        l = vm["length"].as<int>();
        std::cout << "length=" << l << std::endl;
    } else {
        std::cout << "the length was not given. --help for info" << std::endl;
        exit(0);
    }

    // width
    int w;
    if (vm.count("width")) {
        w = vm["width"].as<int>();
        std::cout << "width=" << w << std::endl;
    } else {
        std::cout << "the width was not given. Stop" << std::endl;
        exit(0);
    }
    
    // .cin file
    std::string filenamecin;
    if (vm.count("cin")) {
        filenamecin = vm["cin"].as<std::string>();
        std::cout << "cin=" << filenamecin << std::endl;
    } else {
        std::cout << "cin was not given. Stop" << std::endl;
        exit(0);
    }

     //--------step 1: generate points-------------------

    // open file
    std::ofstream ofs;
    ofs.open(filenamecin);

    std::cout << "generate points in the grid (coordinates is multiple of 10)" << std::endl;
    for(int i=0; i<l; ++i){
        for(int j = 0; j<w; ++j){
            ofs << 10*i << " " << 10*j << "\n"; 
        }
    }
    // close file
    ofs.close();
}