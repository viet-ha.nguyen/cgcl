#include <iostream>
#include <string>
#include <fstream> 
#include <stdlib.h> // rand

// Command line arguments
#include <boost/program_options.hpp>
namespace po = boost::program_options;

// arguments:
// * -N int = number of points
// * --range int = coordinates are integers in the range from 0 to r-1
// * --cin string = .cin file contains the points
int main(int argc, char* argv[]){
    //--------step 0: parse command line arguments-------------------
    
    // Declare the supported options
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "produce help message")
    ("N", po::value<int>() , "number of points")
    ("range", po::value<double>() , "range r by r which points are inside")
    ("cin", po::value<std::string>(), ".cin file to save the points");
   
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    // help
    if(vm.count("help")) {
        std::cout << desc << std::endl;
        exit(0);
    }

    // n
    int n;
    if (vm.count("N")) {
        n = vm["N"].as<int>();
        std::cout << "N=" << n << std::endl;
    } else {
        std::cout << "N was not given. --help for info" << std::endl;
        exit(0);
    }

    // range
    int r;
    if (vm.count("range")) {
        r = vm["range"].as<double>();
        std::cout << "range=" << r << std::endl;
    } else {
        std::cout << "range was not given. Stop" << std::endl;
        exit(0);
    }

    // .cin file
    std::string filenamecin;
    if (vm.count("cin")) {
        filenamecin = vm["cin"].as<std::string>();
        std::cout << "cin=" << filenamecin << std::endl;
    } else {
        std::cout << "cin was not given. Stop" << std::endl;
        exit(0);
    }
    
    //--------step 1: generate and print the points-------------------
    // open file
    std::ofstream ofs;
    ofs.open(filenamecin);

    std::srand(time(NULL));
    // generate points
    for(int i=0; i<n; ++i){
        ofs << std::rand() % r << " " << std::rand() % r << "\n"; 
        //std::cout << std::rand() % r << " " << std::rand() % r << std::endl;
    }

    // close file
    ofs.close();
}