Construct ConfligtGraph from Voronoi diagram
--------------------------------------------

Takes as input a set of points (.cin file in data/ folder)
and constructs a ConflictGraph from the Delaunay triangulation (as contact graph)
and Voronoi diagram (with random variations to compute the conflict according to overlapping areas).

# Compile and run (Qt5 required)

```
cmake -DCGAL_DIR=$HOME/path/to/CGAL-5.2
make
./test-for-voronoi-polygons --num_c 50 --radius 1 --min_dist_threshold -1 --threshold 5 --cin data/mydata_Point_2.cin --gv myConflictGraph.gv --output myOutput.txt
```

In the folder data/ there are two programs to generate:
* l by w grids: ``./generate_Grid_2.o --length --width --cin``
* n random points in [0,range-1] times [0,range-1]: ``./generate_random_Point_2.o -N --range --cin``
where ``--cin`` the filename .cin contains points 

# Construction of the Conflict Graph

CG = Conflict Graph  
DT = Delauney Triangulation  
VD = Voronoi Diagram

Command line arguments:
* ``--num_c int`` is the number of colors (random polygons) corresponding to each VD::cell
* ``--radius double`` is the radius for random points around VD::cell point
* ``--min_dist_threshold double`` the minimum distance of 2 VD::cell points (otherwise 2 points are merged to one)
* ``--threshold double`` is the threshold for the size of intersection of two VD::cell in conflict
* ``--cin string`` filepath to the data .cin file containing 2D DT points
* ``--gv string`` filepath to the .gv file where the conflict graph will be written
* ``--output string`` filepath to the .txt file to put informations on the conflict graph

Recall that the ``cg_builder`` requires:
* a graph
* a map from vertex to its conformation container

1. Compute the DT
2. Create the graph:  
  * add the vertices
    Each DT::vertex not adjacent to the infinite vertex will correspond to a CG::vertex
    (DT::vertex adjacent to the infinite vertex has infinite VD::cell)
  * add the edges of DT between the vertices 
3. For each each VD::cell, compute ``num_c`` random polygons where each of its points is in radius r (or smaller) from the original point of VD::cell
  (using ``CGAL::Random_points_in_disc_2``)). And these random polygons are stored in ``std::list<Polygon_2> conformations``.
  * If two points are very very close (< ``min_dist_threshold``) then the second one is discard.
  * If two points are very close (< ``2*radius``) then the radius of the random point is set to distance/2.
  * ASSUMPTION: we never have three points at distance < ``min_dist_threshold``.  
  Each time one VD::cell point is satisfied, a new random point is added to every polygons in ``std::list<Polygon_2> conformations`` to update this polygon.
4. Call the ``cg_builder``, which computes the size of the intersections and compares it with threshold
  (with ``bool operator()(Polygon_2 poly1, Polygon_2 poly2)`` from ``Conflict_checker_for_pairs_of_voronoi_polygons``)
5. Export the ConflictGraph in dot format to the ``.gv`` file
  (compile to pdf with ``dot -Tpdf myConflictGraph.gv -o myConflictGraph.gv.pdf``)
6. Print some information about the ConflictGraph to the ``.txt`` file (such as number of vertices, etc)

### Remark: 
There are 2 versions of creating random polygons for each VD::cell where
* In version 1, ``num_c`` is given (all DT::vertex have the same number of random polygons in ``std::list<Polygon_2> conformations``).
* In version 2, each DT::vertex has nshift to the power degree of this DT::vertex.
In the program, we use the first version which was above explained in 3. For the version 2, the explanations is the following.

3.(a) For each point of each VD::cell compute ``nshift`` random points in radius ``r`` (or smaller)
(using ``CGAL::Random_points_in_disc_2``) and store them in ``map_DTvertex_2_vecPoints``.
  * If two points are very very close (< min_dist_threshold) then the second one is discard.
  * If two points are very close (< 2*radius) then the radius of the random point is set to distance/2.
  * ASSUMPTION: we never have three points at distance < min_dist_threshold.  

3.(b)  For each DT::vertex corresponding to a CG::vertex and each color,
compute the corresponding Polygon_2 (stored in ``std::list<Polygon_2> conformations``)

#### About run with version 2
```
./test-for-voronoi-polygons --nshift 2 3 --radius 1 --min_dist_threshold -1 --threshold 5 --cin data/mydata_Point_2.cin --gv myConflictGraph.gv --output myOutput.txt
```

# Code architecture

#### ``Conflict_graph_builder.hpp``
Construct a conflict graph given three arguments: 
* A (boost) ``Graph``,
* A ``std::map`` from a vertex to its list of conformations,
* An empty ``ConflictGraph`` (to be constructed).
And a ``Conflict_checker``, in a class containing a method:
* ``bool operator()(Conformation_type conformation1, Conformation_type conformation2);``
See ``/include/readme.md``

#### ``Voronoi_based_conflict_graph_builder.hpp``
Contains:
* ``Conflict_checker_for_pairs_of_voronoi_polygons``
* ``My_traits_for_conformations_which_are_voronoi_polygons``
* ``CG_for_voronoi_polygons``
* ``T_Voronoi_based_conflict_graph_builder``
  * A class with ``static`` members:
    * ``nshift``, ``num_c``, ``radius``, ``min_dist_threshold``
    * ``dt`` (the DT constructed in the main from data file)
    * ``map_c`` (map from ``Graph_vertex_desc`` to ``std::list<Polygon_2>`` saved here for ``export_coloring_tikz``)
  * And the main code to construct the ``ConflictGraph`` (procedure described above) in
    * ``build_conflict_graph(ConflictGraph&)``

  
##### ``build_random_polygon_ncolor``
This is the version 1 to create random polygons, used in the main code to construct the ``ConflictGraph``. It requires three arguments:
* a ``std::map`` from vertex_handle (of DT::vertex) to ``Graph_vertex_desc``
* ``int`` the number of colors (= random polygons) 
* a ``std::map`` from ``Graph_vertex_desc`` to list of conformations (to be constructed)
      
##### ``build_random_polygon_nshift`` 
This is the version 2 to create random polygons with ``nshift``. It requires two arguments:
* a ``std::map`` from ``DT::Vertex_handle`` to ``Graph_vertex_desc``,
* a ``std::map`` from  ``Graph_vertex_desc`` to list of conformations (to be constructed)


##### ``export_coloring_tikz``
This function is included in ``Voronoi_based_conflict_graph_builder.hpp``, it exports a coloring as polygons in tikz. It requires three arguments:
* a ConflictGraph with ``.color`` set to a coloring,
* a ``std::map`` from vertex descriptor to list of conformations (the one constructed in ``build_conflict_graph``),
* a ``.tex`` filename.
The tikz can then be compiled to pdf with ``pdflatex filename.tex``.

#### ``test-for-voronoi-polygons.cpp``
Includes ``Voronoi_based_conflict_graph_builder.hpp`` and:
1. parse command line arguments
2. compute the DT from ``data.cin`` file, stores it in ``T_Voronoi_based_conflict_graph_builder<void>::dt``
3. the core part is
```
//build conflict graph
T_Voronoi_based_conflict_graph_builder<void> vor_cg_builder;
ConflictGraph cg;
vor_cg_builder.build_conflict_graph(cg);
```
4. export ``cg`` as ``.gv`` 
5. print some basic infomation about ConflictGraph ``cg`` to the ``.txt`` (output) file.
