

# I am assuming the install of some recent version of CGAL, say CGAL-5.2
https://github.com/CGAL/cgal/releases/tag/v5.2


# Duality between a Delaunay triangulation (DT) and a Voronoi diagram (VD)

* illustration
https://scicomp.stackexchange.com/questions/771/how-are-the-voronoi-tesselation-and-delaunay-triangulation-problems-duals-of-eac

* example CGAL program 
https://doc.cgal.org/latest/Voronoi_diagram_2/classCGAL_1_1Voronoi__diagram__2.html

example file defining / using a Voronoi diagram in 2D: 
CGAL-5.2/examples/Voronoi_diagram_2/vd_2_point_location.cpp

# Outline of the steps to build a conflict graph using geometric shapes in 2D

1. build DT: the 1-skeleton of DT define the adjacencies / the conflict graph

2. build the dual VD directly from the DT (option 1)
* for each vertex of the DT: iterator over incident triangles, and get the dual point dt.dual(face f) 
to obtain the vertex of the Voronoi diagram
* store this into a Polygon_2

2. build the dual VD (option 2)
a. build the VD
b. use the functions  faces_begin() and faces_end() to obtain the faces
/https://doc.cgal.org/latest/Voronoi_diagram_2/classCGAL_1_1Voronoi__diagram__2_1_1Face.html
c. since a face is a polygon: convert it to a polygon_2d
d.  possibly jitter the vertices

3.  Compute the intersection between two  polygons
https://doc.cgal.org/latest/Boolean_set_operations_2/group__boolean__intersection.html

4. To check whether there is a conflict between two (Voronoi)
   polygons: compute the diameter of the intersection. naive
   algorithm: max distance between all pairs of vertices.
   

Algorithm:

1. generate the DT and collect the pairwise contact say Contacts = { (i, j) } : n sub-units
need: DT

2. generate N  conformations for each Voronoi polygon P_i, i = 1,n
needed: random perturbation of the vertices of each polygon. each polygon could also be rotated/translated a bit.

3. For each pair (i, j) in Contacts: check whether the conformations in P_i x P_j are compatible
needed: intersection between 2 polygons (package: boolean operations) + computation of the surface area
of the intersection if any

4. Assemble the traits class for your C++ code
