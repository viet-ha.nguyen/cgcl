#ifndef _Voronoi_based_conflict_graph_builder_hpp_
#define _Voronoi_based_conflict_graph_builder_hpp_

#include "../include/Conflict_graph_builder.hpp"

// math library
#include <cmath>

// CGAL libraries
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
//#include <CGAL/Simple_cartesian.h>

// CGAL libraries (Polygons)
#include <CGAL/point_generators_2.h>
#include <CGAL/squared_distance_2.h>  // for distance
#include <CGAL/Polygon_2.h>
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/Boolean_set_operations_2.h> // for intersection
#include <CGAL/Polygon_2_algorithms.h> // for intersection
//#include <CGAL/draw_polygon_2.h>

// CGAL libraries (Delauney)
#include <CGAL/Delaunay_triangulation_2.h>
//#include <CGAL/draw_triangulation_2.h>

// Command line arguments
#include <boost/program_options.hpp>
namespace po = boost::program_options;

// Timer
#include <boost/timer/timer.hpp>
#include <boost/chrono/chrono.hpp>

// Boost graph info
typedef adjacency_list<setS, vecS, undirectedS> Graph; // no parallel edges
typedef graph_traits<Graph>::vertex_descriptor Graph_vertex_desc;
typedef graph_traits<Graph>::vertex_iterator Graph_vertex_iter;
typedef graph_traits<Graph>::edge_descriptor Graph_edge_desc;
typedef graph_traits<Graph>::edge_iterator Graph_edge_iter;

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
// with the two types below CGAL::intersection fails like this https://stackoverflow.com/questions/42009418/
//typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
//typedef CGAL::Simple_cartesian<double> C;

// type definitions (Polygons)
typedef K::Point_2                     Point_2;
typedef K::Segment_2                   Segment_2;
typedef K::Vector_2                    Vector_2;
typedef CGAL::Polygon_2<K>             Polygon_2;
typedef CGAL::Polygon_with_holes_2<K>  Polygon_with_holes_2;

// type definitions (Delauney, Voronoi)
typedef CGAL::Delaunay_triangulation_2<K> DT;

// Conflict if the area of the intersection of 2 conformations (voronoi polygons) exceeds the threshold 
class Conflict_checker_for_pairs_of_voronoi_polygons{
  public:

  static double threshold; // thereshold for the size of intersection of two voronoi polygons

    bool operator()(Polygon_2 poly1, Polygon_2 poly2){
      CGAL::Polygon_2<CGAL::Epeck>::FT iarea;
      std::list<Polygon_with_holes_2> ipoly;
      // compute the area of the intersection
      CGAL::intersection(poly1, poly2, std::back_inserter(ipoly));
      iarea = 0;
      for(auto iter = ipoly.begin(); iter!=ipoly.end(); iter++){
        iarea += iter->outer_boundary().area();
      }
      // check if it reaches the conflict threshold
      return iarea >= threshold;
    }
};
double Conflict_checker_for_pairs_of_voronoi_polygons::threshold = 1;


// class Trait
class My_traits_for_conformations_which_are_voronoi_polygons{
  public:
    typedef Conflict_checker_for_pairs_of_voronoi_polygons Conflict_checker;
    typedef Graph Main_graph_type;
    typedef std::list<Polygon_2> Conformation_container;
    typedef std::map<Graph_vertex_desc,Conformation_container> Map_Vertex_descriptor_2_Conformation_Container;
};

// ConflictGraph_builder
typedef T_ConflictGraph_builder<My_traits_for_conformations_which_are_voronoi_polygons> CG_for_voronoi_polygons;

// Nb: the template T could be eg a checker for the conflict
template <class T>
class T_Voronoi_based_conflict_graph_builder
{
  public:
  static int nshift;
  static int num_c;
  static double radius;
  static double min_dist_threshold;
  static DT dt; // the DT is constructed in the main (from data file)

  // maps from DT vertices to G vertices
  // saved here for min_intersections 
  static std::map<DT::Vertex_handle,vertex_desc> map_DTvertex_2_Gvertex;

  // map from Graph_vertex_desc to std::list<Polygon_2>
  // saved here for export_coloring_tikz
  static std::map<Graph_vertex_desc,std::list<Polygon_2>> map_c;
  // save main graph 
  static Graph g; 

  public:
  void build_main_graph();
  void build_conflict_graph(ConflictGraph&);
  void build_random_polygon_ncolor();
  //void build_random_polygon_nshift();
  
  void export_coloring_tikz(ConflictGraph, std::string);
  void export_all_polygons_tikz(ConflictGraph, std::string);
  void min_intersections(double, std::string, std::array<double,2>&);
};

// Implementation 
//i===============================================================================
//template <class T> int T_Voronoi_based_conflict_graph_builder<T>::nshift = -1;
template <class T> int T_Voronoi_based_conflict_graph_builder<T>::num_c = -1;
template <class T> double T_Voronoi_based_conflict_graph_builder<T>::radius = -1;
//template <class T> double T_Voronoi_based_conflict_graph_builder<T>::min_dist_threshold = -1;
template <class T> DT T_Voronoi_based_conflict_graph_builder<T>::dt;
template <class T> std::map<DT::Vertex_handle,vertex_desc> T_Voronoi_based_conflict_graph_builder<T>::map_DTvertex_2_Gvertex;
template <class T> std::map<Graph_vertex_desc,std::list<Polygon_2>> T_Voronoi_based_conflict_graph_builder<T>::map_c;
template <class T> Graph T_Voronoi_based_conflict_graph_builder<T>::g;

template <class T>
void T_Voronoi_based_conflict_graph_builder<T>::build_main_graph(){
  // useful variables
  DT::Vertex_iterator vi, vi_end;
  DT::Vertex_circulator vc, vc_end;
  DT::Vertex_handle v1, v2;
  DT::Edge_iterator ei, ei_end;
  //DT::Face_circulator fc, fc_end;
  
  int c;
  Graph_vertex_desc vdesc;
  double dist_prev, dist_next, dist_min, s;

  std::cout << "# compute vertices of the main graph" << std::endl;
  //
  // if vertex v is on the boundary (adjacent to the infinite vertex)
  // then its Voronoi cell is infinite and we will remove it
  std::list<DT::Vertex_handle> list_DTvertex; // will contain the selected vertices
  // 1. add all vertices to the list
  vi = dt.vertices_begin();
  vi_end = dt.vertices_end();
  for(; vi != vi_end; ++vi){
    list_DTvertex.push_back(vi);
  }
  // 2. remove the vertices adjacent to the infinite vertex
  vc = dt.incident_vertices(dt.infinite_vertex());
  vc_end = vc;
  do{
    list_DTvertex.remove(vc);
  }while(++vc != vc_end);
  // 3. add vertices from the list to g
  for(auto iter=list_DTvertex.begin(); iter!=list_DTvertex.end(); ++iter){
    map_DTvertex_2_Gvertex[*iter] = add_vertex(g);
  }
 
  //----------add the edges (of DT)----------
  std::cout << "# compute edges of the main graph" << std::endl;
  //
  // each edge of the DT corresponds to an edge in the main graph 
  ei = dt.edges_begin();
  ei_end = dt.edges_end();
  for(; ei != ei_end; ++ei){
    v1 = (ei->first)->vertex(DT::cw(ei->second)); 
    v2 = (ei->first)->vertex(DT::ccw(ei->second)); 
    if(map_DTvertex_2_Gvertex.count(v1) && map_DTvertex_2_Gvertex.count(v2)){
      add_edge(map_DTvertex_2_Gvertex[v1], map_DTvertex_2_Gvertex[v2], g);
    }
  }
}

template <class T>
void T_Voronoi_based_conflict_graph_builder<T>::build_conflict_graph(ConflictGraph& cg){
  // create the builder
  CG_for_voronoi_polygons cg_builder;

  // NB: most of the code was here before, it has been moved to the main in .cpp

  // build the ConflictGraph
  cg_builder.build_ConflictGraph(g, map_c, cg);

  // done
  return;
};

// build random polygon conformations: there are two versions as below
// ASSUMPTION: we never have 2 consecutive VD points which are very very close (as 2D grid)


// *********************************************************
// * VERSION 1: with num_c (conformations)
// *********************************************************
// in order to compute intersection; we have to take good random VDpoints as follows
// * compute the min distance (dist_min) from a VD point to all edges of VD cell
// if dis_min < 2*r then the radius of the random point is set to dist_min/2
  
template <class T>
void T_Voronoi_based_conflict_graph_builder<T>::build_random_polygon_ncolor(){
  
  // useful variables
  //DT::Vertex_iterator vi, vi_end;
  DT::Edge_circulator ec, ec_end;
  DT::Vertex_handle v;
  DT::Face_circulator fc, fc_end;
  K traits;
  Point_2 p, p_new;
  Segment_2 seg;
  int c;
  Graph_vertex_desc vdesc;
  double dist_next, dist_min, s;

  // conformation container
  std::list<Polygon_2> conformations;

  // random point generator (for smaller radius we multiply by a scalar)
  CGAL::Random_points_in_disc_2<Point_2,CGAL::Creator_uniform_2<double,Point_2>> pointgen(radius);

  // map a Gvertex to its list of VDpoints
  std::map<Graph_vertex_desc, std::list<Point_2>> map_Gvertex_2_VDpoints;
  // list of VDpoints
  std::list<Point_2> VDpoints;

  // construct map Gvertex_2_VDpoints
  for(auto viter = map_DTvertex_2_Gvertex.begin(); viter != map_DTvertex_2_Gvertex.end(); ++viter){
    v = viter->first;
    vdesc = map_DTvertex_2_Gvertex[v];
    // init list of VDpoints to be empty
    VDpoints.clear();
    // construct VDpoints for vdesc
    fc = dt.incident_faces(v);
    fc_end = fc; 
    do{
      VDpoints.push_back(dt.dual(fc));
    }while(++fc!=fc_end);
    map_Gvertex_2_VDpoints[vdesc] = VDpoints;
  }
  VDpoints.clear();

  // TODO: merge the for above and below

  // For each DT::vertex in the main graph (not for infinite voronoi cells)
  for(auto viter = map_DTvertex_2_Gvertex.begin(); viter != map_DTvertex_2_Gvertex.end(); ++viter){
    v = viter->first;
    vdesc = map_DTvertex_2_Gvertex[v];
    // init conformations with empty polygons
    conformations.clear();
    for(c=0; c<num_c; ++c){
      conformations.push_back(Polygon_2());
    }
    // create all polygons simultaneously, VDpoint after VDpoint
    // for each VDpoint, compute min_dist to take num_c random points
    VDpoints = map_Gvertex_2_VDpoints[vdesc];
    for(auto iter=VDpoints.begin(); iter!=VDpoints.end();++iter){
      p = *iter;
      // init dist_min the min distance of p to other VD points in the VD cell
      dist_min = 2*radius;
      ec = dt.incident_edges(v);
      ec_end = ec;
      // compute dist_min of point p to all VD edges
      do{
        // take VD segment corresponding to DT edge ec
        seg = CGAL::object_cast<Segment_2>(dt.dual(ec));
        dist_next = sqrt(CGAL::to_double(CGAL::squared_distance(seg,p)));
        if(dist_next < dist_min && dist_next !=0){
          dist_min = dist_next;
          //std::cout << "current dist_min is " << dist_min << std::endl;
        }
      }while(++ec != ec_end);
      // compute scaling ratio
      s = dist_min/(2*radius); // 1 = 2*radius
      // update polygons in conformations: add a new random point to every polygon
      for(auto piter = conformations.begin(); piter != conformations.end(); ++piter){
        Polygon_2& poly = *piter;
        do{
          p_new = p + s * Vector_2(Point_2(0,0),*pointgen);
          ++pointgen;
        }while(CGAL::bounded_side_2(VDpoints.begin(), VDpoints.end(),p_new, traits)==CGAL::ON_BOUNDED_SIDE);
        poly.push_back(p_new);
      }
    }
    map_c[vdesc] = conformations;

    // TMP: check that the polygons are simple, and if they are not, print infos
    for(auto piter = conformations.begin(); piter != conformations.end(); ++piter){
      Polygon_2 tmppoly = *piter;
      if(!CGAL::is_simple_2(tmppoly.begin(), tmppoly.end(), K())){
        std::cout << "--------found a polygon not simple --------" << std::endl;
        std::cout << "VD cell=" << Polygon_2(VDpoints.begin(),VDpoints.end(),K()) << std::endl; // convert VDpoints to Polygon_2 for easy cout
        std::cout << "random not simple=" << tmppoly << std::endl;
        std::cout << "-------------------------------------------" << std::endl;
      }
    }
  }
 
}

// *********************************************************
// * VERSION 2: with nshift**degree polygons (conformations)
// *********************************************************
// build random polygons with nshift

/*
 *  NEEDS TO BE UPDATED (CLASS ATTRIBUTES HAVE CHANGED)
 */

/*
template <class T>
void T_Voronoi_based_conflict_graph_builder<T>::build_random_polygon_nshift(){
  // useful variables
  DT::Vertex_handle v;
  DT::Face_circulator fc, fc_end;
  Point_2 p, p_prev, p_next;
  std::vector<std::vector<Point_2>> vecPoints;
  std::vector<Point_2> points;
  std::vector<int> cbase;
  int d, i, c, num_c;
  Polygon_2 poly;
  Graph_vertex_desc vdesc;
  double dist_prev, dist_next, dist_min, s;

  //--------for each DT::vertex compute the points of its VD::polygon--------
  std::cout << "# compute the random points for polygons" << std::endl;
  //
  // the datastructure map_DTvertex_2_vecPoints records the random points for each polygon
  // each DT::vertex is mapped to a polygon (vector of points)
  // where for each polygon point we have (a vector of) nshifts random points
  std::map<DT::Vertex_handle,std::vector<std::vector<Point_2>>> map_DTvertex_2_vecPoints;
  std::map<DT::Vertex_handle,int> map_DTvertex_2_degree;

  // random point generator (for smaller radius we multiply by a scalar)
  CGAL::Random_points_in_disc_2<Point_2,CGAL::Creator_uniform_2<double,Point_2>> pointgen(radius);

  // For each DT::vertex in the main graph (not for infinite voronoi cells)
  for(auto iter = map_DTvertex_2_Gvertex.begin(); iter != map_DTvertex_2_Gvertex.end(); ++iter){
    v = iter->first;
    d = 0; // count the degree of v
    // for each DT::Face (~= polygon points) of v
    // initialization of p_prev and p
    fc = dt.incident_faces(v);
    p_prev = dt.dual(fc);
    p = dt.dual(++fc);
    dist_prev = CGAL::to_double(CGAL::squared_distance(p,p_prev));
    if(dist_prev < min_dist_threshold){ // ASSUMPTION
      ++fc; // move to the next point and keep p_prev
    }
    fc_end = fc; // ASSUMPTION
    // loop
    do{
      // get the dual VD point
      p = dt.dual(fc);
      p_next = dt.dual(++fc);
      if(CGAL::to_double(CGAL::squared_distance(p,p_next)) < min_dist_threshold){ // ASSUMPTION
        p_next = dt.dual(++fc); // move p_next to the next point
      }
      // get the distance between p and p_prev
      dist_prev = CGAL::to_double(CGAL::squared_distance(p,p_prev));
      // if dist is less than min_dist_threshold, then discard p
      if(dist_prev < min_dist_threshold){continue;}
      // get the distance between p and p_next
      dist_next = CGAL::to_double(CGAL::squared_distance(p,p_next));
      // compute the scaling factor for random point radius
      dist_min = std::min(dist_prev,dist_next);
      if(dist_min >= 2*radius){
        s = 1; // not too close
      }
      else{
        s = dist_min/(2*radius);// too close => set the radius to half the distance = scale the random point by s
      }
      // add nshift random (uniform) points
      points.clear();
      for(i=0; i<nshift; ++i){
        points.push_back(p + s * Vector_2(Point_2(0,0),*pointgen));
        ++pointgen;
      }
      // increase degree, record map, prepare next iteration
      d++;
      map_DTvertex_2_vecPoints[v].push_back(points);
      p_prev = p;
    }while(fc != fc_end); // ASSUMPTION
    // DT::vertex done, record degree
    map_DTvertex_2_degree[v] = d;
  }

  //--------compute all polygons--------
  std::cout << "# compute all polygons" << std::endl;
  //
  // ( DT::Vertex + color_number ) -> Polygon_2
  // with color_number from 0 to 2^degree-1
  std::list<Polygon_2> conformations;
  // for each DT::Vertex in the main graph (not for infinite voronoi cells)
  for(auto iter = map_DTvertex_2_Gvertex.begin(); iter != map_DTvertex_2_Gvertex.end(); ++iter){
    v = iter->first;
    // get the vertex desc of v
    vdesc = map_DTvertex_2_Gvertex[v];
    // setup the conformation container of vdesc
    conformations.clear();
    d = map_DTvertex_2_degree[v];
    num_c = std::pow(nshift,d);
    // cbase is the color number on d bits in base nshift
    cbase.clear();
    for(i=0; i<d; ++i){ // init
      cbase.push_back(0);
    }
    // for each color
    for(c=0; c<num_c; ++c){
      // construct the polygon with the corresponding points from map_DTvertex_2_vecPoints
      poly.clear();
      // for each polygon point
      vecPoints = map_DTvertex_2_vecPoints[v];
      // read the choices given by cbase
      for(i=0; i<d; ++i){
        poly.push_back(vecPoints[i][cbase[i]]); // add the point
      }
      // save the polygon in conformations
      conformations.push_back(poly);
      // increment cbase
      for(i=0; i<d; ++i){
        if(cbase[i]==nshift-1){
          cbase[i]=0;
        }
        else{
          cbase[i]=cbase[i]+1;
          break;
        }
      }
    }
    // add conformations to the map of conformations
    map_c[vdesc] = conformations;
  }
}
*/

// export coloring to tikz
// print a found conflict coloring of a conflict graph to a filenam with tikz
// argument:
// * a conflict graph
// * a map
// * a filename 
template <class T>
void T_Voronoi_based_conflict_graph_builder<T>::export_coloring_tikz(ConflictGraph cg, std::string filename){
  // open file
  std::ofstream texfile;
  texfile.open(filename);
  texfile << "\\documentclass[crop,tikz]{standalone}\n";
  texfile << "\\begin{document}\n";
  texfile << "\\begin{tikzpicture}\n";
  // iter over vertex/polygons
  //OLD (vi,vi_end=vertices(cg.main_g)): for(auto viter = map_DTvertex_2_Gvertex.begin(); viter != map_DTvertex_2_Gvertex.end(); ++viter){
  //OLD (vdesc -> *vi):  vdesc = map_DTvertex_2_Gvertex[viter->first];
  vertex_iter vi, vi_end;
  Polygon_2 poly;
  int c;
  for(tie(vi,vi_end)=vertices(cg.main_g); vi != vi_end; ++vi){
    c = cg.main_g[*vi].color;
    // get the poly from the list from map_c
    poly = *(std::next(map_c[*vi].begin(), c));
    // draw poly
    texfile << "\\draw ";
    for(auto piter = poly.begin(); piter != poly.end(); ++piter){
      texfile << "(" << piter->x() << "," << piter->y() << ") -- " ;
    }
    texfile << "cycle;\n";
    texfile << "\\node at (" << poly.begin()->x() << "," << poly.begin()->y() << ") {v" << *vi << " " << c << "};\n";
  }
  // close file
  texfile << "\\end{tikzpicture}\n";
  texfile << "\\end{document}\n";
  texfile.close();
}

// export voronoi diagram and all polygons to tikz
// argument:
// * a conflict graph
// * a filename 
template <class T>
void T_Voronoi_based_conflict_graph_builder<T>::export_all_polygons_tikz(ConflictGraph cg, std::string filename){
  // open file
  std::ofstream texfile;
  texfile.open(filename);
  texfile << "\\documentclass[crop,tikz]{standalone}\n";
  texfile << "\\begin{document}\n";
  texfile << "\\begin{tikzpicture}\n";

  //-------- part 1: print the voronoi diagram (original polygon) --------

  // loop on the vertices not adjacent to the infinite vertex:
  // for each one, draw the voronoi cell (polygon)

  // 1. compute (again...) the list of finite vertices  
  std::list<DT::Vertex_handle> list_DTvertex;
  // add all vertices to the list
  DT::Vertex_iterator vi, vi_end;
  vi = dt.vertices_begin();
  vi_end = dt.vertices_end();
  for(; vi != vi_end; ++vi){
    list_DTvertex.push_back(vi);
  }
  // remove the vertices adjacent to the infinite vertex
  DT::Vertex_circulator vc, vc_end;
  vc = dt.incident_vertices(dt.infinite_vertex());
  vc_end = vc;
  do{
    list_DTvertex.remove(vc);
  }while(++vc != vc_end);
  
  // 2. loop
  DT::Face_circulator fc, fc_end;
  Point_2 p;
  // For each DT::vertex in the main graph (not for infinite voronoi cells)
  for(auto viter = list_DTvertex.begin(); viter != list_DTvertex.end(); ++viter){
    // draw voronoi cell (polygon)
    fc = dt.incident_faces(*viter);
    fc_end = fc;
    texfile << "\\draw[black,line width = 2mm] ";
    do{
      // get the dual VD point
      p = dt.dual(fc);
      texfile << "(" << p.x() << "," << p.y() << ") -- " ;
    }while(++fc != fc_end);
    texfile << "cycle;\n";
  }

  //-------- part 2: print all conformation polygons --------

  vertex_iter vvi, vvi_end;
  Polygon_2 poly;
  // TODO: get map_c from vor_cg_builder
  int num_c, c;
  // for each vertex
  for(tie(vvi,vvi_end)=vertices(cg.main_g); vvi != vvi_end; ++vvi){
    num_c = cg.main_g[*vvi].num_colors;
    // for each color (conformation polygon)
    for(c=0; c<num_c; ++c){
      // get the poly from the list from map_c
      poly = *(std::next(map_c[*vvi].begin(), c));
      // draw poly
      texfile << "\\draw[blue!" << 100*c/num_c << "!red] ";
      for(auto piter = poly.begin(); piter != poly.end(); ++piter){
        texfile << "(" << piter->x() << "," << piter->y() << ") -- " ;
      }
      texfile << "cycle;\n";
    }
  }
  // close file
  texfile << "\\end{tikzpicture}\n";
  texfile << "\\end{document}\n";
  texfile.close();
}

// compute min_intersections (edge, face, star)
// newthreshold  = [max min_threshold, max mean_threshold]
template <class T>
void T_Voronoi_based_conflict_graph_builder<T>::min_intersections(double step, std::string filename, std::array<double,2>& newthreshold){
  // write min_intersection in yaml
  std::ofstream ofs;
  ofs.open(filename, std::ofstream::app);
  ofs << "min_intersections: " << step << "\n";

  // useful variables
  edge_iter ei, ei_end;
  out_edge_iter oei, oei_end;
  vertex_iter vi, vi_end;
  vertex_desc vdesc1, vdesc2, vdesc3;
  double min_size, max_size, min_size_edge, sum_size, mean_size;
  int index, index_mean;
  CGAL::Polygon_2<CGAL::Epeck>::FT iarea;
  std::list<Polygon_with_holes_2> ipoly;

  //double newthreshold=0;// to be returned, it is max of of min_size over edge/face/star

  // ---------------------- edge --------------------------
  // for any edge (u,v) in main: compute the min size and average (mean) of their intersection (for any couple of colors)
  // compute mean by computing sum_size (sum of all) then dividing number of colors of u * of v
  std::vector<int> vec_edge(1,0);
  std::vector<int> vec_edge_mean(1,0);
  for(tie(ei, ei_end)=edges(g); ei!=ei_end; ++ei){
    vdesc1 = source(*ei, g); //source(*ei,cg.main_g);
    vdesc2 = target(*ei, g); //target(*ei,cg.main_g);
    // init mean intersection mean_size
    sum_size = 0;
    // init min intersection size min_size 
    ipoly.clear();
    CGAL::intersection(map_c[vdesc1].front(), map_c[vdesc2].front(), std::back_inserter(ipoly));
    iarea = 0;
    for(auto iter = ipoly.begin(); iter!=ipoly.end(); iter++){
      iarea += iter->outer_boundary().area();
    }
    min_size = CGAL::to_double(iarea);

    // compute min intersection size
    for(auto ci1=map_c[vdesc1].begin(); ci1!=map_c[vdesc1].end() ; ++ci1){
      for(auto ci2=map_c[vdesc2].begin(); ci2!=map_c[vdesc2].end() ; ++ci2){
        ipoly.clear();
        CGAL::intersection(*ci1, *ci2, std::back_inserter(ipoly));
        iarea = 0;
        for(auto iter = ipoly.begin(); iter!=ipoly.end(); iter++){
          iarea += iter->outer_boundary().area();
        }
        // update sum_size
        sum_size = sum_size + CGAL::to_double(iarea);
        // update min_size
        if(min_size > iarea){
          min_size = CGAL::to_double(iarea);
        }
      }
    }
    // compute mean size
    mean_size = sum_size/std::pow(num_c,2);
    // add value to vec_edge_mean
    index_mean = std::floor(mean_size/step);
    if(index_mean >= vec_edge_mean.size()){
      vec_edge_mean.resize(index_mean+1,0);
    }
    ++vec_edge_mean[index_mean];

    // add value to vec_edge
    index = std::floor(min_size/step);
    if(index >= vec_edge.size()){
      vec_edge.resize(index+1,0);
    }
    ++vec_edge[index];

    // update newthreshold
    if (newthreshold.at(0) < min_size){
      newthreshold.at(0) = min_size;
    }
    if (newthreshold.at(1) < mean_size){
      newthreshold.at(1) = mean_size;
    }
  }
  // write to yaml as python list
  ofs << "mean_intersections_edge: [ " << vec_edge_mean[0];
  for( index_mean=1; index_mean < vec_edge_mean.size(); ++index_mean){
    ofs << ", " << vec_edge_mean[index_mean];
  }
  ofs << " ]\n";
  ofs << "min_intersections_edge: [ " << vec_edge[0];
  for( index=1; index < vec_edge.size(); ++index){
    ofs << ", " << vec_edge[index];
  }
  ofs << " ]\n";

  // ---------------------- face --------------------------
  // for any DT face of finite vertices: compute the min size of their intersection and mean_size 
  std::vector<int> vec_face(1,0);
  std::vector<int> vec_face_mean(1,0);

  DT::Finite_faces_iterator fi;
  DT::Vertex_handle v1, v2, v3;
  
  for(fi = dt.finite_faces_begin(); fi!=dt.finite_faces_end();fi++){
    v1 = fi->vertex(0);;
    v2 = fi->vertex(1);
    v3 = fi->vertex(2);
    // if these three vertices of this face fi are in G
    if(map_DTvertex_2_Gvertex.count(v1)!=0 && map_DTvertex_2_Gvertex.count(v2)!=0 && map_DTvertex_2_Gvertex.count(v3)!=0){
      vdesc1 = map_DTvertex_2_Gvertex[v1];
      vdesc2 = map_DTvertex_2_Gvertex[v2];
      vdesc3 = map_DTvertex_2_Gvertex[v3];
      // init sum_size
      sum_size = 0;
      // init min_size
      min_size = CGAL::to_double(map_c[vdesc1].front().area());
      // compute min max intersection size
      for(auto ci1=map_c[vdesc1].begin(); ci1!=map_c[vdesc1].end() ; ++ci1){
        for(auto ci2=map_c[vdesc2].begin(); ci2!=map_c[vdesc2].end() ; ++ci2){
          for(auto ci3=map_c[vdesc3].begin(); ci3!=map_c[vdesc3].end(); ++ci3){
            // compute max intersection size
            // 1 2
            ipoly.clear();
            CGAL::intersection(*ci1, *ci2, std::back_inserter(ipoly));
            iarea = 0;
            for(auto iter = ipoly.begin(); iter!=ipoly.end(); iter++){
              iarea += iter->outer_boundary().area();
            }
            max_size = CGAL::to_double(iarea);
            // 1 3
            ipoly.clear();
            CGAL::intersection(*ci1, *ci3, std::back_inserter(ipoly));
            iarea = 0;
            for(auto iter = ipoly.begin(); iter!=ipoly.end(); iter++){
              iarea += iter->outer_boundary().area();
            }
            if(max_size < CGAL::to_double(iarea)){
              max_size = CGAL::to_double(iarea);
            }
            // 2 3
            ipoly.clear();
            CGAL::intersection(*ci2, *ci3, std::back_inserter(ipoly));
            iarea = 0;
            for(auto iter = ipoly.begin(); iter!=ipoly.end(); iter++){
              iarea += iter->outer_boundary().area();
            }
            if(max_size < CGAL::to_double(iarea)){
              max_size = CGAL::to_double(iarea);
            }
            // update sum_size
            sum_size = sum_size + max_size;
            // update min_size
            if(min_size > max_size){
              min_size = max_size;            
            }
          }
        }
      }
      // compute mean_size
      mean_size = sum_size/std::pow(num_c, 3);
      // add value mean_size to vec_face_mean
      index_mean = std::floor(mean_size/step);
      if(index_mean >= vec_face_mean.size()){
        vec_face_mean.resize(index_mean+1,0);
      }
      vec_face_mean[index_mean]++;

      // add value min_size to vec_face
      index = std::floor(min_size/step);
      if(index >= vec_face.size()){
        vec_face.resize(index+1,0);
      }
      vec_face[index]++;

      // update newthreshold
      if (newthreshold.at(0) < min_size){
        newthreshold.at(0) = min_size;
      }
      if (newthreshold.at(1) < mean_size){
        newthreshold.at(1) = mean_size;
      }
    }
  }
  // write to yaml as python list
  ofs << "mean_intersections_face: [ " << vec_face_mean[0];
  for( index_mean=1; index_mean < vec_face_mean.size(); ++index_mean){
    ofs << ", " << vec_face_mean[index_mean];
  }
  ofs << " ]\n";
  ofs << "min_intersections_face: [ " << vec_face[0];
  for( index=1; index < vec_face.size(); ++index){
    ofs << ", " << vec_face[index];
  }
  ofs << " ]\n"; 
  
  // ---------------------- star --------------------------
  // for any vertex v, look at all its out-edges: compute the min size of their intersection and mean
  // min_size = min_{in all color c of vdesc1} max{in all neighbors v} min{in all color cv of v}(vdesc1,c)(v,cv)
  std::vector<int> vec_star(1,0);
  std::vector<int> vec_star_mean(1,0);
  int count = 0; // count no. neighbors of vdes1

  for(tie(vi, vi_end)=vertices(g); vi!=vi_end; vi++){
    vdesc1 = *vi;
    // init sum_size
    sum_size = 0;
    // init min intersection size min_size
    min_size = CGAL::to_double(map_c[vdesc1].front().area());
    // for each color of vdesc1
    for(auto ci1=map_c[vdesc1].begin(); ci1!=map_c[vdesc1].end() ; ++ci1){
      // init max_size
      max_size = 0;
      // look at all its neighbors
      for(tie(oei, oei_end)=out_edges(vdesc1, g); oei!=oei_end; ++oei){
        // add 1 to count neghbors
        count = count +1;
        vdesc2 = target(*oei,g);
        // init min_size_edge
        ipoly.clear();
        CGAL::intersection(map_c[vdesc1].front(), map_c[vdesc2].front(), std::back_inserter(ipoly));
        iarea = 0;
        for(auto iter = ipoly.begin(); iter!=ipoly.end(); iter++){
          iarea += iter->outer_boundary().area();
        }
        min_size_edge = CGAL::to_double(iarea);
        // compute min_size_edge of edge vdesc1, vdesc2
        for(auto ci2=map_c[vdesc2].begin(); ci2!=map_c[vdesc2].end() ; ++ci2){
          ipoly.clear();
          CGAL::intersection(*ci1, *ci2, std::back_inserter(ipoly));
          iarea = 0;
          for(auto iter = ipoly.begin(); iter!=ipoly.end(); iter++){
            iarea += iter->outer_boundary().area();
          }
          if(min_size_edge > CGAL::to_double(iarea)){
            min_size_edge = CGAL::to_double(iarea);
          }
        }
        // update the max_size
        if(min_size_edge > max_size){
          max_size = min_size_edge;
        }
      }
      // update sum_size
      sum_size = sum_size + max_size;
      // update min_size
      if(max_size < min_size){
        min_size = max_size;
      }
    }
    
    // compute mean_size at vdes1
    mean_size = sum_size/num_c;
    // add value mean_size to vec_star_mean
    index_mean = std::floor(mean_size/step);
    if(index_mean >= vec_star_mean.size()){
      vec_star_mean.resize(index_mean+1,0);
    }
    vec_star_mean[index_mean]++;

    // add value min_size to vec_star
    index = std::floor(min_size/step);
    if(index >= vec_star.size()){
      vec_star.resize(index+1,0);
    }
    vec_star[index]++;

    // update newthreshold
    if (newthreshold.at(0) < min_size){
      newthreshold.at(0) = min_size;
    }
    if (newthreshold.at(1) < mean_size){
      newthreshold.at(1) = mean_size;
    }
  }  
  // write to yaml as python list
  ofs << "mean_intersections_star: [ " << vec_star_mean[0];
  for( index_mean=1; index_mean < vec_star_mean.size(); ++index_mean){
    ofs << ", " << vec_star_mean[index_mean];
  }
  ofs << " ]\n";
  ofs << "min_intersections_star: [ " << vec_star[0];
  for( index=1; index < vec_star.size(); ++index){
    ofs << ", " << vec_star[index];
  }
  ofs << " ]\n";
  // write newthresholds
  ofs << "threshold_min: " << newthreshold.at(0) << "\n";
  ofs << "threshold_mean: " << newthreshold.at(1) << "\n";
  ofs << "\n";
  // close
  ofs.close();
  // return newthreshold
  //return newthreshold;
}


#endif
