#include "../include/Conflict_graph_builder.hpp"

typedef adjacency_list<setS, vecS, undirectedS> Graph; // no parallel edges
typedef graph_traits<Graph>::vertex_descriptor Graph_vertex_desc;

// Conflict if 2 conformations, integers here, are different
class Conflict_checker_for_pairs_of_integers{
  public:
    bool operator()(int a, int b) { return a!=b;}
};

// class Trait
class My_traits_for_conformations_which_are_integers{
  public:
    typedef Conflict_checker_for_pairs_of_integers Conflict_checker;
    typedef Graph Main_graph_type;
    typedef std::vector<int> Conformation_container;
    typedef std::map<Graph_vertex_desc,Conformation_container> Map_Vertex_descriptor_2_Conformation_Container;
};

// ConflictGraph_builder
typedef T_ConflictGraph_builder<My_traits_for_conformations_which_are_integers> CG_for_integers;

// main
int main(){
  // create the builder
  CG_for_integers cg_builder;

  // define the graph and the map
  Graph g; // for cg_builder
  std::map<Graph_vertex_desc,std::vector<int>> map_c; // for cg_builder
  std::map<int,Graph_vertex_desc> map_v; // to add the edges of g
  // add the vertices
  int num_v=10; // 10 vertices
  int num_c=3; // 3 conformations per vertex
  int i, j;
  std::vector<int> conformations; // conformation container
  Graph_vertex_desc v;
  for(i=0; i<num_v; i++){
    // prepare conformations container
    // with three conformations [0,1,2] for each vertex
    conformations.clear();
    for(j=0; j<num_c; j++){
      conformations.push_back(j);
    }
    // add the vertex and update maps
    v = add_vertex(g);
    map_c[v] = conformations;
    map_v[i] = v;
  }
  // add the edges (a cycle)
  for(i=0; i<num_v; i++){
    add_edge(map_v[i], map_v[(i+1)%num_v], g);
  }
  // build the ConflictGraph
  ConflictGraph cg;
  cg_builder.build_ConflictGraph(g,map_c,cg);
  // print the ConflictGraph
  print_conflict_graph(cg);
  // done
  return 0;
}
