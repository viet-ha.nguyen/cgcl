#ifndef COLORING
#define COLORING

#include <iostream>
#include <queue>
#include "datastructure.hpp"
#include "import.hpp"
#include <boost/graph/connected_components.hpp>

//#include "reduction_rules.hpp"
using namespace boost;

bool enum_stop(ConflictGraph&);
bool enum_printAll(ConflictGraph&);

bool conflictColoring_splitcc(ConflictGraph&,bool(*)(ConflictGraph&));

bool conflictColoring_backtrack(ConflictGraph&);
bool conflictColoring_backtrack_cc(ConflictGraph&);
bool conflictColoring_backtrack_enum(ConflictGraph&);
int conflictColoring_backtrack_enum_count(ConflictGraph&);

template<typename some_vertex_iter>
bool conflictColoring_backtrack_enum_rec(some_vertex_iter, some_vertex_iter, ConflictGraph&, bool(*)(ConflictGraph&));

bool conflictColoring_backtrack_degree_global(ConflictGraph&);
bool conflictColoring_backtrack_degree_global_cc(ConflictGraph&);
bool conflictColoring_backtrack_degree_local(ConflictGraph&);
bool conflictColoring_backtrack_degree_local_cc(ConflictGraph&);

int conflictColoring_max(ConflictGraph&);
int conflictColoring_max_rec(int, int, vertex_iter, vertex_iter, ConflictGraph&);

typedef struct Max_coloring{
    std::vector<int> S;
    std::vector<std::map<vertex_desc,int>> C;
} Max_coloring;
int conflictColoring_maxTree(ConflictGraph&);
Max_coloring conflictColoring_maxTree_rec(vertex_desc, vertex_desc, ConflictGraph);

bool conflictColoring_cut_set_cycle(ConflictGraph&);
bool conflictColoring_cut_set_cycle_cc(ConflictGraph&);
bool cut_2_tree(ConflictGraph&);

bool pre_conflictColoring_Forest(ConflictGraph&);
bool pre_conflictColoring_Tree(ConflictGraph&, vertex_desc);
std::vector<bool> pre_conflictColoring_Tree_rec(vertex_desc, vertex_desc, ConflictGraph&);

#endif