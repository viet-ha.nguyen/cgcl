#ifndef DATASTRUCTURE
#define DATASTRUCTURE

#include <vector>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
using namespace boost;

// create typedef for general graph
typedef adjacency_list<setS, vecS, undirectedS> Graph; // no parallel edges
typedef graph_traits<Graph>::vertex_descriptor vertex_desc;
typedef graph_traits<Graph>::vertex_iterator vertex_iter;
typedef graph_traits<Graph>::edge_descriptor edge_desc;
typedef graph_traits<Graph>::edge_iterator edge_iter;
typedef graph_traits<Graph>::out_edge_iterator out_edge_iter;
typedef property_map<Graph, vertex_index_t>::type IndexMap;


// create typedef for main graph
struct MProperty{
    // list of corresponding vertices in the conflict graph
    // the index indicates the color
    std::vector<vertex_desc> conflict_vertices;
    // number of colors
    int num_colors;
    int color; // color in coloring
    int color_max; // used to remember a max-coloring
    //int max_count[num_colors];// used to remember a max coloring for each color in tree
};
typedef adjacency_list<setS, vecS, undirectedS, MProperty> MGraph; // no parallel edges

// create typedef for conflict graph
struct CProperty{
    // corresponding vertex of the main graph
    vertex_desc main_vertex;
    int color_index; // color of the vertex in the conflict graph
    int max_count;// used to remember a max coloring for each color
};
typedef adjacency_list<setS, vecS, undirectedS, CProperty> CGraph; // no parallel edges

class ConflictGraph{
    public:
        // by default main_g and conflict_g are graphs with 0 vertices
        MGraph main_g;
        CGraph conflict_g;
        int coloring_count; // used to store the number of colorings (enum_count)
        // reset the graphs
        void clear(){
            main_g.clear();
            conflict_g.clear();
        }
};

#endif