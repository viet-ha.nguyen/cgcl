#include "coloring.hpp"

// a function to stop the enumeration of solutions,
// used by conflictColoring_backtrack to stop once a solution is found.
bool enum_stop(ConflictGraph& G){
    return true;
}

// a function to print the colorings,
// used by conflictColoring_backtrack_enum to enumerate all solutions.
bool enum_printAll(ConflictGraph& G){
    vertex_iter vi, vi_end;
    std::cout << "conflict-coloring\n";
    for(tie(vi,vi_end)=vertices(G.main_g); vi!=vi_end; ++vi){
        std::cout << "  vertex " << *vi << " has color " << G.main_g[*vi].color << "\n";
    }
    return false;
}

// a function to count the colorings,
// used by conflictColoring_backtrack_enum_count to count all solutions.
bool enum_count(ConflictGraph& G){
    G.coloring_count++;
    return false; // continue the enumeration
}

// split the application of a conflict coloring algorithm
// to the connected components of the graph
// arguments:
// * a ConflictGraph G
// * a conflict coloring algorithm
// returns true if and only if all connected components are conflict-colorable
bool conflictColoring_splitcc(ConflictGraph& G, bool(*conflictColoring_algo_cc)(ConflictGraph&)){
    // useful variables
    vertex_iter vi,vi_end;

    // compute connected components
    std::vector<int> component(num_vertices(G.main_g)); // vertex descriptor -> component number
    int num_components = connected_components(G.main_g, &component[0]);

    // call conflictColoring_algo_cc on each connected component
    ConflictGraph Gcc;
    std::unordered_set<vertex_desc> vset;
    std::map<vertex_desc,vertex_desc> map_main2sub;
    for(int cc=0; cc<num_components; ++cc){
        // reset output variables
        Gcc.clear();
        map_main2sub.clear();
        // prepare set of vertices
        vset.clear();
        for(tie(vi,vi_end)=vertices(G.main_g); vi!=vi_end; ++vi){
            if(component[*vi]==cc){
                vset.insert(*vi);
            }
        }
        // create the graph Gcc of connected component cc
        create_subgraph(G,vset,Gcc,map_main2sub);
        // call conflictColoring_algo_cc
        if(conflictColoring_algo_cc(Gcc)){
            // copy the colors of Gcc to G
            for(auto it=map_main2sub.begin(); it!=map_main2sub.end(); ++it){
                G.main_g[it->first].color = Gcc.main_g[it->second].color;
            }
        }
        else{
            // no coloring of this cc => no coloring of G
            return false;
        }
    }
    return true;
}

// split the call of conflictColoring_backtrack_cc on connected components
bool conflictColoring_backtrack(ConflictGraph& G){
    return conflictColoring_splitcc(G,conflictColoring_backtrack_cc);
}

// find a conflict edge coloring using backtrack algorithm (init part)
// arguments :
// * a ConflictGraph
// returns:
// * true if a coloring has been found,
//   and sets .color property of vertices in main graph to the found conflict coloring
// * false if no coloring has been found,
//   and reset .color to -1
bool conflictColoring_backtrack_cc(ConflictGraph& G){
    // init colors
    vertex_iter vi, vi_end;
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        G.main_g[*vi].color = -1;
    }
    // init vertex iterator
    tie(vi,vi_end)=vertices(G.main_g);
    // start
    return conflictColoring_backtrack_enum_rec(vi,vi_end,G,enum_stop);
}

// TODO: split the enumeration to connected components.
// enumerate (print) all conflict colorings using backtrack algorithm (init part)
// arguments :
// * a ConflictGraph
// returns:
// * false and resets all .color to -1
bool conflictColoring_backtrack_enum(ConflictGraph& G){
    // init colors
    vertex_iter vi, vi_end;
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        G.main_g[*vi].color = -1;
    }
    // init vertex iterator
    tie(vi,vi_end)=vertices(G.main_g);
    // start
    return conflictColoring_backtrack_enum_rec(vi,vi_end,G,enum_printAll);
}

// enumerate (count) all conflict colorings using backtrack algorithm (init part)
// arguments :
// * a ConflictGraph
// returns:
// * the number of colorings
int conflictColoring_backtrack_enum_count(ConflictGraph& G){
    // init coloring_count
    G.coloring_count = 0;
    // init colors
    vertex_iter vi, vi_end;
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        G.main_g[*vi].color = -1;
    }
    // init vertex iterator
    tie(vi,vi_end)=vertices(G.main_g);
    // start
    conflictColoring_backtrack_enum_rec(vi,vi_end,G,enum_count);
    return G.coloring_count;
}

// find a conflict coloring using backtrack algorithm (recursive part)
// arguments:
// * vertex iterator (current)
// * vertex iterator end
// * the ConflictGraph
// * a function taking as input the conflict graph, and doing something with the solution (in .color property),
//   returning true then the enumeration stops, or false then the enumeration continues.
// returns:
// * true if a coloring has been found,
//   and sets .color property of vertices in main graph to the valid conflict coloring
// * false if no coloring has been found,
//   and reset .color to -1
// the iterators are generic, because we may follow the order of a boost graph iterator, or a custom vector
template<typename some_vertex_iter = vertex_iter>
bool conflictColoring_backtrack_enum_rec(some_vertex_iter vi, some_vertex_iter vi_end, ConflictGraph& G, bool(*f)(ConflictGraph& G)){
    // useful variables
    vertex_desc conflict_v;
    out_edge_iter oei, oei_end;
    vertex_desc conflict_u;
    vertex_desc main_u;
    int color_u;
    bool conflict = false;
    
    // check if we have reached the end
    if(vi==vi_end){
        // all vertices have color and no conflict: success!
        return f(G); // returns true -> stop, false -> continue
    }

    // try all colors for the current vertex, check conflict, and make recursive call
    for (int i = 0; i < G.main_g[*vi].num_colors; ++i){
        // set the color
        G.main_g[*vi].color = i;
        // check conflict out_edges (vertices not set yet have color -1)
        conflict = false;
        // get (v,i) from conflict graph
        conflict_v = G.main_g[*vi].conflict_vertices[i];
        for(tie(oei,oei_end)=out_edges(*vi,G.main_g); oei != oei_end; ++oei){
            // consider edge (v,u)
            // get u in main graph
            main_u = target(*oei,G.main_g);
            color_u = G.main_g[main_u].color;
            // check if u has a color (not -1)
            if (color_u != -1){
                // check if edge (v,u) is fulfilled
                conflict_u = G.main_g[main_u].conflict_vertices[color_u];
                if (edge(conflict_v,conflict_u,G.conflict_g).second){
                    // edge (v,i)(u,color_u) exist in the conflict graph of uv: conflict!
                    conflict = true;
                    break;
                }
            }
        }
        // check if a conflict has been found with color i for vi
        if(!conflict){
            // no conflict
            // recursive call on the next vertex
            if(conflictColoring_backtrack_enum_rec<some_vertex_iter>(std::next(vi),vi_end,G,f)){
                // found a coloring in this branch: success!
                return true;
            }
        }
    }
    // no coloring found in this branch: reset color and backtrack!
    G.main_g[*vi].color = -1;
    return false;
}

// split the call of conflictColoring_backtrack_degree_global_cc on connected components
bool conflictColoring_backtrack_degree_global(ConflictGraph& G){
    return conflictColoring_splitcc(G,conflictColoring_backtrack_degree_global_cc);
}

// improvement of the backtrack algorithm, by considering the vertices
// in decreasing degree order in the main graph
bool conflictColoring_backtrack_degree_global_cc(ConflictGraph& G){
    // useful variables
    vertex_iter vi, vi_end;
    std::multimap<int,vertex_desc> map_degree2vertex; // degree -> vertex descriptors
    int d;
    //int num_v = num_vertices(G.main_g);
    // init colors and construct map_degree2vertex
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        G.main_g[*vi].color = -1;
        d = degree(*vi, G.main_g);
        map_degree2vertex.insert(std::make_pair(d,*vi));
    }
    // construct a vector of vertex_desc according to decreasing degrees
    std::vector<vertex_desc> vertices_deg;
    std::multimap<int,vertex_desc>::reverse_iterator mi;
    for(mi = map_degree2vertex.rbegin(); mi != map_degree2vertex.rend(); ++mi){
        vertices_deg.push_back(mi->second);
    }
    // start backtrack with the vector as iterator
    std::vector<vertex_desc>::iterator vvi = vertices_deg.begin();
    std::vector<vertex_desc>::iterator vvi_end = vertices_deg.end();
    return conflictColoring_backtrack_enum_rec<std::vector<vertex_desc>::iterator>(vvi,vvi_end,G,enum_stop);
}

// split the call of conflictColoring_backtrack_degree_local_cc on connected components
bool conflictColoring_backtrack_degree_local(ConflictGraph& G){
    return conflictColoring_splitcc(G,conflictColoring_backtrack_degree_local_cc);
}

// improvement of the backtrack algo, by considering the vertices
// in decreasing degree order during a graph search, starting from the vertex of highest degree
bool conflictColoring_backtrack_degree_local_cc(ConflictGraph& G){
    // useful variables
    vertex_iter vi, vi_end;
    out_edge_iter oei, oei_end;
    vertex_desc v, u, v_max;
    int d, d_max;
    //int num_v = num_vertices(G.main_g);
    std::vector<vertex_desc> vertices_deg; // the order we are building for backtrack
    std::map<vertex_desc,bool> is_visited;
    std::multimap<int,vertex_desc> queue; // degree -> vertex in queue
    std::multimap<int,vertex_desc>::reverse_iterator mi; // map iterator
    // init colors, visted, and get the vertex of highest degree
    d_max = -1;
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        G.main_g[*vi].color = -1;
        is_visited[*vi] = false; 
        d = degree(*vi, G.main_g);
        if(d > d_max){
            d_max = d;
            v_max = *vi;
        }
    }
    // construct a vector of vertex_desc according to decreasing local degrees
    // start with the vertex of highest degree
    queue.insert(std::make_pair(d_max,v_max));
    is_visited[v_max] = true;
    // each loop, take the last element (=highest degree) in queue: 
    // * remove it from queue
    // * add it to vertices_deg 
    // * add its unvisted neighbors to queue
    while (!queue.empty()){
        mi = queue.rbegin();
        v = mi->second;
        queue.erase(std::next(mi).base()); // from https://stackoverflow.com/a/1830240
        vertices_deg.push_back(v);
        for(tie(oei,oei_end) = out_edges(v,G.main_g); oei != oei_end; oei++){
            u = target(*oei,G.main_g);
            d = degree(u,G.main_g);
            if(!is_visited[u]){
                queue.insert(std::make_pair(d,u));
                is_visited[u] = true;
            }
        }
    }
    // start backtrack with the vector as iterator
    std::vector<vertex_desc>::iterator vvi = vertices_deg.begin();
    std::vector<vertex_desc>::iterator vvi_end = vertices_deg.end();
    return conflictColoring_backtrack_enum_rec<std::vector<vertex_desc>::iterator>(vvi,vvi_end,G,enum_stop);
}

// computes a maximum conflict coloring using backtrack algo (init part)
// arguments :
// * a ConflictGraph
// returns the maximum number of fulfilled edges
// and sets .color property of main vertices to a max-coloring
int conflictColoring_max(ConflictGraph& G){    
    // useful variables
    vertex_iter vi, vi_end;
    // init colors
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        G.main_g[*vi].color = -1;
    }
    // init vertex iterator
    tie(vi,vi_end)=vertices(G.main_g);
    // start with max_count=-1 and count=0
    int max_count = conflictColoring_max_rec(-1,0,vi,vi_end,G);
    // update .color with the max-coloring stored in .color_max
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        G.main_g[*vi].color = G.main_g[*vi].color_max;
    }
    // finished
    return max_count;
}

// computes a maximum conflict coloring (recusive part)
// arguments :
// * maximum number of fulfilled edges found so far
// * current number of fulfilled edges
// * vertex iterator (current)
// * vertex iterator end
// * the ConflictGraph
// returns the maximum number of fulfilled edges
// uses .color to explore all colorings, and .color_max to remember the best so far
int conflictColoring_max_rec(int max_count, int count, vertex_iter vi, vertex_iter vi_end, ConflictGraph& G){
    //useful variables
    out_edge_iter oei, oei_end;
    vertex_desc conflict_v, main_u, conflict_u;
    int color_u;

    // check if we have reached the end,
    // if yes, check if we obtain a better coloring and remember it
    if(vi==vi_end){
        if (count > max_count){
            max_count = count;
            // remember the current max-coloring
            for (tie(vi, vi_end) = vertices(G.main_g); vi != vi_end; vi++){
                G.main_g[*vi].color_max = G.main_g[*vi].color;
            }
        }
        // backtrack to a new branch
        return max_count;
    }

    // remember the current count before the current vertex
    int count_before = count;
    // check all colors for the current vertex
    for (int i = 0; i < G.main_g[*vi].num_colors; ++i){
        // reset count to the value before a color is given to *vi
        count = count_before;
        // set the color
        G.main_g[*vi].color = i;
        // get (v,i) from conflict graph
        conflict_v = G.main_g[*vi].conflict_vertices[i];
        // update count if there are more fulfilled edges by choosing color i for vi
        for(tie(oei,oei_end)=out_edges(*vi,G.main_g); oei != oei_end; ++oei){
            // consider edge (v,u)
            // get u in main graph
            main_u = target(*oei,G.main_g);
            color_u = G.main_g[main_u].color;
            // check if u has a color (not -1)
            if (color_u != -1){
                // check if edge (v,u) is fulfilled
                conflict_u = G.main_g[main_u].conflict_vertices[color_u];
                if (!edge(conflict_v,conflict_u,G.conflict_g).second){
                    // edge (v,i)(u,color_u) do not exist: fulfilled!
                    count++;
                }
                // todo here: if no of conflict in this branch is more than n-max_cout(best found),
                //               then, break this search branch
            }
        }
        // check recursively to the next vertex iter...
        max_count = conflictColoring_max_rec(max_count, count, std::next(vi), vi_end, G);
    }
    // tried all colors for the current vertex
    // reset color and go to another branch
    G.main_g[*vi].color = -1;
    return max_count;
}


// conflict coloring max on trees is polynomial
// argument:
// * a ConflictGraph which is assumed to be a tree
// returns the maximum number of fulfilled edges
// and sets .color_max property of main vertices to a max-coloring
// (also set .color if all edges are fulfilled)
int conflictColoring_maxTree(ConflictGraph& T){
    // init vertex iterator
    vertex_desc root = *vertices(T.main_g).first;
    // debug
    std::cout << "root " << root << "\n";
    // start
    Max_coloring max = conflictColoring_maxTree_rec(root,root,T);
    // get the max
    int max_S = 0;
    std::map<vertex_desc,int> max_C;
    for (int i = 0; i < T.main_g[root].num_colors; i++){
        if (max.S[i] > max_S){
            max_S = max.S[i];
            max_C = max.C[i];
        }
    }
    // fill .color_max with a max coloring
    vertex_iter vi, vi_end;
    for(tie(vi,vi_end)=vertices(T.main_g); vi!=vi_end; ++vi){
        T.main_g[*vi].color_max = max_C[*vi];
    }
    if(max_S == num_edges(T.main_g)){
        // this is also a coloring, set .color as well
        for(tie(vi,vi_end)=vertices(T.main_g); vi!=vi_end; ++vi){
            T.main_g[*vi].color = max_C[*vi];
        }   
    }
    // done
    return max_S;
}

// computes a maximum conflict coloring on tree, recursively
// arguments:
// * current vertex iterator
// * vertex descriptor of the parent
// * the conflict graph which is assumed to be a tree
// returns a structure of:
// * an array of num_colors elements, giving the maximum number of fulfilled edges
//   relative to the current vertex colors
// * and a coloring of its subtree realizing that number of fullfilled edges (itself included)
// remark: current==parent for the root
Max_coloring conflictColoring_maxTree_rec(vertex_desc current, vertex_desc parent, ConflictGraph T){
    // useful variables
    out_edge_iter oei, oei_end;
    vertex_desc child, conflict_current, conflict_child;
    int i_color, i_child, i_child_color;
    int num_c = T.main_g[current].num_colors;

    // arrays to be returned
    Max_coloring max;
    // intialize at the correct sizes
    std::map<vertex_desc,int> m;
    for(int i=0; i<num_c; i++){
        max.S.push_back(0);
        max.C.push_back(m);
    }

    // check if we have reached a leaf: if yes, initialize and return max
    if(out_degree(current,T.main_g)==1 && current!=parent) {
        for (i_color = 0; i_color < num_c; i_color++){
            max.S[i_color] = 0;
            max.C[i_color][current] = i_color;
        }
        // debug
        //std::cout << "leaf " << current << "\n";
        return max;
    }
    // not a leaf

    // number of children
    int num_children = out_degree(current,T.main_g)-1;
    // +1 for the root
    if(current==parent){
        num_children++;
    } 
    // maximum number of fulfilled edges of the children
    // and associated colorings
    Max_coloring max_children[num_children];
    // correspondance index -> vertex descriptor of the children
    vertex_desc children[num_children]; 

    // recover information from the children
    i_child = 0;
    for(tie(oei,oei_end)=out_edges(current,T.main_g); oei != oei_end; ++oei){
        // get u in main graph and first check if u is not parent of v
        child = target(*oei,T.main_g);
        if (child != parent){
            // recursive call to get S from the child
            max_children[i_child] = conflictColoring_maxTree_rec(child, current, T);
            children[i_child] = child;
            i_child++;
        }
    }

    // for each color of the current vertex, choose a coloring on its children
    // in order to fulfill the maximum number of edges in the subtree
    int max_S;
    int max_S_child;
    int max_C_child[num_children];
    std::map<vertex_desc,int> C_child;
    for (i_color = 0; i_color < num_c; ++i_color){
        // initialize
        max.S[i_color] = 0;
        // get (v,i) from conflict graph
        conflict_current = T.main_g[current].conflict_vertices[i_color];
        // maximize the number of fulfilled edges in the subtree for every child
        for(i_child = 0; i_child < num_children; ++i_child){
            child = children[i_child];
            // maximize the number of fulfilled edges in the subtree for this child
            max_S = -1;
            for (i_child_color = 0; i_child_color < T.main_g[child].num_colors; i_child_color++){
                conflict_child = T.main_g[child].conflict_vertices[i_child_color];
                // get the value of S_children for this color
                max_S_child = max_children[i_child].S[i_child_color];
                if (!edge(conflict_current, conflict_child, T.conflict_g).second){
                    // no conflict edge => +1
                    max_S_child++;
                }
                // check if this color is better
                if (max_S_child > max_S){
                    max_S = max_S_child;
                    max_C_child[i_child] = i_child_color;
                }
            }
            // add the best count for this child to S
            max.S[i_color] += max_S;
        }
        // gather the coloring in max->C
        max.C[i_color][current] = i_color;
        for(i_child = 0; i_child < num_children; ++i_child){
            // copy the map of the child color that realized the maximum:
            // * get the color that realized the maximum
            i_child_color = max_C_child[i_child];
            // * get the corresponding coloring
            C_child = max_children[i_child].C[i_child_color];
            // * copy it to the current coloring
            for(auto it = C_child.begin(); it != C_child.end(); ++it){
                max.C[i_color].insert(*it);
            }
        }
    }

    // debug
    /* std::cout << "vertex " << current << " returns S=";
    for (i_color = 0; i_color < T.main_g[current].num_colors; i_color++){
        std::cout << S[i_color] << ",";
    }
    std::cout << "\n"; */

    // done
    return max;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// define global variables in order to pass additional arguments to cut_2_tree
// which is the functor given to conflictColoring_backtrack_enum_rec
// which has a fixed prototype: bool(*)(ConflictGraph&)
// TODO: try to improve this by making the functor be able to have different types
std::map<vertex_desc,vertex_desc> GLOBAL_map_tree2cut;
ConflictGraph GLOBAL_G_tree;

// split the call of conflictColoring_cut_set_cycle_cc on connected components
bool conflictColoring_cut_set_cycle(ConflictGraph& G){
    return conflictColoring_splitcc(G,conflictColoring_cut_set_cycle_cc);
}

// conflict coloring using cut set cycle method
// (see the book "Constraint Processing" by Rina Dechter (Morgan Kaufmann edition) chapter 10)
// 1. using BFS to detect cycle and construct greedily cut set (CS)
// 2. construct a tree from the CS found (with aditionnal vertices)
// 3. for every coloring of the cut set (found by backtrack enumeration),
//    try to extend this sub-coloring to the tree representing the whole graph
bool conflictColoring_cut_set_cycle_cc(ConflictGraph& G){
    // useful variables
    vertex_iter vi, vi_end;
    out_edge_iter oei, oei_end;
    vertex_desc root, v;

    std::map<vertex_desc,bool> is_visited;
    int num_v = num_vertices(G.main_g);
    int d, deg_min=num_v+1, e;
    // initialize coloring and find root (of min degree)
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        G.main_g[*vi].color = -1;
        is_visited[*vi] = false;
        d = degree(*vi, G.main_g);
        if(d < deg_min){
            deg_min = d;
            root = *vi;
        }
    }

    // TODO: try other methods to construct a cut set

    // To minimize the size of cycle cut set (heuristic),
    // BFS follows the increasing degree order of vertices 
    //std::queue<vertex_desc> queue; // FIFO
    // change queue of type map
    std::queue<vertex_desc> queue; //vertex queue -> vertex main G
    std::multimap<int,vertex_desc> map_degree2child; // degree -> child descriptors
    std::unordered_set<vertex_desc> tree; // vertices of the tree
    std::unordered_set<vertex_desc> neighbors;
    std::unordered_set<vertex_desc> cut; // cycle cut set 
    // (unordered_set are used to compute intersections efficiently)
    // initialize
    queue.push(root);
    is_visited[root] = true;
    // each loop, pop v from queue and:
    // 1. check whether v can be added to tree, otherwise cut
    // 2. push the unvisited neighbors of v to queue
    while(!queue.empty()){
        // take the first element in queue
        v = queue.front();
        queue.pop();
        // 1.
        // check if v creates a cycle in tree by counting the number of edges
        neighbors.clear();
        for(tie(oei, oei_end) = out_edges(v, G.main_g); oei!=oei_end; oei++){
            neighbors.insert(target(*oei, G.main_g));
        }
        // e is the size of the intersection of tree and neighbors
        // source: https://stackoverflow.com/a/24337598
        e = std::count_if(neighbors.begin(), neighbors.end(), [&](int k) {return tree.find(k) != tree.end();});
        if(e<=1){
            // add to tree
            tree.insert(v);
        }
        else{
            // creates a cycle: add to cut set
            cut.insert(v);
        }
        // 2.
        // reset map_degree2child
        map_degree2child.clear();
        // push unvisited neighbors to map according to degree
        for(auto it = neighbors.begin(); it != neighbors.end(); ++it){
            if(!is_visited[*it]){
                d = degree(*it, G.main_g);
                map_degree2child.insert(std::make_pair(d,*it));
            }
        }
        // push unvisited neighbors to list in increasing degree
        for(auto mi=map_degree2child.begin(); mi!=map_degree2child.end(); mi++){
            v = mi->second; // we don't need the original v anymore
            queue.push(v);
            is_visited[v] = true;
        }
    }
    
    /*
    // debug: print the cut set cycle:
    std::cout << "the cut set is ";
    for (vertex_desc i:cut){
        std::cout << i << " ";
    } 
    std::cout << "\n"; 
    */

    // construct subgraph by cut
    ConflictGraph G_cut;
    std::map<vertex_desc,vertex_desc> map_vertex_G2cut; // main graph G -> G_cut
    create_subgraph(G,cut,G_cut,map_vertex_G2cut);
    
    // construct subgraph by tree
    ConflictGraph G_tree;
    std::map<vertex_desc,vertex_desc> map_vertex_G2tree; // main graph G -> G_tree
    create_subgraph(G,tree,G_tree,map_vertex_G2tree);

    /* 
    // debug: print the graphs and maps
    std::cout << "G=\n";
    print_conflict_graph(G);
    std::cout << "\n";
    std::cout << "map_vertex_G2cut=\n";
    for(auto ci = map_vertex_G2cut.begin(); ci!=map_vertex_G2cut.end(); ci++){
        std::cout << "  " << ci->first << " -> " << ci->second << "\n" ;
    }
    std::cout << "\n";
    std::cout << "G_cut=\n";
    print_conflict_graph(G_cut);
    std::cout << "\n";
    std::cout << "map_vertex_G2tree=\n";
    for(auto ci = map_vertex_G2tree.begin(); ci!=map_vertex_G2tree.end(); ci++){
        std::cout << "  " << ci->first << " -> " << ci->second << "\n" ;
    }
    std::cout << "\n";
    std::cout << "G_tree=\n";
    print_conflict_graph(G_tree);
    std::cout << "\n"; 
    */

    // construct the complete tree (G_tree + cut)
    std::map<vertex_desc,vertex_desc> map_tree2cut; // G_tree -> G_cut
    vertex_desc u, v_u;
    for(auto ci = cut.begin(); ci!=cut.end(); ci++){
        for(tie(oei,oei_end) = out_edges(*ci,G.main_g);oei!=oei_end;oei++){
            // get u neighbor of *ci
            u = target(*oei,G.main_g);
            // check if the neighbor is in the tree (not in the cut)
            if(cut.find(u) == cut.end()){
                // add v_u a copy of *ci to G_tree
                v_u = add_conflict_vertex(G.main_g[*ci].num_colors,G_tree);
                // add to map_tee2cut
                map_tree2cut[v_u] = map_vertex_G2cut[*ci];
                // copy the edge to G_tree
                copy_conflict_edge(*ci,u,G,v_u,map_vertex_G2tree[u],G_tree);
            }
        }
    }
    /* 
    // debug: print the graphs and maps
    std::cout << "map_vertex_G2tree (again) =\n";
    for(auto ci = map_vertex_G2tree.begin(); ci!=map_vertex_G2tree.end(); ci++){
        std::cout << "  " << ci->first << " -> " << ci->second << "\n" ;
    }
    std::cout << "\n"; 
    std::cout << "G_tree (complete) =\n";
    print_conflict_graph(G_tree);
    std::cout << "\n";
    std::cout << "map_tree2cut=\n";
    for(auto ci = map_tree2cut.begin(); ci!=map_tree2cut.end(); ci++){
        std::cout << "  " << ci->first << " -> " << ci->second << "\n" ;
    }
    std::cout << "\n";
     */
    // 1. enumerate conflict colorings in G_cut
    // 2. each coloring found will apply functon cut_2_tree to G_tree to complete the coloring
    // init vertex iterator
    tie(vi,vi_end)=vertices(G_cut.main_g);
    // set the global arguments
    GLOBAL_G_tree = G_tree;
    GLOBAL_map_tree2cut = map_tree2cut;

    // start
    if(conflictColoring_backtrack_enum_rec(vi,vi_end,G_cut,cut_2_tree)){
        // coloring found: the result is stored in GLOBAL_G_tree and G_cut
        // set .color of G
        for(tie(vi,vi_end) = vertices(G.main_g); vi != vi_end; vi++){
            if(map_vertex_G2tree.find(*vi) != map_vertex_G2tree.end()){
                // *vi is a vertex from G_tree
                G.main_g[*vi].color = GLOBAL_G_tree.main_g[map_vertex_G2tree[*vi]].color;
            }
            else{
                // *vi is a vertex from G_cut
                G.main_g[*vi].color = G_cut.main_g[map_vertex_G2cut[*vi]].color;
            }
        }
        // color found so in this case we return true
        return true;
    }
    else{
        // no coloring found
        // .color of G are still -1
        return false;
    }
} 

// Complete a coloring in a tree given a sub-coloring of the cut set.\\
// argument (some of them are global variables):
// * a conflictGraph G_cut (<=> sub-coloring)
// * a conflictGraph GLOABAL_G_tree (a tree)
// * a map GLOBAL_map_tree2cut from vertex descriptors of GLOABAL_G_tree to G_cut
// This is the functor passed by conflictColoring_cut_set_cycle_cc
// conflictColoring_backtrack_enum,
// which tries to extend the solutions on the cut set.
// For each sub-coloring found on the cut set,
// it calls the function pre_conflictColoring_Forest
// to complete coloring
bool cut_2_tree(ConflictGraph& G_cut){
    // useful variables
    vertex_iter vi, vi_end;

    // init .color of G_tree
    for (tie(vi, vi_end) = vertices(GLOBAL_G_tree.main_g);vi!=vi_end; vi++){
        GLOBAL_G_tree.main_g[*vi].color = -1;
    }
    // copy the colors from G_cut to G_tree
    // to check: print the subcoloring
    // std::cout << "The subcoloring is \n";
    for(auto mi=GLOBAL_map_tree2cut.begin();mi!=GLOBAL_map_tree2cut.end();mi++){
        // copy the color from G_cut to G_tree 
        GLOBAL_G_tree.main_g[mi->first].color = G_cut.main_g[mi->second].color; 
        // std::cout << "  vertex " << mi->first << " has color " << GLOBAL_G_tree.main_g[mi->first].color << "\n";
    }

    // complete coloring of G using conflictColoring for tree
    // (caution: our graph GLOBAL_G_tree may be a forest)
    if (pre_conflictColoring_Forest(GLOBAL_G_tree)){
        return true; // enum_rec stops
    }
    else{
        return false; // enum_rec continues
    } 
}

//%%%%%%%%%%%% PRE-COLORING ON TREE %%%%%%%%%%%%%%%%%%%%%%%%%%%%

// PRE-COLORING of a forest
// same as pre_conflictColoring_Tree, for T a forest
// Note: assumes that each connected component has an uncolored vertex
bool pre_conflictColoring_Forest(ConflictGraph& T){
    // compute connected components
    std::vector<int> component(num_vertices(T.main_g)); // vertex descriptor -> component number
    int num_components = connected_components(T.main_g, &component[0]);

    // get one uncolored vertex per connected component
    std::vector<vertex_desc> roots(num_components);
    vertex_iter vi,vi_end;
    for(tie(vi,vi_end)=vertices(T.main_g); vi!=vi_end; ++vi){
        if(T.main_g[*vi].color == -1){
            roots[component[*vi]]=*vi;
        }
    }
    
    // run pre-coloring on tree for each of the vertices
    for(int i=0; i<num_components; ++i){
        if(!pre_conflictColoring_Tree(T,roots[i])){
            return false;
        }
    }
    // all cc have conflict-coloring
    return true;
}

// PRE-COLORING (init part)
// decide if there exists a conflict coloring on trees from a given coloring for a subset of vertices (set already in .color)
// argument:
// * a ConflictGraph T which is assumed to be a tree
// * vertex descriptor of the root
// Note: the root (the first element of vertex set) should be an uncolored vertex
// Note: the preset colors have no conflict with each other 
// returns true if exist a conflict coloring, false otherwise
bool pre_conflictColoring_Tree(ConflictGraph& T, vertex_desc root){
    // useful variables
    vertex_iter vi, vi_end;

    /*
    // debug
    for(tie(vi,vi_end)=vertices(T.main_g); vi!=vi_end; ++vi){
        std::cout << "vertex " << *vi << " has color " << T.main_g[*vi].color << "\n";
    }
    std::cout << "the root is " << root << "\n";
    exit(0);
    */

    // call recursive part
    std::vector<bool> b = pre_conflictColoring_Tree_rec(root, root, T);
    
    /*
    // debug
    std::cout << "result of the rec call from root is ";
    for (int i=0; i < T.main_g[root].num_colors; i++){
        std::cout << b[i];
    }
    std::cout << "\n";
    exit(0);
    */

    int num_c = T.main_g[root].num_colors;
    for(int c = 0; c < num_c ; c++){
        if(b[c]){
            return true;
        }
    }
    return false;
}

// PRE-COLORING (recursive part)
// computes a conflict coloring on tree recursively given a coloring in a subset of vertices
// arguments:
// * vertex descriptor of the current vertex
// * vertex descriptor of the parent
// * the conflict graph which is assumed to be a tree
// returns an array of num_colors (of the current vertex) booleans,
// indicating which colors are possible (true) or impossible (false) for this vertex.
// remark: current==parent for the root
std::vector<bool> pre_conflictColoring_Tree_rec(vertex_desc current, vertex_desc parent, ConflictGraph& T){
    // useful variables
    out_edge_iter oei, oei_end;
    vertex_desc child, conflict_current, conflict_child;
    int i_color, i_child, i_child_color, c;
    int num_c = T.main_g[current].num_colors;
    std::vector<bool> b(num_c); // memory allocation (b is returned)
    // init b[num_c] to true for all colors,
    // and then we will set to false the impossible colors
    for (c=0; c<num_c; c++){
        b[c] = true;
    }

    // check if we have reached a leaf: if yes, initialize and return b
    if(out_degree(current,T.main_g)==1 && current!=parent) {
        // if this leaf has a .color already set, return b = false for all colors except the .color
        if (T.main_g[current].color != -1){
            // get the color of this leaf
            c = T.main_g[current].color;
            for (i_color = 0; i_color < num_c; i_color++){
                b[i_color] = false;
            }
            b[c] = true;
        }
        // (otherwise b is set to all true)

        /*
        // debug
        std::cout << "leaf " << current << " (parent " << parent << ") ";
        for (i_color = 0; i_color < num_c; i_color++){
            std::cout << b[i_color];
        }
        std::cout << "\n";
        //std::cout << "b of " << current << " is at address " << b << "\n";
        */

        return b; 
    }
    // not a leaf

    // number of children
    int num_children = out_degree(current,T.main_g)-1;
    // +1 for the root
    if(current==parent){
        num_children++;
    }
    //std::cout << "vertex " << current << " has " << num_children << " children\n";
    
    // correspondance index -> vertex descriptor of the children
    vertex_desc children[num_children]; 
    //bool* b_child[num_children]; // array of boolean arrays of all children
    std::vector<std::vector<bool>> b_child(num_children); // array of boolean arrays of all children
    int num_c_child;
    int count=0;
    // take information from the children
    i_child = 0;
    for(tie(oei,oei_end)=out_edges(current,T.main_g); oei != oei_end; ++oei){
        // get u in main graph and first check if u is not parent of v
        child = target(*oei,T.main_g);
        // consider only the children
        if (child != parent){
            // child infos
            children[i_child] = child;
            num_c_child = T.main_g[child].num_colors;

            // allocate size result (this error was quite hard to fix...)
            b_child[i_child].resize(num_c_child);
            // recursive call to get b from the child
            b_child[i_child] = pre_conflictColoring_Tree_rec(child, current, T);
            //std::cout << "address of result at " << current << " for child " << child << " is " << b_child[i_child] << "\n";

            /*
            // debug
            std::cout << "result at " << current << " for child " << child << " is (size=" << b_child[i_child].size() << ")\n";
            for(c=0;c<num_c_child;c++){
                std::cout << b_child[i_child][c];
            }
            std::cout << "\n";
            */

            // speedup: if b_child contains all false then no need to continue,
            //          set b to all false and return.
            count = 0;
            for(c=0;c<num_c_child;c++){
                if(!b_child[i_child][c]){
                    count += 1;
                }
            }
            if (count==num_c_child){
                //std::cout << "speedup at " << current << " (child is " << child << ")\n";
                for(c=0;c<num_c;c++){
                    b[c] = false;
                }
                return b;
            }

            // increment child index
            i_child++;
        }
    }

    // for each color of the current vertex v, look at each array of a child,
    // to see if this color is possible or not
    bool child_compatible;
    for (i_color = 0; i_color < num_c; ++i_color){
        // get (current,i_color) from conflict graph
        conflict_current = T.main_g[current].conflict_vertices[i_color];
        // for each child, check if this color is possible or not
        for(i_child = 0; i_child < num_children; ++i_child){
            child_compatible = false;
            child = children[i_child];
            // check if child has a compatible possible color
            for (i_child_color = 0; i_child_color < T.main_g[child].num_colors; i_child_color++){
                if(b_child[i_child][i_child_color]){
                    conflict_child = T.main_g[child].conflict_vertices[i_child_color];
                    // check if there is a conflict edge
                    if (!edge(conflict_current, conflict_child, T.conflict_g).second){
                        // no conflict edge: compatible!
                        child_compatible = true;
                        // no need to check the other colors of this child
                        break;
                    }
                }
            }
            if(!child_compatible){
                // found an incompatible child for i_color
                b[i_color] = false;
                // no need to check the other children for this color
                break;
            }
        }
    }

    // debug
    /* std::cout << "vertex " << current << " returns S=";
    for (i_color = 0; i_color < T.main_g[current].num_colors; i_color++){
        std::cout << S[i_color] << ",";
    }
    std::cout << "\n"; */

    // done
    return b;
}

//%%%%%%%%%%%%%%%%  TODO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// techniques to improve the search:
// * order of choosing a vertex:
//      * Degree heuristic (DH) = Initially when the number of colors are the same for all vertices, choose vertex of highest degree
//      * Min remaing values (MRV) = Ow, choose vertex of least possible colors 
// * which color order in a vertex:
//      * Least constrainning value (LCV) = prefer a color that conflict the least contrainsts

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%