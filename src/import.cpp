#include "import.hpp"

// print main graph and conflict graph in terminal
// uses the fact that vertex descriptors are integers (vecS)
void print_conflict_graph(ConflictGraph G){
    // useful variables
    vertex_iter vi, vi_end;
    edge_iter ei, ei_end;
    vertex_desc u, v;
    // print main graph vertices
    std::cout << "vertices:\n";
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        std::cout << "  vertex " << *vi << " has " << G.main_g[*vi].num_colors << " colors\n";
    }
    // print main graph edges and conflict graphs
    std::cout << "edges:\n";
    for(tie(ei,ei_end)=edges(G.main_g); ei != ei_end; ++ei){
        u = source(*ei,G.main_g);
        v = target(*ei,G.main_g);
        std::cout << "  " << u << " -- " << v << "\n";
        // print the conflict graph
        for (int c_u = 0; c_u < G.main_g[u].num_colors; ++c_u){
            for (int c_v = 0; c_v < G.main_g[v].num_colors; ++c_v){
                // check if edge (u,c_u)(v,c_v) exists
                if(edge(G.main_g[u].conflict_vertices[c_u], G.main_g[v].conflict_vertices[c_v], G.conflict_g).second){
                    std::cout << "    (" << u << "," << c_u << ") -- (" << v << "," << c_v << ");\n";
                }
            }
        }
    }
}

// add a vertex to the main graph
// arguments:
// * number of colors in conflict graph
// * the ConflictGraph
// return: vertex descriptor in the main graph
vertex_desc add_conflict_vertex(int num_colors, ConflictGraph& G){
    // create vertex in main graph
    vertex_desc main_v = add_vertex(G.main_g);
    G.main_g[main_v].num_colors = num_colors;
    G.main_g[main_v].color = -1;
    // create num_colors vertices in conflict graph
    vertex_desc conflict_v;
    for(int i=0; i<num_colors; ++i){
        conflict_v = add_vertex(G.conflict_g);
        // update properties
        G.conflict_g[conflict_v].main_vertex = main_v;
        G.conflict_g[conflict_v].color_index = i;
        G.main_g[main_v].conflict_vertices.push_back(conflict_v);
    }
    return main_v;
}

// add an edge  to the conflict graph (and to the main graph)
// arguments:
// * a vertex desc v in main graph
// * a vertex desc u in main graph
// * a color of v
// * a color of v
// * the ConflictGraph
// return: it returns the result of add_edge (at v with color_v and u with color_u) to the conflict graph, see
//         https://www.boost.org/doc/libs/1_58_0/libs/graph/doc/adjacency_list.html
std::pair<edge_desc,bool> add_conflict_edge(vertex_desc main_v, int color_v, vertex_desc main_u, int color_u, ConflictGraph& G){
    
    //add edge uv to main graph main_g
    add_edge(main_v,main_u,G.main_g);
    
    // check color range
    int num_colors_v = G.main_g[main_v].num_colors;
    int num_colors_u = G.main_g[main_u].num_colors;
    //error if a color is not in the list of colors of a vertex
    if (color_v > num_colors_v || color_u > num_colors_u){
        std::cout << "error: color out of range.\n";
        // TODO raise an exception
    }
    // add conflict edges of edge uv: edge (u, conflict_u)(v, conflict_v)
    vertex_desc conflict_v = G.main_g[main_v].conflict_vertices[color_v];
    vertex_desc conflict_u = G.main_g[main_u].conflict_vertices[color_u]; 
    return add_edge(conflict_u, conflict_v, G.conflict_g);
}

// random number generator (for seed)
// source: https://stackoverflow.com/a/7617612
unsigned long long rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((unsigned long long)hi << 32) | lo;
}

// generate a random conflict graph
// arguments:
// * a ConflictGraph (empty)
// * number of vertices in main graph
// * probability of each edge in main graph
// * number of colors per vertex of the main graph
// * probability of each edge in the conflict graph (bipartite)
// return the number of edges in the main graph
int generate_random_conflict_graph_erdos_renyi(ConflictGraph& G, int num_v, float p_v, int num_c, float p_c){
    // useful variables
    vertex_iter vi, vi_end;
    vertex_desc u, v, main_u, main_v;
    edge_iter ei, ei_end;
    float r = 0;
    std::srand(rdtsc());
    // generate a random graph g
    auto seed = rdtsc();
    minstd_rand gen(seed);
    Graph g(ERGen(gen, num_v, p_v), ERGen(), num_v);
    // construct the vertices of the main graph
    std::map<vertex_desc,vertex_desc> map_g;
    for(tie(vi,vi_end)=vertices(g); vi != vi_end; ++vi){
        main_u = add_conflict_vertex(num_c, G);
        // correspondance *vi and main_u
        map_g[*vi] = main_u;
    }
    // construct the edges of the main graph and of the conflict graph
    int number_of_edges = 0;
    bool edge_exists = false;
    for(tie(ei,ei_end)=edges(g); ei != ei_end; ++ei){
        u = source(*ei,g);
        v = target(*ei,g);
        main_u = map_g[u];
        main_v = map_g[v];
        // generate a random conflict graph of main_u main_v
        edge_exists = false;
        for (int c_u = 0; c_u < num_c; ++c_u){
            for (int c_v = 0; c_v < num_c; ++c_v){
                r = static_cast <float> (std::rand()) / static_cast <float> (RAND_MAX);
                if (r < p_c){
                    // add the edge
                    edge_exists = true;
                    add_conflict_edge(main_u,c_u,main_v,c_v,G);
                }   
            }
        }
        if(edge_exists){
            number_of_edges++;
        }
    }
    // return the number of edges in the main graph
    return number_of_edges;
}

// write a graph to .gv in dot format
// nodes id are of the form vucx where u is a vertex and x a color number.
// arguments:
// * a ConflictGraph
// * path to the .gv file
int export_conflict_graph_graphviz(ConflictGraph G, std::string filepath){
     // useful variables
    vertex_iter vi, vi_end;
    edge_iter ei, ei_end;
    vertex_desc u, v;
    // open file
    std::ofstream dotfile;
    dotfile.open(filepath);
    dotfile << "graph conflict_graph{\n";
    dotfile << "forcelabels=true;\n";
    dotfile << "node [shape=record];\n";
    /*
    // write conflict graph vertices
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        dotfile << "subgraph cluster_v" << *vi << "{\n";
        for(int c = 0; c < G.main_g[*vi].num_colors; ++c){
            dotfile << "v" << *vi << "c" << c << "; \n";
        }
        dotfile << "}\n";
    }
    */
    // write conflict graph vertices with formula 
    // v0[label="<c0>0|<c1>1|<c2>2|<c3>3",xlabel="v0"];
     for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        dotfile << "v" << *vi << "[label=\"";
        for(int c = 0; c < G.main_g[*vi].num_colors; ++c){
            dotfile << "<c" << c << ">" << c << "|";
        }
        dotfile.seekp(-1, std::ios_base::end);
        dotfile << "\",xlabel=\"v" << *vi << "\"];\n";
    }
   
    // write conflict graph edges 
    for(tie(ei,ei_end)=edges(G.main_g); ei != ei_end; ++ei){
        u = source(*ei,G.main_g);
        v = target(*ei,G.main_g);
        for (int c_u = 0; c_u < G.main_g[u].num_colors; ++c_u){
            for (int c_v = 0; c_v < G.main_g[v].num_colors; ++c_v){
                // (u,c_u)(v,c_v) 
                if(edge(G.main_g[u].conflict_vertices[c_u], G.main_g[v].conflict_vertices[c_v], G.conflict_g).second){
                    //dotfile << "v" << u << "c" << c_u << " -- v" << v << "c" << c_v << ";\n"; 
                    // formula in gv file is v4:c0 -- v1:c1;
                    dotfile << "v" << u << ":c" << c_u << "--v" << v << ":c" << c_v << ";\n";
                }
            }
        }
    }
    // close file
    dotfile << "}";
    dotfile.close();
    return 0;
}

// write a main graph to .gv in dot format
// nodes id are of the form (uv).
// arguments:
// * a ConflictGraph
// * path to the .gv file
int export_main_graph_graphviz(ConflictGraph G, std::string filepath){
     // useful variables
    vertex_iter vi, vi_end;
    edge_iter ei, ei_end;
    vertex_desc u, v;
    // open file
    std::ofstream dotfile;
    dotfile.open(filepath);
    dotfile << "graph main_graph{\n";

    // write main vertices
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        dotfile << "v" << *vi << ";\n";
    }
   
    // write main edges 
    for(tie(ei,ei_end)=edges(G.main_g); ei != ei_end; ++ei){
        u = source(*ei,G.main_g);
        v = target(*ei,G.main_g);
        dotfile << "v" << u << "--v" << v << ";\n";  
    }
    // close file
    dotfile << "}";
    dotfile.close();
    return 0;
}

// generate a random conflict tree using Prufer sequences
// https://fr.wikipedia.org/wiki/Codage_de_Pr%C3%BCfer
// arguments:
// * a ConflictGraph (empty)
// * number of vertices in main graph
// * number of colors per vertex of the main graph
// * probability of each edge in the conflict graph (bipartite)
// return the number of edges in the main graph
// remark: it is possible to have an edge in the main graph,
//         even if its conflict graph is empty
//         (in order to have a tree structure in the main graph)
int generate_random_conflict_tree_prufer(ConflictGraph& T, int num_v, int num_c, float p_c){
    //variables
    int i, j;
    vertex_desc my_vertices[num_v];
    int P[num_v-2];
    int D[num_v];
    float r = 0;
    std::srand(time(NULL));
    // generete a sequence P of length num_v-2
    for (i = 0; i < num_v-2; i++){
        P[i] = std::rand() % num_v;
    }
    // construct the vertices of the main tree
    for(i = 0; i < num_v; ++i){
        my_vertices[i] = add_conflict_vertex(num_c, T);
    }
    // sequence D of degree 1 for all vertices
    for (i = 0; i < num_v; ++i){
        D[i] = 1;
    }
    // increase D for each element of P
    for (i = 0; i < num_v-2; i++){
        ++D[P[i]]; 
    }
    // main loop for each value of P
    for (i=0; i < num_v-2; i++){
        // find smallest j such that D[j]==1
        for (j=0; j<num_v; ++j){
            if (D[j]==1){
                break;
            }
        }

        // add edge between P[i] and j in main graph
        add_edge(my_vertices[P[i]], my_vertices[j], T.main_g);
        //generate a random conflict graph of edge P[i]j
        for (int c_i = 0; c_i < num_c; ++c_i){
            for (int c_j = 0; c_j < num_c; ++c_j){
                r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                if (r < p_c){
                    // add the edge
                    add_conflict_edge(my_vertices[P[i]],c_i,my_vertices[j],c_j,T);
                }   
            }
        }
        // decrease P[i] and j in D
        --D[P[i]];
        --D[j];
    }
    // add the remaining edge between the two vertices i,j such that D[i]=D[j]=1
    for (i=0; i<num_v; ++i){
        if (D[i]==1){
            --D[i];
            break;
        }
    }
    for (j=i+1; j<num_v; ++j){
        if (D[j]==1){
            --D[j];
            break;
        }
    }
    //add a random conflict graph of edge ij
    for (int c_i = 0; c_i < num_c; ++c_i){
        for (int c_j = 0; c_j < num_c; ++c_j){
            r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
            if (r < p_c){
                // add the edge
                add_conflict_edge(my_vertices[i],c_i,my_vertices[j],c_j,T);
            }   
        }
    }
    return num_v-1;
}

// import a conflict graph from dot format:
// nodes id are of the form vucx where u is a vertex and x a color number.
// arguments:
// * path to the .gz file
// * the ConflictGraph
bool import_conflict_graph_graphviz(std::string file_name, ConflictGraph& G){
    // open file
    std::ifstream myfile;
    myfile.open (file_name); 
    if (myfile.fail()){
        std::cout << "error open file";
        return false;
    }
    else{
        // useful variables for parsing
        int n_u, c_u, n_v, c_v;
        std::string line;
        std::map<int,vertex_desc> map_vertex; // remember number -> vertex descriptor
        int i=0;
        int l=0, num_c=0;
        while(getline(myfile,line)){
            // parse vertices
            if(std::regex_match(line, std::regex(".*\\[label=.*\\];"))){
                l = line.length();
                // get number of the vertex
                std::sscanf(line.c_str(),"v%d[.*",&n_v);
                // get number of colors
                num_c=1;
                for(i=0;i!=l;i++){
                    if(line[i]=='|'){
                        num_c+=1;
                    }
                }
                // add the vertex to G and remember its number in map_vertex
                map_vertex[n_v]=add_conflict_vertex(num_c,G);
                //std::cout << "parse: vertex " << n_v << " with " << num_c << " colors\n";
            }
            // parse edges
            if (std::regex_match(line,std::regex(".*--.*;"))){
                std::sscanf(line.c_str(),"v%d:c%d--v%d:c%d;",&n_u,&c_u,&n_v,&c_v);
                // add the edge to G
                add_conflict_edge(map_vertex[n_u],c_u,map_vertex[n_v],c_v,G);
                //std::cout << "parse: edge vertex " << n_u << " color " << c_u << " to vertex " << n_v << " color " << c_v << "\n";
            }
        }
    }
    myfile.close();
    return true;
}
// copy the conflict graph of an edge (ov, ou) in graph G to that of edge (cv,cu) in graph subG
bool copy_conflict_edge(vertex_desc ov, vertex_desc ou, ConflictGraph G, vertex_desc cv, vertex_desc cu, ConflictGraph& subG){
    vertex_desc conflict_i, conflict_j;
    int num_cv = subG.main_g[cv].num_colors;
    int num_cu = subG.main_g[cu].num_colors;
    for(int i=0; i<num_cv; i++){
        for(int j=0; j<num_cu; j++){
            conflict_i = G.main_g[ov].conflict_vertices[i];
            conflict_j = G.main_g[ou].conflict_vertices[j];
            if(edge(conflict_i,conflict_j,G.conflict_g).second){
                add_conflict_edge(cv,i,cu,j,subG);
                
            }
        }
    }
    return true;
}

// Create an induced subgraph.
// The result is given by reference in the two last arguments.
// arguments:
// * the original graph (ConflictGraph)
// * a subset of vertex descriptors (std::unordered_set<vertex_desc>)
// * the induced subgraph to be created (ConflictGraph)
// * a map for the correspondance vertex_desc of the original graph -> vertex_desc of the subgraph
// return false if a vertex descriptor of the subset do not exist in the original graph, true otherwise
// TODO: it seems impossible to check whether a vertex descriptor exists, so the function will return true, or fail :-/
bool create_subgraph(ConflictGraph G, std::unordered_set<vertex_desc> subset, ConflictGraph& subG, std::map<vertex_desc,vertex_desc>& map){
   // useful variables
    vertex_iter vi, vi_end, vj;
    vertex_desc v, sub_i, sub_j; //main_i, main_j;
    int num_c;
    // construct ConflictGraph subG
    // 1. construct vertex set 
    for(auto si = subset.begin(); si != subset.end(); ++si){
        num_c = G.main_g[*si].num_colors; // TODO: here we should check that *si exists... how??
        v = add_conflict_vertex(num_c, subG);
        map[*si] = v;
    }
    // 2. construct edge set
    for(auto si = subset.begin(); si != subset.end(); ++si){
        for(auto sj = si; sj != subset.end(); sj++){
            if(edge(*si,*sj,G.main_g).second){
                sub_i = map[*si];
                sub_j = map[*sj];
                // copy the conflict graph of this edge
                copy_conflict_edge(*si,*sj,G,sub_i,sub_j,subG);
            }
        }
    }
    /*
    for(tie(vi, vi_end) = vertices(subG.main_g); vi!=vi_end; vi++){
        for(vj = vi; vj!=vi_end; vj++){
            main_i = map[*vi];
            main_j = map[*vj];
            if(edge(main_i,main_j,G.main_g).second){
                // copy the conflict graph of this edge
                copy_conflict_edge(main_i,main_j,G,*vi,*vj,subG);
            }
        }
    }
    */
    return true;
}
