#ifndef IMPORT
#define IMPORT

#include <iostream>
#include <fstream>
#include <map>
#include <stdlib.h> // rand
#include <boost/graph/erdos_renyi_generator.hpp>
#include <boost/random/linear_congruential.hpp> // ERGen
#include "datastructure.hpp"
#include <regex> //to match

using namespace boost;
typedef erdos_renyi_iterator<minstd_rand, Graph> ERGen;

void print_conflict_graph(ConflictGraph);
int export_conflict_graph_graphviz(ConflictGraph, std::string);
int export_main_graph_graphviz(ConflictGraph, std::string);

vertex_desc add_conflict_vertex(int, ConflictGraph&);
std::pair<edge_desc,bool> add_conflict_edge(vertex_desc, int, vertex_desc, int, ConflictGraph&);

//bool create_conflict_graph_from_graphviz(ConflictGraph&, std::string);

int generate_random_conflict_graph_erdos_renyi(ConflictGraph&, int, float, int, float);

int generate_random_conflict_tree_prufer(ConflictGraph&, int, int, float);

bool import_conflict_graph_graphviz(std::string, ConflictGraph&);

bool copy_conflict_edge(vertex_desc, vertex_desc, ConflictGraph, vertex_desc, vertex_desc, ConflictGraph&);
bool create_subgraph(ConflictGraph, std::unordered_set<vertex_desc>, ConflictGraph&, std::map<vertex_desc,vertex_desc>&);

#endif