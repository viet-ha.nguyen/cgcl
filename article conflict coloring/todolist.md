

# Notes about biology

## experimental techniques for large assemblies:

* X ray crystallography -- very few cases ... one Nobel in chemistry for many of them. exple: ribosome

* cryo electron microscopy: much more effective ... but low
  resolution. whence our problem
  
  THIS IS NOT ROUTINE ANYWAY


## dynamics 
* what matters: atomic resolution models : physical chemistry explains
mechanisms at the atomic level (binding energies, conformational changes, etc)

* but for complex machines with 10 or more subunits: very difficult to
  obtain such models, *setting aside* their conformational
  diversity. in fact, it is already a tour de force to obtain one
  conformation.
  
  our goal: using one coarse model of the machine + atomic models of
  the subunits, find plausible geometries corresponding to the various


## why size of ~10 vertices

the complexite lies in the number of colors, not in the # of vertices


## number of solutions and their significance

* conformations are continuous

* models from physical chemistry: one route to explain mechanisms, in
the realm of 'energy landscapes': (1) enumerate low energy local
minima (2) use a simplified model (eg a harmonic model HM) for each of
them (3) derive the physics from the HM

in our case: with O(100 - 10,000) local minima: could build very
informative models

NB: our conformations can be used to run a minimization of the score/energy,
so to as to obtain the local minima needed to do the physics


## about tau

physical equivalent of tau: overlap of the molecules, direct
interpretation in terms of interaction potential energy.
https://en.wikipedia.org/wiki/Molecular_mechanics

conclusion is: we can enumerate low potential energy conformations



Intro: Dorian
Exp: Ha
Ccl: FC

