

\section{Experiments}
%%i%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Geometric random instances of conflict coloring}
\label{sec:experiment-intro}
%%ii-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%

As a test set, we consider instances associated with Voronoi diagrams
in the plane.   This  geometric setting is especially interesting for several reasons:
(i) the number of nodes (cells in the Voronoi diagram) can be tuned, and so is the number of
colors (ii) the combinatorial structure of the Voronoi diagram can be used to infer
local thresholds/constraints.
\medskip
\toblack

Given a set of 2D data points, recall that
the Voronoi diagram (VD) is the partition of the plane into convex regions,
such that all points in one region have a data point as nearest
neighbor \cite{boissonnat1998algorithmic}.  The 2D VD has a dual
structure known as the Delaunay triangulation (DT), which (generically)
consists of vertices, edges and triangles. 

Consider the DT of a dataset $P$ of $N$ points drawn uniformly at random in a square of
side $\sqrt{N}*100$. (This ensures that the average density
of points is the same for any value of $N$, see Remark \ref{rmk:rel-error} below).
Using this  geometric setting, we  define geometric instances of the
conflict coloring problem as follows (Figure \ref{fig:del-vor-instance-colors}, Figure \ref{fig:perturbed-poly}):
%%
\begin{itemize}
\item (Nodes) The nodes of the graph correspond to the data set $P$ of
  $N$ points. More precisely, we retain as vertices those data points
  have a {\em finite Voronoi region} (there are $N'$ such vertices),
  or equivalently, which do not lie on the convex hull of the data
  points.

\item (Conformations/colors) Consider a data point $p\in P$ and its
  Voronoi region $V_r(p)$ -- the index $r$ stands for {\em
    reference}. Let $v$ be a {\em Voronoi vertex} of $V_r(p)$. (Note
  that all such points are finite since $p$ is not on the convex
  hull.)  We take a random perturbation of $v$ in
  $\mathcal{D}(v,\delta) \setminus V_r(p)$ where $\mathcal{D}(v,
  \delta)$ is the disc centered at $v$ and radius $\delta$.  The set
  of all such perturbed Voronoi vertices defines a random (perturbed)
  polygon for $p$.  The set of all perturbed regions (polygons) for
  point $p$, denoted $\colorsforp{p}$, defines its colors.

\item (Conflict) Consider two Voronoi regions $V(p)\in \colorsforp{p}$
  and $V(q)\in \colorsforp{q}$ of two neighboring points in DT.  We
  compute the surface area of the intersection of these two regions,
  denoted $ia(V(p), V(q))$ for {\em intersection area}. The regions $V(p)$ and
  $V(q)$ are termed in conflict if $ia(V(p), V(q)) \geq
  \tau_c$, for some threshold $\tau_c$.
\end{itemize}

Summarizing, a random instance is defined by the four tuple: $I = (P,
\delta, C, \tau_c)$ with $P = \{p_1,\dots, p_N \}$ a set of $N$ points,
$\delta$ the radius used to perturb the Voronoi vertices, $c$ the
number of colors/randomly perturbed Voronoi regions, and $\tau_c$ the
threshold to define conflicts.

\begin{figure}[!ht] 
\centerline{ 
\includegraphics[height=.75\linewidth]{fig-experiments/del-vor-instance-colors}}
\caption{{\bf Random instance of color matching defined from perturbations of a 2D Voronoi diagram.}
%%
{\bf (A)} In a random 2D Voronoi diagram, points with a finite Voronoi
cell (bold cells) a retained -- $p_1,\dots,p_7$ in the example; each
such point corresponds to the vertices in the considered graph. The
dotted line segments represent the Delaunay triangulation.
%%
{\bf (B)} To create the {\em colors} associated with a node $p$ of the
graph, for each Voronoi vertex $v$ of the  Voronoi cell $V_r(p)$, a perturbed point of $p$ is 
uniformly chosen at random in $\mathcal{D}(v, \delta) \setminus V_r(p)$.}
\label{fig:del-vor-instance-colors} 
\end{figure} 

\begin{figure}[!ht] 
\resizebox{\columnwidth}{!}{
\begin{tabular}{ccc}
\includegraphics[height=.35\linewidth]{fig-experiments/perturbed_r05.pdf}&
\includegraphics[height=.35\linewidth]{fig-experiments/perturbed_r1.pdf}&
\includegraphics[height=.35\linewidth]{fig-experiments/perturbed_r2.pdf}
\end{tabular}
}
\caption{ {\bf Instances corresponding to the construction of
    Figure~\ref{fig:del-vor-instance-colors} upon varying the
    perturbation radius $\delta$.}  Perturbed random polygons of the
  finite Voronoi regions (thick black polygons) given a data points of
  size $N = 15$ after generating with perturbed radius $\delta = 0.5$
  (left), $\delta = 1$ (middle) and $\delta = 2$ (right) respectively
  and $c = 5$.}
\label{fig:perturbed-poly} 
\end{figure} 



\begin{remark}(Implementation notes.)
Recall that a polygon is termed {\em simple} if two of its edges do
not intersect, except possibly at their endpoints.
In this work, we use polygon representation provided by the Computational Geometry Algorithm Library \cite{cgal}, 
see \url{https://doc.cgal.org/Manual/3.1/doc_html/cgal_manual/Polygon/Chapter_main.html}.
%%
For the sake of simplicity, in sampling conformations, we retain simple polygons only.
\end{remark}

\subsection{Parameters monitored}

A random instance and a threshold $\tau_c$ yield a set of conflicts.
We wish to get insights on the relationship between the value of
$\tau_c$ derived from {\em local sufficient conditions}, and the
existence of global solutions.
\paragraph{Local constraints based on min intersection areas.}
We use the topological information contained in the DT to derive
sufficient conditions on the existence of {\em local} solutions.
More specifically, we consider three nested local
levels:
\toblack
%%
\begin{itemize}
\item {\sc Edges.} For an edge $p_ip_j$ of the DT, the reference regions
  $V_r(p_i)$ and $V_r(p_j)$ share a Voronoi edge, and the perturbed
  regions may overlap.  For this edge, we compute the minimum intersection area  
  $\tauedge{p_ip_j}$ of their perturbed regions, which ensures the
  existence of a local solution, this can be formalized as follows: 
\begin{equation}
\label{eq:tauedge}
\tauedge{p_ip_j} = \pmb{\min\limits_{\substack{V_i \in \colorsforp{p_i}\\ V_j \in \colorsforp{p_j}}}} ia(V_i, V_j).
\end{equation}

The max value obtained over all edges is denoted $\tauedgemax{Min}$.

\item {\sc Triangles.} Consider a triangle $(p_ip_jp_k)$ (face) in DT. In a
  manner analogous to edges, we compute the minimum value
  $\tautriangle{p_ip_jp_k}$ required have
  a local solution for this triple. Formally:

\begin{equation}
\label{eq:tautriangle}
\tautriangle{p_ip_jp_k} = \pmb{\min\limits_{\substack{V_i \in \colorsforp{p_i}\\ V_j \in \colorsforp{p_j} \\ V_k \in \colorsforp{p_k}}}} \max \{ ia(V_i, V_j), ia(V_i, V_k), ia(V_j, V_k) \}.
  \end{equation}
The max value obtained over all triangles is denoted $\tautrianglemax{Min}$.

\item {\sc Stars.} Finally, we consider the star centered at each data point
  $p_i$, that contains $p_i$ and its set of neighbors $N(p_i)$ in the
  DT with finite dual Voronoi cell.  We compute the minimum value
  $\taustar{p_i}$ of the threshold, which ensures the existence of a
  local solution of the star. Formally:

\begin{equation}
\label{eq:taustar}
\taustar{p_i} = 
\pmb{\min\limits_{V_i\in \colorsforp{p_i}}} \max\limits_{p_j \in N(p_i)} \min\limits_{V_j \in \colorsforp{p_j}} ia(V_i, V_j).
\end{equation}


The max value obtained over all stars is denoted $\taustarmax{Min}$.
\end{itemize}

The values $\tauedge{p_ip_j}$, $\tautriangle{p_ip_jp_k}$ and
$\taustar{p_i}$ provide local necessary conditions. Observe that, in
order to have a global solution for an instance, the threshold $\delta_c$
must be at least all these local values.
We therefore define 
%%
\begin{equation}
\label{eq:taulocmaxmin}
\taulocmax{min} = \max \{ \tauedgemax{Min}, \tautrianglemax{Min}, \taustarmax{Min} \}.
\end{equation}

\paragraph{Local constraints based on average intersection areas.}
In the previous equations, one uses the most stringent constraint
for an edge / triangle / star.  Consider the case where one increases
the number of colors: the min values observed for an edge / triangle /
star is expected to decrease. Which means in turn that in increasing
the number of colors, fewer solutions are expected overall. We also consider a variant
of the problem where the  local thresholds are defined from mean intersection areas.
%%
Replace the (bold) min by mean in Equations  (\ref{eq:tauedge}), (\ref{eq:tautriangle}), (\ref{eq:taustar})
yield the following:
%%
\begin{equation}
\label{eq:taulocmaxmean}
\taulocmax{Mean} = \max \{ \tauedgemax{Mean}, \tautrianglemax{Mean}, \taustarmax{Mean} \}.
\end{equation}

Let $m$ be some multiplicative factor -- that will differ for 
$\tauglobmin$ and  $\tauglobmean$.  Using the previous quantities, we  define global 
values of $\tau_c$ as follows:
\begin{equation}
\label{eq:tausglob}
\begin{cases}
\tauglobmin = m\times\taulocmax{Min},\\
\tauglobmean  = m\times\taulocmax{Mean}.
\end{cases}
\end{equation}





\begin{remark}
\label{rmk:rel-error}
In sampling data points in a square of side $\sqrt{N}*100$, the
expected area for each VD region is $10^4$, the threshold values are
in unit and we would mention also in brackets the fraction of a
threshold value over the expected area, for example $\tauloc = 100$
(1\% of the expected area of a VD region).
\end{remark}



\subsection{Results}
%%ii-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%

\paragraph{Setup.}

We consider two complementary analysis: {\em the fraction of solutions in the entire search space, and the existence of at least one solution.}

The following values are used in our experiments.
%%
Random instances are generated with parameters $N = 15,\delta = 1$.

For each instance, we compute the values of $\tauglobmin$ and
$\tauglobmean$.  We retain random instances whose $\taulocmax{Min}
\leq 100$ (thus at most 1\% of the expected area of a VD polygon).
%%
The number of colors used in the  experiments varies in the set
$C \in \{ 5, 10, 15, 50, 100\}$.
%%
The multiplicative factors vary  $ m\in m_1 = \{ 1.01, 1.5, 2, 3, \dots, 10 \}$ for $\tauglobmin$ and $m \in m_2 =  \{ 0.1, 0.4,0.7,\dots,2.2, 3, 5 \}$ for $\tauglobmean$.


\subsubsection{Conflict coloring is hard}

\paragraph{Statistic.}
A combinatorial problem may display a phase transition
\cite{hartmann2006phase}, with instances moving from easy to hard when
the parameters evolve. We study the hardness of conflict coloring in
this respect, using the {\em fraction of valid configurations}
$\statfmc$, namely fraction of valid configurations over the entire
search space $\prod_{p_i\in P} \colorsforp{p_i}$. We compute the value
of $\statfmc$ (given the values of $\tauglobmin$ and $\tauglobmean$),
by enumerating all these configurations.
%%
We repeat the calculation  $R=100$, and make a violin plot of the values obtained\footnote{
To run the algorithm, we  use 
\href{https://choco-solver.org/}{Choco}-a CSP solver see \cite{jussien2008choco}.}.


\paragraph{Results.}
The results show a marked difference for values of $\tauglobmin$
vs. $\tauglobmean$ (Figure~\ref{fig:violin_plots}).
%%
For the former, values of $f_{1.01}$ and $f_{2}$ are very small, and
overall, the distribution of $\statfmc$ is shifted towards smaller values
when the number of colors $c$ increases.
%%
This is sound, as increasing the number of colors is expected to
decrease the intersection area, whence the value of $\taulocmax{Min}$.
On the other hand, $\taulocmax{Mean}$ being defined from
average values, one expect its value to be less sensitive to the
number of colors (Figure~\ref{fig:distribution_tau_plots}).
%%
This lesser sensitivity is obvious from the violin plots associated
with $\tauglobmean$, since these are much less sensitive to the number
of colors.


\begin{figure}[!ht] 
\resizebox{\columnwidth}{!}{
\begin{tabular}{c c c c}
\rotatebox{90}{$\taulocmax{Min}$} &
\includegraphics[scale=.35]{fig-experiments/xp1listm-min-8-100/plot_violin_frac_sol_multis_c5.pdf} & \includegraphics[scale=.35]{fig-experiments/xp1listm-min-8-100/plot_violin_frac_sol_multis_c10.pdf} &
\includegraphics[scale=.35]{fig-experiments/xp1listm-min-8-100/plot_violin_frac_sol_multis_c15.pdf}\\
\rotatebox{90}{$\taulocmax{Mean}$} & 
\includegraphics[scale=.35]{fig-experiments/xp1listm-mean-8-100/plot_violin_frac_sol_multis_c5.pdf} & \includegraphics[scale=.35]{fig-experiments/xp1listm-mean-8-100/plot_violin_frac_sol_multis_c10.pdf} &
\includegraphics[scale=.35]{fig-experiments/xp1listm-mean-8-100/plot_violin_frac_sol_multis_c15.pdf}\\
& $c=5$ & $c=10$ & $c=15$
\end{tabular}}
\caption{{\bf Violin plots for the fraction $\statfmc$ of valid configurations
in $\prod_{p_i\in P} \colorsforp{p_i}$, for fixed values of the number of colors $c$ and the
multiplicative factors $m$ in Equation~\ref{eq:tausglob}.}
  Each plot features three sub-plots corresponding to the number of colors $c \in
  \{5,10,15\}$; each sub-plot features violins where each of which is corresponding to a multiplicative value in $m_1$ for for $\taulocmax{Min}$, or $m_2$ for $\taulocmax{Mean}$. Each violin
  plot corresponds to $R=100$ random instances.  These experiments
  involve $N'=8$ finite Voronoi regions.}
\label{fig:violin_plots} 
\end{figure} 

\subsubsection{Existence of one solution}

\paragraph{Statistic.}
Our second analysis focuses on the existence of at least one solution,
depending of our global thresholds $\tauglobmin$ and $\tauglobmean$
(Equation~\ref{eq:tausglob}).  We run our algorithm on $R=100$ instances,
and define the  success rate as  $\statsmc=\#cases\ with\ one\ solution / R$.


\paragraph{Results.}
Statistics on $\statsmc$ complement those on $\statfmc$
%%
Consider first the multiplicative factors $m$.  It clearly appears
that $\statsmc$ increases when increasing $m$, for both
$\taulocmax{Mean}$ and $\taulocmax{Min}$ (Figure~\ref{fig:plot_rate_s}).
%%
Consider next the number of colors $c$.  While the statistic
$\statsmc$ hardly depends on the number of colors $c$ for
$\taulocmax{Mean}$, it does so for $\taulocmax{Min}$. Namely, in using
$\taulocmax{Min}$, $\statsmc$ decreases when increasing the number of
colors.
 More precisely, our experiment shows that for $c \in \{ 5, 10, 15, 50, 100 \}$, the average of $\taulocmax{mean}$ is respectively around $(120, 100, 97, 90, 91)$ and $(38, 40, 11, 1.3, 0.05)$ for $\taulocmax{min}$. It explains the statistics of $\statsmc$  (Figure~\ref{fig:plot_rate_s}) and also  $\statfmc$ (Figure~\ref{fig:violin_plots}).
%% \st{ It seems contrary for $\taulocmax{Mean}$, when $c$   increases, the rate $\statsmc$ increases (steady).}


\begin{figure}[!ht] 
\resizebox{\columnwidth}{!}{
\begin{tabular}{cc}
\includegraphics[scale=.5]{fig-experiments/xp1listm-min-8-100/plot_percent_of_pos_f_multis_numc2.pdf} &
\includegraphics[scale=.5]{fig-experiments/xp1listm-mean-8-100/plot_percent_of_pos_f_multis_numc2.pdf}\\
 $\taulocmax{Min}$ & $\taulocmax{Mean}$
\end{tabular}}
\caption{ {\bf Heat map representing the 
 fraction $\statsmc$ of instances with at least one solution, for fixed values of the number of colors $c$ (horizontal axis) and the multiplicative factor $m$  (vertical axis).}
%%
In the heat-map and the Tables: rows (resp. columns) correspond to values of $c$ (resp. $m$).
A total of $R=100$ random instances were used. White color infers value 0.}
\label{fig:plot_rate_s} 
\end{figure} 


\begin{figure}[!ht] 
\resizebox{\columnwidth}{!}{
\begin{tabular}{cc}
\includegraphics[scale=.5]{fig-experiments/xp1listm-min-8-100/plot_percent_of_conflict_edges_f_multis_c.pdf} &
\includegraphics[scale=.5]{fig-experiments/xp1listm-mean-8-100/plot_percent_of_conflict_edges_f_multis_c.pdf}\\
 $\taulocmax{Min}$ & $\taulocmax{Mean}$
\end{tabular}}
\caption{ {\bf Heat map representing the fraction of conflict edges,
    averaged over all edges of the Delaunay triangulation.  (Note that
    for a Delaunay edge, there are at most $c^2$ conflict edges.)}
%%
 Total run of $R =  100$. Pink color maps to values in $[0, 10^{-3})$.}
\label{fig:plot_density_conflict} 
\end{figure} 

\subsection{Running times}
We provide the running times of checking the existence of one solution for $\taulocmax{min}$ and $\taulocmax{mean}$, see Figure~\ref{fig:running_time}. Clearly, the running augments when increasing $c$ in general. 
  Figure~\ref{fig:running_time} also show that  the time lines corresponding to values $m$ in the left plot are quite similar. In the other hand, those in the right plot tend to 0 when $m$ increases which is to say that instances get easier. Hence, the difficulty of an instance does not depend on $m$ for $\taulocmax{min}$ but it does for $\taulocmax{mean}$. 
  
On computational complexity point of view, {\sc Conflict Coloring} is very hard which the running time for solving the problem is exponential to the size of the input. However, the statistics show that the running time does not explode for $c$ big enough and $N$ moderate. Indeed, they are less than 0.25 seconds for $N = 15$ (with $N' = 8$) and $c \leq  100$ (Figure~\ref{fig:running_time}). Hence, it is possible to solve the problem {\sc Conflict Coloring} in practice (with instances of tens of subunits). 

\begin{figure}[!ht]
\resizebox{\columnwidth}{!}{
\centering{
\includegraphics[scale=.5]{fig-experiments/xp1listm-min-8-100/plot_running_time_algo_f_numc_multi.pdf}
\includegraphics[scale=.5]{fig-experiments/xp1listm-mean-8-100/plot_running_time_algo_f_numc_multi.pdf}
}}
\caption{{\bf Tables of average running times checking the existence of one solution for $\taulocmax{Min}$ (left) and $\taulocmax{Mean}$ (right) (in second).} Total run of $R = 100$. }
\label{fig:running_time}
\end{figure}


\subsection{Values and thresholds: upper bounds, lower bounds, and solutions}
\label{sec:cc_outlook}

From our experimental work, the statistics provide useful information
about the relation between local constraints and global solutions.

\medskip

Application-wise, an important question is the adjustment of lower and
upper values for the thresholds $\tau_c$, in order to get useful
information.  In the sequel, we explain how to infer such values via
the choice of the multiplicative factor $m$, for the case of
$\taulocmax{mean}$ -- that of $\taulocmax{min}$ is similar.

The upper bound is generally application dependent.  Large values of
$\tau_c$ are such that most colorings are valid, yet, such large
$\tau_c$ may be irrelevant application-wise.
%%
For example, transposing our Delaunay-Voronoi setting to molecular
modeling, large intersection volumes between atomic representations
(the equivalent of the intersection area used in this work) directly
translate into prohibitive van der Waals interaction energies~\cite{force} (which explains interactions of subunits from physical aspects).

To choose a lower bound, one seeks the smallest value of
$m$ yielding the desired value of $s_{c,m}$.
%%
In our experiments, for $s_{c,m}=1$, $m \leq 0.7$ suffices whatever
the value of $c$ (the right plot of Figure~\ref{fig:plot_rate_s}).  For $s_{c,m} = s_0
(<1)$, relevant pairs $(c,m)$ are obtained by seeking the smallest
values of $m$ and $c$ such that $s_{c,m} \geq s_0$.

In addition, we provide
  Figure~\ref{fig:plot_density_conflict} which shows the density of
  conflict graphs corresponding to pairs $(c, m)$. In fact, the
  statistics of density is opposite to the one of $\statsmc$ (\ie for
  a fixed $c$, when $m$ increases, a conflict graph is clearly sparser
  and it is the contrary for a fixed $m$ and when $c$ increases).
  Regarding pairs $(c,m)$ whose $\statsmc = 1$, the statistics show
  that the density is at most $47\%$ when $(c = 15 ,m = 4)$ for
  $\taulocmax{min}$ and at most $78\%$ when $(c = 100, m= 0.1)$ for
  $\taulocmax{mean}$. And the density expects to increase when 
  $c$ increases (but $m$ does not) for $\taulocmax{min}$ but it keeps stable for
  $\taulocmax{mean}$ when $c$ increases.  In this latter case,
a high fraction of conflict edges does not jeopardize the existence of
solutions, as evidenced \eg by the pair $(c = 100, m=0.1)$ (fraction
of conflict edges $\sim 0.8$ and $s_{100, 0.1} = 1$).


\begin{figure}[!ht] 
\resizebox{\columnwidth}{!}{
\begin{tabular}{cccc}
\rotatebox{90}{With Min values}&
\includegraphics[scale=.35]{fig-experiments/xp1listm-mean-8-100/plot_min_intersections_edge_num_c2.pdf} &
\includegraphics[scale=.35]{fig-experiments/xp1listm-mean-8-100/plot_min_intersections_face_num_c2.pdf} &
\includegraphics[scale=.35]{fig-experiments/xp1listm-mean-8-100/plot_min_intersections_star_num_c2.pdf}\\
%%
& $\tau_2$ & $\tau_3$ & $\tau_{*}$\\
%%
\rotatebox{90}{With Mean values}&
\includegraphics[scale=.35]{fig-experiments/xp1listm-mean-8-100/plot_mean_intersections_edge_num_c2.pdf}&
\includegraphics[scale=.35]{fig-experiments/xp1listm-mean-8-100/plot_mean_intersections_face_num_c2.pdf}&
\includegraphics[scale=.35]{fig-experiments/xp1listm-mean-8-100/plot_mean_intersections_star_num_c2.pdf}
\end{tabular}}
\caption{{\bf Distribution of intersection areas $ia(\cdot, \cdot)$ for Min  (top row) and Mean values (bottom row).}
The three plots correspond to $\tauedge$, $\tautriangle$ and $\taustar$, respectively.
A total of $R=100$ random instances were used. Note that $y$-axis presents the average number of edges, faces or stars of an instance; and $x$-axis infers values  $\lfloor ia(\cdot, \cdot) \rfloor$ .}
\label{fig:distribution_tau_plots} 
\end{figure} 
