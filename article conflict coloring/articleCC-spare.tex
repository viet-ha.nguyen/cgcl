

\tored

\section{Approximation algorithm for {\sc Fulfill $2$-Coloring} using Semidefinite programming} \label{sec:approximation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Observe that  {\sc Max cut} is the particular case of {\sc Fulfill $2$-Coloring} in which the conflict $2$-graph of any edge $e=uv$ contains two edges $(u,1)(v,1)$ and $(u,2)(v,2)$. 
Goemans and Williamson~\cite{GoWi95} showed a 0.87856-approximation algorithm for  {\sc Max Cut} based on semidefinite programming. In this section, we show how this method can be adapted to construct an approximation algorithm for {\sc Fulfill 2-Coloring} and use the technique in \cite{GoWi95} by Goemans and Williamson. We will prove the following result.

\begin{theorem}
The problem {\sc Fulfill 2-Coloring} is 0.79607-approximable.
\end{theorem}
\begin{proof}

Let $V = \{v_{1}, \dots, v_{n}\}$ be the set of vertices of a given a graph $G$.
Let $y_i \in \{-1, 1\}$ be a variable corresponding to $v_i$.
Let  $y_0$ be an additional variable which determines if $1$ corresponds to color $1$ or color $2$. Precisely, $y_i = y_0$ if and only if $v_i$ has color $1$.

Let $e$ be an edge of $G$. We shall define a formula $f(e)$ depending on $M_e$ which is equal to 1 (resp. 0) if the edge is fulfilled (resp. conflict).  $M_e$ is one of the conflict $2$-graphs depicted in Figure \ref{fig:B2x}: there are $i$ and $j$ such that 
$e=v_iv_j$ and $v_i$ is the bottom vertex and $v_j$ the top vertex.
\begin{figure}[hbtp]
\centering{
\includegraphics[scale=1]{figures/graph_B(2,x).pdf}}
\caption{The possible conflict $2$-graphs (up to top-bottom symmetry). Blue is color $1$ and red is color $2$.}
%, in the sense that, for any edge $e$, there is an order of its two endpoints such that its conflict $2$-graph is one of those in the figure.}
\label{fig:B2x}
\end{figure}

 
Observe that if $M_e=M_0$, then $e$ is fulfilled, so we set $f(e)=1$, and if  $M_e=M_4$, then $e$ is never fulfilled, so we set $f(e)=0$. 


\noindent $f(e) = \dfrac{1-y_0y_i}{4}+\dfrac{1-y_0y_j}{4}+\dfrac{1-y_iy_j}{4}$ ~~ if $M_{e}=M_1^1$~;\\
$f(e) = \dfrac{1-y_0y_i}{4} +\dfrac{1+y_0y_j}{4} + \dfrac{1+y_iy_j}{4}$ ~~ if $M_{e}=M_1^2$~;\\
$f(e) = \dfrac{1+y_0y_i}{4} +\dfrac{1+y_0y_j}{4}+ \dfrac{1-y_iy_j}{4}$ ~~ if $M_{e}=M_1^3$~;\\
$f(e) = \dfrac{1-y_iy_j}{2}$ if $M_{e} = M_2^1$~; ~~~~~~~~ ~~~~~~~~ ~~~
$f(e) =\dfrac{1+y_iy_j}{2}$ if  $M_{e} = M_2^2$~; \\
$f(e) = \dfrac{1-y_0y_i}{2}$ if $M_{e} = M_2^3$~; ~~~~~~~~  ~~~~~~~~ ~~~
$f(e) = \dfrac{1+y_0y_i}{2}$ if $M_{e} = M_2^4$~;\\
$f(e) = \dfrac{1-y_0y_i-y_0y_j+y_iy_j}{4}$ if  $M_{e} = M_3^1$~; ~~~
$f(e) = \dfrac{1+y_0y_i+y_0y_j+y_iy_j}{4}$  if $M_{e} = M_3^2$~; \\
$f(e) = \dfrac{1+y_0y_i-y_0y_j-y_iy_j}{4}$ if $M_{e} = M_3^3$.



Note that if $M_e\in \{M_1^1, M_1^2, M_1^3\}$, then the formulas are the ones for {\sc Max 2-Sat} (where $c(u) = 1$ corresponds to $u$ being {\it true} and $c(u)=2$ to $u$ being {\it false}),  if $M_e\in \{M_2^1, M_2^2\}$, then the formulas are the ones for {\sc Max Cut}, and if  $M_e\in \{M_3^1, M_3^2, M_3^3\}$, then the formulas are similar to those of {\sc MaxDiCut}~\cite{GoWi95}.


 We formulate our problem as an integer programming problem (P) which maximizes $S=\sum_{e\in E(G)} f(e)$ subject to $y_i \in \{-1, 1\}$. Since in any case $f(e)$ is a nonnegative linear combination of $1-y_iy_j$, $1+y_iy_j$ and $1-y_iy_j-y_iy_k+y_jy_k$, $1+y_iy_j+y_iy_k+y_jy_k$, there are nonnegative coefficient $a_{ij}$, $b_{ij}$, $c_{ijk}$ and $d_{ijk}$ such that 
 $$S=  \sum_{i,j,k} a_{ij}(1-y_iy_j)+ b_{ij}(1+y_iy_j)+c_{ijk}(1-y_iy_j-y_iy_k+y_jy_k)+d_{ijk}(1+y_iy_j+y_iy_k+y_jy_k).$$

We can relax (P) by replacing every $y_i$ to a unit vector $x_i \in S_n$, the $n$-dimentional unit sphere. 
This results in the problem (P') which maximizes 
$$ S'= \sum_{ij \in E} a_{ij}(1-x_ix_j)+ b_{ij}(1+x_ix_j)+c_{ijk}(1-x_ix_j-x_ix_k+x_jx_k)+d_{ijk}(1+x_ix_j+x_ix_k+x_jx_k)$$ subject to $x_i \in S_n$. This is a semidefinite programming problem with positive semidefinite matrix $X$ with $X_{ij}=x_ix_j$. Using the method of  Goemans and Williamson, we have the following algorithm.
\begin{enumerate}
\item Solve (P')%\eqref{eq2}.
\item Take a uniformly random vector $r$ on the unit sphere $S_n$.
\item For $0\leq i\leq n$,  set $y_i=-1$ if $x_ir <0$, and $y_i=+1$ otherwise.
\item For $1\leq i\leq n$, set $c(v_i) = 1$ if $y_iy_0=1$ and $c(v_i) = 2$ otherwise.
\end{enumerate}

\medskip

$y_i$ may se seen as the sign of $x_ir$.  By Step 3 and 4, two vertices $v_i$ and $v_j$ have different colors if and only if $y_i\neq y_j$. This happens with probability
 $\dfrac{\theta_{ij}}{\pi}$ where  $\theta_{ij}$ is the angle between $x_i, x_j$. Note that $x_ix_j = \cos \theta_{ij}$. 

\medskip

Let $\ds S_a= \sum_{i,j} a_{ij}(1-y_iy_j)$, $\ds S_b=\sum_{i,j} b_{ij}(1+y_iy_j)$,  $\ds S_c= \sum_{i,j,k} c_{ijk}(1-y_iy_j-y_iy_k+y_jy_k)$ and $S_d=\ds \sum_{i,j,k} d_{ijk}(1+y_iy_j+y_iy_k+y_jy_k)$.
We have $S=S_a+S_b + S_c + S_d$, so by linearity of the expectation $\E(S) =\E(S_a) + \E(S_b) + \E(S_c) +\E(S_d)$.

\begin{eqnarray*}
\E(S_a) &= &  2\sum_{i,j} a_{ij} \Pee(y_i\neq y_j) = 2\sum_{i,j}  a_{ij} \dfrac{\theta_{ij}}{\pi} \\
&\geq &\alpha \sum_{i,j} a_{ij} (1- \cos \theta_{ij})
= \alpha \sum_{i, j} a_{ij}(1-x_ix_j)
\end{eqnarray*}
where $\ds  \alpha = \min_{0\leq \theta \leq \pi} \dfrac{2}{\pi}\dfrac{\theta}{1-\cos \theta} > 0.87856 $.

\smallskip
Similarly, $\ds \E(S_b) \geq \alpha \sum_{i,j} b_{ij}(1+x_ix_j)$.


\begin{eqnarray*}
\E(S_c) &= & 4 \sum_{i,j,k} c_{ijk}\Pee(y_i\neq y_j=y_k)\\
& \geq & 4\sum_{i,j,k} c_{ijk}\dfrac{\theta_{ik}+\theta_{jk}-\theta_{jk}}{2\pi} \\
& \geq & \beta \sum_{i,j,k} c_{ijk}(1-x_ix_j-x_ix_k+x_jx_k)
\end{eqnarray*}
where $\ds  \beta = \min_{0\leq \theta \leq \arccos(-1/3)} \dfrac{2}{\pi}\dfrac{2\pi-3\theta}{1+3\cos \theta} > 0.79607 $ as shown
in ~\cite{GoWi95}.

\smallskip
Similarly,  $\ds \E(S_d) \geq \beta \sum_{i,j,k} d_{ijk}(1+x_ix_j+x_ix_k+x_jx_k)$.



\medskip
Hence 
\begin{eqnarray*}
\E(S) & \geq &   \alpha \sum_{i, j} \bigg(a_{ij}(1-x_ix_j) + b_{ij}(1+x_ix_j) \bigg)\\
& & + \beta \sum_{i,j,k} \bigg ( c_{ijk}(1-x_ix_j-x_ix_k+x_jx_k)+ d_{ijk}(1+x_ix_j+x_ix_k+x_jx_k)\bigg)\\
& & \geq \beta S'
\end{eqnarray*}
%& \geq \beta \text{ SDP}(P') \geq \beta \text{ OPT}(P)
%\end{align*}
But the $x_i$ have been computed to give the maxtimum value $OPT(P')$ of $S'$,
so $\E(S)\geq \beta OPT(P') \geq \beta OPT(P)$.  
Therefore, the algorithm is a $\beta$-approximation of {\sc Fulfill 2-Coloring}.
\end{proof}





%%%%%%%%%%%%%%%%
\section{Polynomial-time algorithms for graphs with bounded treewidth}
\label{sec:fpt}


In this section, we give a polynomial-time algorithm that solves the maximization version of {\sc Domino}, called {\sc Max Domino},
 when the treewidth of the graph is bounded.
In a similar way, we can prove that for each fixed positive integer $\tau$, there is a polynomial-time algorithm that solves {\sc Max-Threshold Fulfill Coloring}, when the treewidth of the graph is at most $\tau$.
Given a graph $G$, integers $k, q$, and a set of weight functions $w^k$, {\sc Max-Threshold Fulfill Coloring} consists in determining the maximum threshold $\lambda$ for which there is a $k$-coloring of $G$ that ${\cal M}^{\lambda}$-fulfills at least $q$ edges. This algorithm can be found in appendix.


We first introduce the notion of nice tree decomposition (Section~\ref{ssec:nice-tree}) which will be used in our algorithm (Section~\ref{ssec:algos-bounded-tw}).

%for problem  the more general problem in the paper (so it solves {\sc fulfill coloring}) and  for problem {\sc max-$\lambda$}.\\


\subsection{Nice tree decomposition}
\label{ssec:nice-tree}

A \textit{tree decomposition} of a graph $G = (V,E)$ is a pair $\mathcal{T}=(T, \mathcal{X})$, where   $T$ is a tree and $\mathcal{X} = \{X_x \mid x\in V(T)\}$ is a family of subsets of $V$, called {\it bags}.

%where $T$ is a tree and $X_t$ is a \textit{bag} (a subset of vertices of $V$), satisfying the following conditions:
\begin{enumerate}
\item Every graph vertex $v \in V$ is in at least one bag;

\item For every edge $uv \in E$, there is at least one bag that contains both $u$ and $v$;

\item For every graph vertex $v \in V$, the subgraph induced by the nodes whose bag contains $v$ is a subtree $T_v$ of $T$.
\end{enumerate}

\noindent
The \textit{width} of a tree decomposition $\mathcal{T}$ is $\tau({\cal T}) = \max_{B \in \mathcal{X}} |B|-1$, and the \textit{treewidth} of a graph $G$, denoted by $\tw(G)$, is the minimum width among all the possible tree decompositions of $G$.
Figure~\ref{fig:example_tree} depicts an example of simple graph and a tree decomposition of width $2$.

\begin{figure}[t]
  \centerline{
  	\includegraphics[scale=1]{figures/graph.pdf}
  	\hspace{2cm}
  	\includegraphics[scale=1]{figures/tree_dec.pdf}
  }
  \caption{A graph and a tree decomposition.}
  \label{fig:example_tree}
\end{figure}

%We also introduce the notion of {\it nice tree decomposition} which will be used in our dynamic programming algorithms.
%This tree decomposition has a root and so presents a child-parent relation on nodes. \\
% \{X_t\}_{t\in V(T)}\\
In order to keep our algorithm and its analysis simple, we use a particular kind of tree decompositions called \textit{nice tree decompositions}.
A \textit{nice tree decomposition} $\mathcal{T}=(T,\mathcal{X})$ is a tree decomposition such that $T$ is rooted at a node $r$ and satisfies the following conditions:
\begin{enumerate}
\item If $x \in V(T)$ is a leaf or the root, then $X_x = \emptyset$.
%For every leaf $l$ and the root $r$, $X_l = X_r = \emptyset$,

%For the other tree nodes,
\item Every other node $x$ of $T$ is one of the three following types:
	\begin{enumerate}
	\item $x$ is an \textit{introduce node} if $x$ has a unique child $y$ in $T$ and $X_x= X_{y} \cup \{v\}$ for some $v \in V(G)\setminus X_y$. We say that $v$ is {\it introduced at $x$}.
	
	\item $x$ is a \textit{join node} if $x$ has two children $x_{1}$ and $x_{2}$ in $T$ and $X_x = X_{x_1} = X_{x_2}$.
	
	\item $x$ is a \textit{forget node} if $x$ has a unique child $y$ in $T$  and $X_x = X_y \setminus \{v\}$ for some $v \in X_y$. We say that $v$ is {\it forgotten at $x$}.
	\end{enumerate}
\end{enumerate}


Figure~\ref{fig:example_nice} describes an example of nice tree decomposition of the graph depicted in Figure~\ref{fig:example_tree}.\\
Note that any graph vertex is forgotten only once (by Condition 3 of a tree decomposition) but may be introduced several times.

 
 
\begin{lemma}[\cite{Cygan}]\label{lem:nice_tree}
Given a tree decomposition $\mathcal{T} = (T, \mathcal{X})$ of width at most $\tau$ of a graph $G$, one can compute in $O(\tau^2\max(|V(G)|, |V(T)|))$ time a nice tree decomposition of width at most $\tau$ with at most $O(\tau.|V(G)|)$ nodes.  \end{lemma}
 
 
 
\subsection{Dynamic programming algorithm}
\label{ssec:algos-bounded-tw}
%%%%%%%%%%%%%%%%%%%%%%%%%%


%We prove in Theorem~\ref{the:bounded-treewidth} polynomial-time algorithms for the problems \textsc{Max Domino} and {\sc Max-$\lambda$ Fulfill Coloring} if the graph has bounded treewidth.

\begin{theorem}
\label{the:bounded-treewidth}
Given a graph $G = (V,E)$ with treewidth at most $\tau$, an integer $k \geq 1$, a set of weight functions $w^{k}$, the problem \textsc{Max Domino} can be solved in time $O(\tau^{2}  \cdot k^{\tau +2} \cdot |V|)$.
\end{theorem}

In order to prove this theorem, we need some definitions and observations.
Let $\mathcal{T} = (T, \mathcal{X})$ be a nice tree decomposition of $G$ and let $r$ be the root of $T$.
Let $x$ be a node of $T$.  We denote by $T_x$ the subtree of $T$ rooted at $x$ and containing all the descendants of $x$.
We construct the graph $G_{x} = (V_{x},E_{x})$ as follows.
$V_{x} = \bigcup_{y\in T_x} X_y$ and $E_{x}$ is constructed as follows.

\begin{enumerate}
	\item If $x$ is a leaf of $T$, then $E_{x} = \emptyset$.
	\item If $x$ is an introduce node with child $y$, then $E_{x} = E_{y}$. 
	\item If $x$ is a forget node with child $y$, then $E_{x}= E_{y} \cup \{vw\in E(G) \mid w\in X_x\}$ with $\{v \} = X_y\setminus X_x$.
	\item If $x$ is a join node of $T$ with children $x_{1}$ and $x_{2}$, then $E_{x} = E_{x_{1}} \cup E_{x_{2}}$. Note that $V_{x} = V_{x_{1}} \cup V_{x_{2}}$.
\end{enumerate}

{\centering
\begin{figure}[t]
  \centerline{\includegraphics[scale=1]{figures/nicetree_dec.pdf}}
  \caption{A nice tree decomposition of the tree graph depicted in Figure \ref{fig:example_tree}: the two leaves (leftmost nodes) and the root (rightmost node) are empty nodes, introduce nodes are blue, forget nodes are red and a join node is yellow.}
  \label{fig:example_nice}
\end{figure}
}

By the construction of $G_{x}$, every edge of $G_x$ has an endpoint that has been forgotten.

\medskip

Given a node $x \in V(T)$ and a $k$-coloring $c_{x}: X_x \rightarrow [k]$, we define $S(x,c_{x})$ as the value of an optimal solution for {\sc Max Domino} problem for the graph $G_{x}$ with the additional constraint that $c(b) = c_{x}(b)$ for every $b
\in X_x$.
Formally,
\begin{equation}\label{eq:def_S}
 S(x,c_{x}) = \max\limits_{\substack{c:V_{x} \to [k] \\ \forall b\in X_x, c(b) = c_{x}(b)}} \sum\limits_{\substack{uv \in E_x }} w^k((u,c(u))(v,c(v))). 
\end{equation}



\smallskip

By convention we set  $S(x,c_{x}) = 0$ when $x$ is a leaf.
We are now able to precisely design our dynamic programming algorithm, called {\sc Max Domino Algorithm}, that consists of computing $S(x,c_{x})$ for every $x \in V(T)$ and every $k$-coloring $c_{x}$ of $X_x$.
Lemma~\ref{lem:computation-algo-dyn} shows the formulas allowing such a computation.

\begin{lemma}
\label{lem:computation-algo-dyn}
Let $x$ be a node of $T$ and $c_x$ a $k$-coloring of $X_x$.

\begin{itemize}
\item[(a)] If $x$ is a join node with children $x_{1}$ and $x_{2}$, then $S(x,c_{x}) = S(x_{1},c_{x}) + S(x_{2},c_{x})$.
\item[(b)] If $x$ is an introduce node with child $y$, then $S(x,c_{x}) = S(y,c_{y})$, where $c_y$ is the $k$-coloring of $X_y$ such that $c_{y}(v) = c_{x}(v)$ for all $v \in X_y$.
\item[(c)] If $x$ is a forget node with child $y$, then
$$S(x,c_x) = \max\limits_{c_y\in {\cal C}(c_x)} \bigg\{ S(y, c_{y}) + \sum\limits_{\substack{w \in X_x \\ vw \in E_{x}}} w^k((v, c_y(v))(w, c_{y}(w))  \bigg\}$$ where $\{v\}= X_y\setminus X_x$ and ${\cal C}(c_x)$ is the set of $k$-colorings of $X_y$ such that
$c_{y}(b) = c_{x}(b)$ for all $b \in X_x$.
\end{itemize}
\end{lemma}



\begin{proof}
(a) Suppose $x$ is a join node with children $x_{1}$ and $x_{2}$.
We have $X_x = X_{x_{1}} = X_{x_2}$ and, by definition of a tree decomposition, any node in $V_{x_{1}} \setminus X_x$ (resp. $V_{x_{2}} \setminus X_x$) is not in $X_{x_{2}}$ (resp. $X_{x_{1}}$).
Thus $S(x,c_{x}) = S(x_{1},c_{x}) + S(x_{2},c_{x})$.


(b)  Suppose $x$ is an introduce node with child $y$.
Then by definition, $E_x=E_y$. So clearly $S(x,c_{x}) = S(y,c_{y})$.

(c) Suppose $x$ is a forget node with child $y$. Let $\{v\} = X_y\setminus X_x$.
By definition $G_x= G_{y} \cup \{vw\in E(G) \mid w\in X_x\}$.
Let $c^*$ be a $k$-coloring of $V_x$ such that $S(x,c_x)=   \sum\limits_{\substack{ab \in E_x }} w^k((a,c^*(a))(b,c^*(b)))$ and let $c_y$ be the restriction of $c^*$ to $X_y$.   Note that $c_y \in {\cal C}(c_x)$.
Then 
\begin{eqnarray*}
S(x,c_x) & = & \sum\limits_{\substack{ab \in E_y }} w^k((a,c^*(a))(b,c^*(b))) + \sum\limits_{\substack{vw \in E(G)\\ w\in X_x}} w^k((v,c^*(v))(w,c^*(w)))\\
& = & \sum\limits_{\substack{ab \in E_y }} w^k((a,c^*(a))(b,c^*(b))) + \sum\limits_{\substack{vw \in E(G)\\ w\in X_x}} w^k((v,c_y(v))(w,c_y(w)))
\end{eqnarray*}
Since $c^*$ maximizes this sum and the second term is the same for all coloring whose restriction to $X_y$ is $c_y$,
we get 
$S(x,c_x)= S(y,c_y) + \sum\limits_{\substack{vw \in E(G)\\ w\in X_x}} w^k((v,c_y(v))(w,c_y(w)))$. 
Therefore $S(x,c_x) = \max\limits_{c_y\in {\cal C}(c_x)} \bigg\{ S(y, c_{y}) + \sum\limits_{\substack{w \in X_x \\ vw \in E_{x}}} w^k((v, c_y(v))(w, c_{y}(w))  \bigg\}$
\end{proof}



\begin{proof}[Proof of Theorem~\ref{the:bounded-treewidth}]
The algorithm is the following:

\begin{enumerate} 
\item Compute a nice tree decomposition ${\cal T}=(T, \mathcal{X})$ of $G$ with width at most $\tau$.
\item Compute the value of $S(x,c_{x})$ for all node $x$ of $T$ and all $k$-coloring $c_x$ of $X_x$.
\item Return the maximum value of $S(r, c_r)$ over all $k$-colorings $c_r$ of the bag $X_r$ of the root $r$.
\end{enumerate}

\medskip
 
At Step 2, each value $S(x,c_{x})$ is computed using the formulas of Lemma~\ref{lem:computation-algo-dyn}.
If $x$ is a leaf, a join node or an introduce node, then the value of $S(x,c_{x})$ is obtained in constant time by the formula.
If $x$ is a forget node, then the algorithm takes the maximum over $k$ values because $|\mathcal{C}(c_x)|=k$ (there are $k$ possible colors for the vertex $v$ of $X_y\setminus X_x$).
The number of edge weights in the sum is upper-bounded by the size of $X_{x}$. 
Hence the value can be computed in $O(\tau \cdot k)$ time.\\
Now for each node $x$, there are at most $k^{\tau+1}$ possible $k$-colorings of $X_x$, so there are at most
$ k^{\tau+1}\cdot |V(T)|$ values  $S(x,c_{x})$, so at most $O(\tau \cdot k^{\tau+1} \cdot |V|)$ values by Lemma \ref{lem:nice_tree}. 
As each of these values is computed in $O(\tau \cdot k)$ time, Step 2 runs in
 $O(\tau^{2} \cdot k^{\tau+2} \cdot |V|)$  time.


Clearly Step 1 and  3 can be done faster than Step 2, so the total running time of our algorithm is $O(\tau^{2} \cdot k^{\tau+2} \cdot |V|)$.
\end{proof}

\appendix


%%%%%%%%%%%%%%%%%
\section{Polynomial-time algorithm for Max-Threshold Fulfill Coloring for bounded treewidth graphs}
\label{sec:computing lambda}
In this section, we will prove the following theorem.

\begin{theorem}
\label{the:bounded-treewidth-lambda}
Given a graph $G = (V,E)$ with treewidth at most $\tau$, an integer $k \geq 1$, a set of weight functions $w^{k}$, the problem \textsc{Max-threshold fulfill coloring} can be solved in time $O(\tau^{2}  \cdot k^{\tau +2} \cdot |V|)$.
\end{theorem}

Given a node $x \in V(T)$, a $k$-coloring $c_x: X_x \rightarrow [k]$, we define $\lambda(x, c_x)$ as the maximum threshold for {\sc max-threshold fulfill coloring} for graph $G_x$ with the constraint that $c(b) = c_x(b)$ for all $b \in X_x$.
 Observe that, the objective of {\sc max-threshold fulfill coloring} is to maximize $ \min\limits_{uv \in E_x} w^k((u,c(u))(v, c(v)))$. Formally, we have
\begin{equation}\label{eq:def_m}
 \lambda(x,c_x) = \max\limits_{\substack{c:V_x \to [k] \\ \forall b \in X_x,c(b)=c_x(b)}} \min\limits_{uv \in E_x} w^k((u,c(u))(v,c(v))).
\end{equation}

We set  $\lambda(x,c_x) = + \infty$ if $x$ is a leaf.
We are now able to precisely design our dynamic programming algorithm, called {\sc Max-Threshold Fulfill Coloring Algorithm}, that consists of computing $\lambda(x,c_x)$ for every $x \in V(T)$ and every $k$-coloring $c_{x}$ of $X_x$.
Lemma~\ref{lem:computation-algo-dyn-lambda} shows the formulas allowing such a computation.


%We will construct an algorithm that computes $\lambda(x,c_x)$ for every $x \in V(T)$ and every $k$-coloring $c_x$ of $X_x$.

%The procedure of the algorithm is exactly the same as one of {\sc max domino algorithm} but the value $\lambda(x,c_x)$ is computed by following the lemma below.


\begin{lemma}
\label{lem:computation-algo-dyn-lambda}
Let x be a node of $T$ and $c_x$ a $k$-coloring of $X_x$.
\begin{itemize}
\item[(a)] If $x$ is a join node with children $x_{1}$ and $x_{2}$, then $\lambda(x,c_{x}) = \min\limits_{i \in \{1,2\}} \left\lbrace \lambda(x_i,c_x) \right\rbrace$.
\item[(b)] If $x$ is an introduce node with child $y$, then $\lambda(x,c_{x}) = \lambda(y,c_{y})$, where $c_y$ is the $k$-coloring of $X_y$ such that $c_{y}(v) = c_{x}(v)$ for all $v \in X_y$.
\item[(c)] If $x$ is a forget node with child $y$, then
$$\lambda(x,c_x) = \max\limits_{c_y\in {\cal C}(c_x)} \min \bigg\{ \lambda(y, c_{y}) , \min\limits_{\substack{w \in X_x \\ vw \in E_{x}}} w^k((v, c_y(v))(w, c_{y}(w))  \bigg\}$$ where $\{v\}= X_y\setminus X_x$ and ${\cal C}(c_x)$ is the set of $k$-colorings of $X_y$ such that
$c_{y}(b) = c_{x}(b)$ for all $b \in X_x$.
\end{itemize}
\end{lemma}\label{lem:algo_threshold}
%	\lambda(t,(u_1,c_1),\dots,(u_q,c_q))  =  \max\limits_{c_0 \in [k]} \min \left\lbrace \lambda(t', (u_0,c_0),\dots,(u_q,c_q)), \min\limits_{j\in [q]} w^k((u_0,c_0)(u_j,c_j)) \right\rbrace.

\begin{proof}
Similarly as in Lemma \ref{lem:computation-algo-dyn}, (a) and (b) are easily obtained.

(c) For $x$ a forget node with child $y$, let $v = X_y \setminus X_x$.\\
\begin{align*}
\lambda(x,c_x) &= \max\limits_{\substack{c:V_x\to [k] \\ \forall b\in X_x, c(b)=c_x(b)}}\min\limits_{uv \in E_x} w^k((u,c(u))(v,c(v))) \\
	&= \max\limits_{\substack{c:V_x \to [k]\\ \forall b \in X_x, c(b) = c_x(b) }}  \min  \bigg\{ \min\limits_{uv \in E_y} w^k((u,c(u))(v,c(v))), \min\limits_{\substack{w\in X_x \\ vw \in E_x}} w^k((v,c_y(v))(w,c_y(w)))\bigg\} \\
  		&= \max\limits_{c_y\in \mathcal{C}(c_x)} \min  \bigg\{ \lambda(y, c_y),  \min\limits_{\substack{w \in X_x \\ vw \in E_{x}}} w^k((v, c_y(v))(w, c_{y}(w)) \bigg\}. \\
  	\end{align*} 

 \end{proof}


We are now able to prove Theorem~\ref{the:bounded-treewidth-lambda} (the proof is similar to the one of Theorem~\ref{the:bounded-treewidth}).

\begin{proof}[Proof of Theorem~\ref{the:bounded-treewidth-lambda}]
The algorithm is the following:
\begin{enumerate} 
\item Compute a nice tree decomposition ${\cal T}=(T, \mathcal{X})$ of $G$ with width at most $\tau$.
\item Compute the value of $\lambda(x,c_{x})$ for all node $x$ of $T$ and all $k$-coloring $c_x$ of $X_x$.
\item Return the maximum value of $\lambda(r, c_r)$ over all $k$-colorings $c_r$ of the bag $X_r$ of the root $r$.
\end{enumerate}

\medskip

At Step 2, each value $\lambda(x,c_{x})$ is computed using the formulas of Lemma~\ref{lem:computation-algo-dyn-lambda}.
If $x$ is a leaf, a join node or an introduce node, then the value of $\lambda(x,c_{x})$ is obtained in constant time by the formula.
If $x$ is a join forget node, then the algorithm takes the maximum over $k$ values because $|\mathcal{C}(c_x)|=k$ (there are $k$ possible colors for the vertex $v$ of $X_y\setminus X_x$).
The number of edge weights that are considered in the min function is upper-bounded by the size of $X_{x}$.
Hence the value can be computed in $O(\tau \cdot k)$ time.\\
Now for each node $x$, there are at most $k^{\tau+1}$ possible $k$-colorings of $X_x$, so there are at most
$ k^{\tau+1}\cdot |V(T)|$ values  $\lambda(x,c_{x})$, so at most $O(\tau \cdot k^{\tau+1} \cdot |V|)$ values by Lemma~\ref{lem:nice_tree}.
As each of these values is computed in $O(\tau \cdot k)$ time, Step 2 runs in
 $O(\tau^{2} \cdot k^{\tau+2} \cdot |V|)$  time.

Clearly Step 1 and  3 can be done faster than Step 2, so the total running time of our algorithm is $O(\tau^{2} \cdot k^{\tau+2} \cdot |V|)$.
\end{proof}




%Now, we will analysis the running time of the algorithm for computing value $\lambda(x,c_x)$  followed by Lemma above \ref{lem:algo_threshold}.
%If $t$ is a leaf, a join node or an introduce node, it takes a constant time. And, each bag $X_x$ contains at most $\tau+1$ vertices and each vertex has $k$ colors. Hence, the running time to compute all values $\lambda$ at a node $x$ is at most $O(k^{\tau+1})$. There are at most $\tau n$ such node $x$ by Lemma \ref{lem:nice_tree}, then the running time at all these nodes $t$ is $O(\tau nk^{\tau+1})$. \\
%If $x$ is a forget node with $v$ forgotten, for a fix coloring $c_x$, it takes $O(\tau)$ time to compute $\min\limits_{\substack{w \in X_x \\ vw \in E_{x}}} w^k((v, c_y(v))(w, c_{y}(w))$, then we need $O(\tau k)$ for $\mathcal{C}(c_x)$ . Since a forget node has at most $\tau$ vertices, then there are $k^{\tau}$ possible $k$-coloring of $X_x$, thus the running time for computing all $\lambda(x,c_x)$ is $O(\tau k^{\tau+1})$. Moreover, there are at most $n$ forget node since each vertex is forgotten once. Therefore, the running time at all forget nodes is $O(\tau nk^{\tau+1})$.

We expose several algorithms solving the problems.
As the decision problem \textsc{Max Cut} is a particular case of \textsc{Fulfill $2$-Coloring}, we investigate whether  results for \textsc{Max Cut}  (for decision or optimization problem) can be also generalized to our problem on two colors.
Hadlock~\cite{Had75} showed that  \textsc{Max Cut} is polynomial-time solvable for planar graphs. In contrast, we observe that \textsc{Fulfill $2$-Coloring} is $\NP$-complete (Remark~\ref{rem:NP}). Goemans and Williamson~\cite{GoWi95} proved that \textsc{Max Cut} is $0.87856$-approximable using semidefinite programming.
In Section~\ref{sec:approximation}, we use their approach to construct approximation algorithms for finding a coloring with maximum number of fulfilled edges (optimization version of \textsc{Fulfill $2$-Coloring}).

Next, in Section~\ref{sec:fpt}, we show a polynomial-time algorithm that solves the maximization version of \textsc{Domino} for graphs with bounded treewidth.
Similarly, we prove a polynomial-time algorithm that solves the problem (for bounded treewidth graphs) that consists in {\bf determining the maximum threshold $\lambda$} for which, given a graph $G$, integers $k, q$, a set of weight functions $w^k$, there is a $k$-coloring of $G$ that $\mathcal{ M}^{\lambda}$-fulfills at least $q$ edges.
}









