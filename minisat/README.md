Use minisat to find conflict coloring.

# Requirements

The following packages (available in ``yum``):
* ``minisat2`` for the command ``minisat``,
* ``minisat2-libs`` and ``minisat2-devel`` for the cpp minisat API (``#include "minisat/core/Solver.h"``, etc).

# Compile and run

There are three programs, each compiled with ``make name_of_program``:
* ``minisat`` with one argument ``filepath.gv``
* ``minisat-atmostone`` with one argument ``filepath.gv``
* ``minisat_output`` with three arguments ``filepath.gv`` ``filepath.output.txt`` and an integer ``1`` to find a coloring ``2`` to count the number of colorings

They:
1. bulid the ``ConflictGraph`` object from the ``.gv`` file using CGCL ``import_conflict_graph_graphviz``
2. create a ``.cnf`` file with the formula in DIMACS format (stored in ``/tmp``)
3. call minisat and measure running time:
  * ``minisat`` and ``minisat-atmostone`` use the command ``minisat`` with ``std::system`` call
  * ``minisat_output`` use the cpp minisat API to find a coloring or count the number of colorings

``minisat`` construct a formula with one clause per vertex (at least one color) and one clause per conflict edge (forbid the pair of colors).

``minisat-atmostone`` add clauses to have at most one color per vertex.

``minisat_output`` is used by ``python/doit.py`` and ``python/solver_output.cpp``, it prints the output to a specified txt file.
