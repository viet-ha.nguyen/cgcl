#include <iostream>
#include <string>
#include <sstream> // for std::stringstream

// CGCL
#include "../src/datastructure.hpp"
#include "../src/import.hpp"

// Timer
#include <boost/timer/timer.hpp>
#include <boost/chrono/chrono.hpp>

// Minisat API
//#include "minisat/utils/System.h"
//#include "minisat/utils/ParseUtils.h"
//#include "minisat/utils/Options.h"
#include "minisat/core/Dimacs.h"
#include "minisat/core/Solver.h"
#include "minisat/core/SolverTypes.h"
#include <zlib.h>

// Run bash command (std::system)
#include <stdlib.h>
#include <sys/wait.h> // WEXITSTATUS

using namespace boost;
using namespace Minisat; // TODO Minisat::l_True

// command line arguments
// * string the path of .gv file (input)
// * string the path of .txt file (output)
// * int 1=find 2=enum
int main(int argc, char *argv[]){

    // -------- read command line argument --------
    
    if(argc < 4){
        std::cout << "error: you need to provide a path to .gv file (input), a path to .txt file (output), and int 1=find 2=enum\n";
        return 0;
    }
    std::string filenamegv = argv[1];
    std::string filenameoutput = argv[2];
    int find1_enum2 = atoi(argv[3]);

    // -------- generate a ConflictGraph from a gv file --------

    // useful variables
    vertex_iter vi,vi_end;
    edge_iter ei,ei_end;
    int i;
    std::string clause;

    // use CGCL import
    ConflictGraph G;
    if (import_conflict_graph_graphviz(filenamegv,G)){
        std::cout << "ConflictGraph successfully generated\n";
    }
    else{
        std::cout << "error to generate G \n";
        return 0;
    }
    
    // -------- create the cnf file for G --------

    std::ofstream cnffile;
    cnffile.open("/tmp/conflictColoring.cnf");
    cnffile << "c conflict coloring " << filenamegv << "\n";
    // 1. compute the number of variables
    int num_var = num_vertices(G.conflict_g);
    // 2. compute the number of clauses
    int num_clauses = num_vertices(G.main_g) + num_edges(G.conflict_g);
    // 3. create a map from vertex descriptor in conflict graph to variable
    std::map<vertex_desc,int> map_vc2var;
    i = 1;
    for(tie(vi,vi_end)=vertices(G.conflict_g); vi != vi_end; ++vi){
        map_vc2var[*vi] = i++;
    }
    // 4. construct the cnf formula with one variable per (vertex,color)
    cnffile << "p cnf " << num_var << " " << num_clauses << "\n";
    // 4.a. create clauses enforcing at least one color per vertex
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        clause = "";
        for(i=0; i<G.main_g[*vi].num_colors; ++i){
            clause += std::to_string(map_vc2var[G.main_g[*vi].conflict_vertices[i]]) + " ";
        }
        clause += "0\n";
        cnffile << clause;
    }
    // 4.b. create clauses for conflict edges
    for(tie(ei,ei_end)=edges(G.conflict_g); ei != ei_end; ++ei){
        clause = "-" + std::to_string(map_vc2var[source(*ei,G.conflict_g)]) + " -" + std::to_string(map_vc2var[target(*ei,G.conflict_g)]) + " 0\n";
        cnffile << clause;
    }
    // 5. close file
    cnffile.close();

    // -------- Call minisat --------

    // variables for result
    bool bfind=false;
    int nenum=0;

    // Minisat Solver read cnf file
    Minisat::Solver S;
    gzFile cnffilez = gzopen("/tmp/conflictColoring.cnf", "rb");
    if(cnffilez == NULL){std::cout << "gzopzn error NULL file\n";}else{std::cout << "gzopen ok\n";}
    Minisat::parse_DIMACS(cnffilez, S);
    gzclose(cnffilez);

    boost::timer::cpu_timer rtime; // timer start
    // color solve cnf formula with minisat
    switch(find1_enum2){
        case 1: // find
            {
                bfind = S.solve();
                /*
                //std::string minisatcommand = "minisat -verb=1 /tmp/conflictColoring.cnf";
                //bfind = std::system(minisatcommand.c_str());
                */
            }
            break;
        case 2: // enum
            {
                Minisat::vec<Minisat::Lit> blocking_clause;
                nenum = 0;
                while(S.solve()){
                    // add the "negation" of the solution
                    blocking_clause.clear();
                    for (int v = 0; v < S.nVars(); v++){
                        blocking_clause.push(Minisat::mkLit(v, S.modelValue(v) == l_True));
                    }
                    S.addClause(blocking_clause);
                    nenum++;
                }
                /*
                // TODO : try to use the "minisat API"
                nenum = 0;
                std::string minisatcommand = "minisat -verb=1 /tmp/conflictColoring.cnf /tmp/conflictColoring.cnf.output";
                // minisat procedure to count the number of solutions:
                // 1. call minisat to get a solution
                // 2. add the negation of the solution to the formula
                // 3. loop
                // call minisat (init)
                //bfind = std::system(minisatcommand.c_str());
                while(WEXITSTATUS(bfind)==10){
                    nenum++;
                    // add the negation of the solution, if any
                    std::ofstream cnfoutputfile;
                    cnfoutputfile.open("/tmp/conflictColoring.cnf.output");
                    std::ofstream cnffile;
                    cnffile.open("/tmp/conflictColoring.cnf", std::ofstream::app);
                    // TODO
                    // TODO
                    cnfoutputfile.close();
                    cnffile.close();
                    // call minisat
                    bfind = std::system(minisatcommand.c_str());
                }
                */
            }
            break;
    }
    rtime.stop(); // timer stop

    // -------- Output --------

    // open file
    std::ofstream ofs;
    ofs.open(filenameoutput, std::ofstream::app);

    // compute time in seconds
    typedef boost::chrono::duration<double> sec;
    sec seconds = boost::chrono::nanoseconds(rtime.elapsed().user);

    // print result
    switch(find1_enum2){
        case 1: // find
            // algo name and time
            ofs << "MINISAT_running_time :" << seconds.count() << "\n"; // time in seconds
            // coloring found
            if(bfind){//if(WEXITSTATUS(bfind)==10){
                // exit code 10 : satisfiable
                ofs << "MINISAT_coloring_found: 1\n";
            }
            else{//if(WEXITSTATUS(bfind)==20){
                // exit code 20 : satisfiable
                ofs << "MINISAT_coloring_found: 0\n";
            }
            break;
        case 2: // enum
            // algo name and time
            ofs << "MINISAT_ENUM_running_time: " << seconds.count() << "\n"; // time in seconds
            ofs << "MINISAT_ENUM_coloring_count: " << nenum << "\n";
            break;
    }
    // empty line
    ofs << "\n";

    // close
    ofs.close();

    // done
    return 0;
}