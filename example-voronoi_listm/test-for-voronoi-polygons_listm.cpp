#include "../example-voronoi/Voronoi_based_conflict_graph_builder.hpp"

// main
// command line arguments: see --help

int main(int argc, char* argv[]){

//--------step 0: parse command line arguments-------------------
std::cout << "# parse command line arguments" << std::endl;

// Declare the supported options
po::options_description desc("Allowed options");
desc.add_options()
  ("help", "produce help message")
  //("N_wanted", po::value<int>() , "number of main vertices")
  ("num_c", po::value<int>() , "number of conformation for each DT::point")
  ("radius", po::value<double>() , "radius of the random VD points")
  //("min_dist_threshold", po::value<double>() , "minimum distance between two VD points")
  //("threshold", po::value<double>() , "threshold for the intersection area of two polygons in conflict")
  ("cin", po::value<std::string>() , "filepath to the .cin file containing 2D DT points")
  ("gv_min", po::value<std::vector<std::string>>()->multitoken() , "filepath to the .gv files for min")
  ("gv_mean", po::value<std::vector<std::string>>()->multitoken() , "filepath to the .gv files for mean")
  ("output_min", po::value<std::vector<std::string>>()->multitoken() , "filepath to the .yaml files for min")
  ("output_mean", po::value<std::vector<std::string>>()->multitoken() , "filepath to the .yaml files for mean")
  ("polytikz", po::value<std::string>() , "filepath to a .tex file to draw polygons in tikz (optional)")
  ("min_intersections", po::value<double>() , "step to plot min intersections (optional)")
  ("multis_min", po::value<std::vector<double>>()->multitoken() , "list of multi for min")
  ("multis_mean", po::value<std::vector<double>>()->multitoken() , "list of multi for mean");
  

po::variables_map vm;
po::store(po::parse_command_line(argc, argv, desc), vm);
po::notify(vm);

// help
if(vm.count("help")) {
  std::cout << desc << "\n";
  return 0;
}

// N_wanted
/*int N_wanted=-1;
if (vm.count("N_wanted")) {
  N_wanted = vm["N_wanted"].as<int>();
  std::cout << "N_wanted=" << N_wanted << std::endl;
}*/

// num_c
if (vm.count("num_c")) {
  T_Voronoi_based_conflict_graph_builder<void>::num_c = vm["num_c"].as<int>();
  std::cout << "num_c=" <<   T_Voronoi_based_conflict_graph_builder<void>::num_c << std::endl;
} else {
  T_Voronoi_based_conflict_graph_builder<void>::num_c = 2;
  std::cout << "num_c=" << T_Voronoi_based_conflict_graph_builder<void>::num_c << " (default)" << std::endl;
}

// radius
if (vm.count("radius")) {
  T_Voronoi_based_conflict_graph_builder<void>::radius = vm["radius"].as<double>();
  std::cout << "radius=" << T_Voronoi_based_conflict_graph_builder<void>::radius << std::endl;
} else {
  T_Voronoi_based_conflict_graph_builder<void>::radius = 1;
  std::cout << "radius=" << T_Voronoi_based_conflict_graph_builder<void>::radius << " (default)" << std::endl;
}

// cin
std::string filenamecin;
if (vm.count("cin")) {
  filenamecin = vm["cin"].as<std::string>();
  std::cout << "filenamecin=" << filenamecin << std::endl;
} else {
  filenamecin = "data/mydata_Point_2.cin";
  std::cout << "filenamecin=" << filenamecin << " (default)" << std::endl;
}

// gv
std::vector<std::string> filenamegv_min;
if (vm.count("gv_min")) {
  filenamegv_min = vm["gv_min"].as<std::vector<std::string>>();
  //std::cout << "filenamegv_min=" << filenamegv_min << std::endl;
} else {
  std::cout << "ec" << std::endl;
}
std::vector<std::string> filenamegv_mean;
if (vm.count("gv_mean")) {
  filenamegv_mean = vm["gv_mean"].as<std::vector<std::string>>();
  //std::cout << "filenamegv_mean=" << filenamegv_mean << std::endl;
} else {
  std::cout << "ec" << std::endl;
}

// output
std::vector<std::string> filenameoutput_min;
if (vm.count("output_min")) {
  filenameoutput_min = vm["output_min"].as<std::vector<std::string>>();
  //std::cout << "filenameoutput_min=" << filenameoutput_min << std::endl;
} else {
  std::cout << "ec" << std::endl;
}
std::vector<std::string> filenameoutput_mean;
if (vm.count("output_mean")) {
  filenameoutput_mean = vm["output_mean"].as<std::vector<std::string>>();
  //std::cout << "filenameoutput_mean=" << filenameoutput_mean << std::endl;
} else {
  std::cout << "ec" << std::endl;
}

// multis
std::vector<double> multis_min;
if (vm.count("multis_min")) {
  multis_min = vm["multis_min"].as<std::vector<double>>();
  //std::cout << "multis_min=" << multis_min << std::endl;
} else {
  std::cout << "ec" << std::endl;
}
std::vector<double> multis_mean;
if (vm.count("multis_mean")) {
  multis_mean = vm["multis_mean"].as<std::vector<double>>();
  //std::cout << "multis_mean=" << multis_mean << std::endl;
} else {
  std::cout << "ec" << std::endl;
}

//--------construct the Delaunay triangulation-------------------
std::cout << "## compute the Delauney triangulation from data" << std::endl;
std::ifstream ifs(filenamecin);
assert(ifs);
std::istream_iterator<DT::Point> begin(ifs);
std::istream_iterator<DT::Point> end;
T_Voronoi_based_conflict_graph_builder<void>::dt.insert(begin, end);
ifs.close();
// draw the DT
//CGAL::draw(T_Voronoi_based_conflict_graph_builder<void>::dt);

//--------step1: Conflict Graph construction starts here-------------------

//-------- init --------
T_Voronoi_based_conflict_graph_builder<void> vor_cg_builder;

//-------- build main graph --------
std::cout << "# build the main graph from the Delauney triangulation" << std::endl;
vor_cg_builder.build_main_graph();

// NEW
// check the number of main vertices

if(num_vertices(vor_cg_builder.g)!=8){ // CAUTION!!
  // STOP
  // write in yaml
  std::ofstream ofs;
  for(int i=0; i<filenameoutput_min.size(); ++i){
    ofs.open(filenameoutput_min[i], std::ofstream::app);
    ofs << "number_of_main_vertices: " << boost::num_vertices(vor_cg_builder.g) << "\n";
    //ofs << "num_c: " << T_Voronoi_based_conflict_graph_builder<void>::num_c << "\n";
    //ofs << "radius: " << T_Voronoi_based_conflict_graph_builder<void>::radius << "\n";
    //ofs << "multi: " << multis_min[i] << "\n";
    ofs.close();
  }
  for(int i=0; i<filenameoutput_mean.size(); ++i){
    ofs.open(filenameoutput_mean[i], std::ofstream::app);
    ofs << "number_of_main_vertices: " << boost::num_vertices(vor_cg_builder.g) << "\n";
    ofs.close();
  }
  return 0;
}

//-------- timer start --------
boost::timer::cpu_timer rtime;

//-------- build conformations for every vertex --------
std::cout << "# compute conformations (random polygons)" << std::endl;
// with version 1 (ncolor)
vor_cg_builder.build_random_polygon_ncolor();

//-------- timer pause --------
rtime.stop();

//-------- (optional) compute min intersections --------
std::array<double, 2> set_threshold;
// std::array<std::string,2> inter;
// inter.at(0) = "mininter";
// inter.at(1) = "meaniter"; 
double multi = -1;

// NB: --min_intersections should always be 1 (=step) for this program

// parse --min_intersections
if (vm.count("min_intersections")) {
  double step = vm["min_intersections"].as<double>();
  vor_cg_builder.min_intersections(step, filenameoutput_min[0], set_threshold);
  // copy min_intersections infos to all other output files
  // source: https://stackoverflow.com/a/46536544
  std::ifstream src;
  std::ofstream dst;
  for(int i=1; i<filenameoutput_min.size(); ++i){
    src.open(filenameoutput_min[0], std::ios::in | std::ios::binary);
    dst.open(filenameoutput_min[i], std::ios::out | std::ios::binary);
    dst << src.rdbuf();
    src.close();
    dst.close();
  }
  for(int i=0; i<filenameoutput_mean.size(); ++i){
    src.open(filenameoutput_min[0], std::ios::in | std::ios::binary);
    dst.open(filenameoutput_mean[i], std::ios::out | std::ios::binary);
    dst << src.rdbuf();
    src.close();
    dst.close();
  }

  // NEW
  // check set_threshold for min is <= 100
  if(set_threshold.at(0)>100){ // CAUTION!!
    // STOP
    // write in yaml
    std::ofstream ofs;
    for(int i=0; i<filenameoutput_min.size(); ++i){
      ofs.open(filenameoutput_min[i], std::ofstream::app);
      ofs << "number_of_main_vertices: " << boost::num_vertices(vor_cg_builder.g) << "\n";
      ofs.close();
    }
    for(int i=0; i<filenameoutput_mean.size(); ++i){
      ofs.open(filenameoutput_mean[i], std::ofstream::app);
      ofs << "number_of_main_vertices: " << boost::num_vertices(vor_cg_builder.g) << "\n";
      ofs.close();
    }
    return 0;
  }
}


//
// THE NEXT PART OF THE CODE IS SPLITTED IN TWO PARTS (if/else):
// IF: BUILD ONE GV
// ELSE: BUIDL TWO GV (minormean=3)
//

// the ConflictGraph
ConflictGraph cg;

{

  // prepare variables to make a for loop (min and mean)
  std::array<double,2> mm_threshold;
  mm_threshold[0] = set_threshold.at(0); // min threshold
  mm_threshold[1] = set_threshold.at(1); // mean threshold
  std::array<std::vector<std::string>,2> mm_gv;
  mm_gv[0] = filenamegv_min; // min gv filepath
  mm_gv[1] = filenamegv_mean; // mean gv filepath
  std::array<std::vector<std::string>,2> mm_output;
  mm_output[0] = filenameoutput_min; // min yaml filepath
  mm_output[1] = filenameoutput_mean; // mean yaml filepath
  std::array<std::vector<double>,2> mm_multis;
  mm_multis[0] = multis_min; // min multis
  mm_multis[1] = multis_mean; // mean multis

  // NB: filenameoutput_min and filenamegv_min should be at least as long as multis_min
  //     (and the same for mean)
  //     NOT CHECKED!!

  // loop on min (0) and mean (1)
  for(int mm=0; mm<2; mm++){

    // NEW
    // loop on multis
    for(int i=0; i<mm_multis[mm].size(); ++i){

      //-------- set threshold and do things --------
      Conflict_checker_for_pairs_of_voronoi_polygons::threshold = mm_multis[mm][i]*mm_threshold[mm];
      std::cout << "# (mm=" << mm << " and multi=" << mm_multis[mm][i] << ") the computed threshold is threshold=" << Conflict_checker_for_pairs_of_voronoi_polygons::threshold << std::endl;

      // build cg and export infos
      //-------- timer resume --------
      rtime.resume();

      //-------- build conflict graph --------
      std::cout << "# (mm=" << mm << ") compute the conflict edges" << std::endl;
      cg.clear();
      vor_cg_builder.build_conflict_graph(cg);

      //-------- timer stop --------
      rtime.stop();

      //--------step2: export the ConflictGraph--------
      std::cout << "## (mm=" << mm << ") export ConflictGraph to " << mm_gv[mm][i] << " (dot -Tpdf mygraph.gv -o mygraph.pdf)" << std::endl;
      export_conflict_graph_graphviz(cg, mm_gv[mm][i]);

      //--------step3: information of Conflict Graph -------------------
      std::cout << "## (mm=" << mm << ") ConflictGraph infos in " << mm_output[mm][i] << std::endl;
      std::cout << "## caution: running for mean is wrong (includes the running time for min)" << std::endl;

      // open file
      std::ofstream ofs;
      ofs.open(mm_output[mm][i], std::ofstream::app);

      // write running time
      // "user" see https://theboostcpplibraries.com/boost.timer 
      // "seconds" see https://stackoverflow.com/a/17320036
      typedef boost::chrono::duration<double> sec;
      sec seconds = boost::chrono::nanoseconds(rtime.elapsed().user);
      ofs << "running_time_build_cg: " << seconds.count() << "\n"; // time in seconds

      // write parameters
      //ofs << "nshift: " << T_Voronoi_based_conflict_graph_builder<void>::nshift << "\n";
      ofs << "num_c: " << T_Voronoi_based_conflict_graph_builder<void>::num_c << "\n";
      ofs << "radius: " << T_Voronoi_based_conflict_graph_builder<void>::radius << "\n";
      ofs << "multi: " << mm_multis[mm][i] << "\n";
      //ofs << "min_dist_threshold: " << T_Voronoi_based_conflict_graph_builder<void>::min_dist_threshold << "\n";
      ofs << "threshold: " << Conflict_checker_for_pairs_of_voronoi_polygons::threshold << "\n";
      //ofs << inter.at(i) << ": " << set_threshold.at(i) << "\n";

      // write basic info to output file
      ofs << "number_of_main_vertices: " << boost::num_vertices(cg.main_g) << "\n";
      ofs << "number_of_main_edges: " << boost::num_edges(cg.main_g) << "\n";
      ofs << "average_number_of_color_per_vertices: " << ( boost::num_vertices(cg.main_g)==0 ? 0 : (double)boost::num_vertices(cg.conflict_g) / (double)boost::num_vertices(cg.main_g) ) << "\n";
      ofs << "average_number_of_conflict_edges_per_edge: " << ( boost::num_edges(cg.main_g)==0 ? 0 : (double)boost::num_edges(cg.conflict_g) / (double)boost::num_edges(cg.main_g) ) << "\n";

      // empty line
      ofs << "\n";

      // close
      ofs.close();
    }
  }
}

//-------- (optional) export polygons to tikz -------------------

// parse --polytikz string
if (vm.count("polytikz")) {
  std::string filenametikz = vm["polytikz"].as<std::string>();
  std::cout << "## export polygons in tikz to filenametikz=" << filenametikz << std::endl;
  vor_cg_builder.export_all_polygons_tikz(cg, filenametikz);
}

// done
return 0;
}
