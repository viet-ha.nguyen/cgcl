cmake_minimum_required(VERSION 3.1...3.15)

find_package(CGAL REQUIRED OPTIONAL_COMPONENTS Qt5)

if(CGAL_Qt5_FOUND)
  add_definitions(-DCGAL_USE_BASIC_VIEWER -DQT_NO_KEYWORDS)
else()
  message(STATUS "Qt5 NOT found")
endif()

add_executable(test-for-voronoi-polygons
  test-for-voronoi-polygons_listm.cpp
  ../src/import.cpp
  ../src/coloring.cpp
)

if(CGAL_Qt5_FOUND)
  target_link_libraries(test-for-voronoi-polygons PUBLIC CGAL::CGAL_Qt5)
else()
  target_link_libraries(test-for-voronoi-polygons PUBLIC CGAL)
endif()

find_package(Boost COMPONENTS program_options timer REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})
target_link_libraries(test-for-voronoi-polygons PUBLIC ${Boost_LIBRARIES})
