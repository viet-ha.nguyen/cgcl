// choco imports
import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.Solver;

// file imports
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.HashMap;

// big integers
//import java.math.BigInteger;

// using Choco to solve conflict graph coloring

public class choco_output {

    // command line argument:
    // * string, filepath to the data .gv file containing a conflictgraph
    // * string, filepath to the .txt file to write informations about the execution of the algorithm 
    // * int, 1=find 2=enum
    public static void main(String[] args) {

        // -------- read argument from the command line --------

        if(args.length<3){
            System.out.println("error: you need to provide a .gv file for input, a .txt file for output, and 1=find or 2=enum");
            return;
        }
        String filenamegv = args[0];
        String filenameoutput = args[1];
        int find1_enum2 = Integer.parseInt(args[2]);

        // -------- build choco model from conflict graph in filenamegv --------

        // useful variables
        String line;
        String vertex;
        int l=0;
        int num_c=0;

        // choco model
        Model model = new Model("coloring");
        HashMap<String, IntVar> vertices_intvar = new HashMap<String, IntVar>();
        HashMap<String, Tuples> tuples = new HashMap<String, Tuples>();
        String edge_key;
        
        // try to open file
        try {  
            File myFile = new File(filenamegv);
            Scanner myReader = new Scanner(myFile);
            // patterns and matchers for vertices and edges
            Pattern vertex_p = Pattern.compile("v(\\d+)\\[label=.*\\];");
            Matcher vertex_m;
            Pattern color_p = Pattern.compile("<c\\d+>");
            Matcher color_m;
            Pattern edge_p = Pattern.compile("v(\\d+):c(\\d+)--v(\\d+):c(\\d+);");
            Matcher edge_m;
            while (myReader.hasNextLine()) {
                line = myReader.nextLine();
                // parse vertices
                vertex_m = vertex_p.matcher(line);
                if(vertex_m.find()){
                    //System.out.println("vertex: "+vertex_m.group(1));
                    // get number of colors
                    color_m = color_p.matcher(line);
                    num_c = 0;
                    while (color_m.find()){
                        num_c++;
                    }
                    //System.out.println(num_c);
                    //num_c = color_m.results().count(); // for Java 9+
                    // add a variable to the model
                    vertices_intvar.put(vertex_m.group(1),model.intVar(vertex_m.group(1), 0, num_c-1));
                }
                // parse edges
                edge_m = edge_p.matcher(line);
                if(edge_m.find()){
                    //System.out.println("edge: "+edge_m.group(1)+edge_m.group(2)+edge_m.group(3)+edge_m.group(4));
                    // prepare tuples for the constraints
                    edge_key = edge_m.group(1)+"-"+edge_m.group(3);
                    if(!tuples.containsKey(edge_key)){
                        tuples.put(edge_key,new Tuples());
                    }
                    tuples.get(edge_key).add(Integer.parseInt(edge_m.group(2)),Integer.parseInt(edge_m.group(4)));
                }
            }
            // add constraints to the model
            Pattern edge_key_p = Pattern.compile("(\\d+)-(\\d+)");
            Matcher edge_key_m;
            IntVar v1,v2;
            for (String e_key : tuples.keySet()) {
                edge_key_m = edge_key_p.matcher(e_key);
                if(edge_key_m.find()){
                    v1 = vertices_intvar.get(edge_key_m.group(1));
                    v2 = vertices_intvar.get(edge_key_m.group(2));
                    model.not(model.table(v1,v2,tuples.get(e_key))).post();
                }
            }
            
            myReader.close();
        } 
        // catch the exception
        catch(FileNotFoundException e) {
            e.printStackTrace();   
        }

        // -------- call choco solver --------

        // variables for result
        boolean bfind=false;
        //BigInteger nenum = new BigInteger("0");
        long nenum=0;

        // measure time: begin
        long tStart = System.currentTimeMillis();
	    // call choco to find or enum
        switch(find1_enum2){
            case 1:
                // find
                bfind = model.getSolver().solve();
                break;
            case 2:
                // enum
                Solver solver = model.getSolver();
                while(solver.solve()){
                    //nenum = nenum.add(BigInteger.ONE);
                    nenum=nenum+1;
                }
                break;
        }
        // measure time: end
        long tStop = System.currentTimeMillis();
        double t = (tStop-tStart)*1.00/1000; // time in seconds

        // -------- output --------
        
        // try to open file
        try {
            File fileo = new File(filenameoutput);
            FileWriter fileow = new FileWriter(fileo, true); //true=append
            // print
            switch(find1_enum2){
                case 1:
                    // find
                    fileow.write("CHOCO_running_time: "+t+"\n");
                    if(bfind){
                        fileow.write("CHOCO_coloring_result: 1\n\n");
                    }
                    else{
                        fileow.write("CHOCO_coloring_result: 0\n\n");
                    }
                    break;
                case 2:
                    // enum
                    fileow.write("CHOCO_ENUM_running_time: "+t+"\n");
                    fileow.write("CHOCO_ENUM_coloring_result: "+nenum+"\n\n");
                    break;
            }
            // close file
            fileow.close();
        }
        // catch the exception
        catch(Exception e) {
            e.printStackTrace();   
        }
    }
}
