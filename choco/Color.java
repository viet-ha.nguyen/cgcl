import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.constraints.extension.Tuples;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.HashMap;

// using Choco to solve conflict graph coloring

public class Color  {

    // command line argument:
    // * path to .gv file
    public static void main(String[] args) {
        // read argument from the command line
        if(args.length==0){
            System.out.println("error: you need to provide a path to .gv file");
            return;
        }
        String filepath = args[0];

        // useful variables
        String line;
        String vertex;
        int l=0;
        int num_c=0;

        // choco model
        Model model = new Model("coloring");
        HashMap<String, IntVar> vertices_intvar = new HashMap<String, IntVar>();
        HashMap<String, Tuples> tuples = new HashMap<String, Tuples>();
        String edge_key;
        
        // try to open file
        try {  
            File myFile = new File(filepath);
            Scanner myReader = new Scanner(myFile);
            // patterns and matchers for vertices and edges
            Pattern vertex_p = Pattern.compile("v(\\d+)\\[label=.*\\];");
            Matcher vertex_m;
            Pattern color_p = Pattern.compile("<c\\d+>");
            Matcher color_m;
            Pattern edge_p = Pattern.compile("v(\\d+):c(\\d+)--v(\\d+):c(\\d+);");
            Matcher edge_m;
            while (myReader.hasNextLine()) {
                line = myReader.nextLine();
                // parse vertices
                vertex_m = vertex_p.matcher(line);
                if(vertex_m.find()){
                    //System.out.println("vertex: "+vertex_m.group(1));
                    // get number of colors
                    color_m = color_p.matcher(line);
                    num_c = 0;
                    while (color_m.find()){
                        num_c++;
                    }
                    //System.out.println(num_c);
                    //num_c = color_m.results().count(); // for Java 9+
                    // add a variable to the model
                    vertices_intvar.put(vertex_m.group(1),model.intVar(vertex_m.group(1), 0, num_c-1));
                }
                // parse edges
                edge_m = edge_p.matcher(line);
                if(edge_m.find()){
                    //System.out.println("edge: "+edge_m.group(1)+edge_m.group(2)+edge_m.group(3)+edge_m.group(4));
                    // prepare tuples for the constraints
                    edge_key = edge_m.group(1)+"-"+edge_m.group(3);
                    if(!tuples.containsKey(edge_key)){
                        tuples.put(edge_key,new Tuples());
                    }
                    tuples.get(edge_key).add(Integer.parseInt(edge_m.group(2)),Integer.parseInt(edge_m.group(4)));
                }
            }
            // add constraints to the model
            Pattern edge_key_p = Pattern.compile("(\\d+)-(\\d+)");
            Matcher edge_key_m;
            IntVar v1,v2;
            for (String e_key : tuples.keySet()) {
                edge_key_m = edge_key_p.matcher(e_key);
                if(edge_key_m.find()){
                    v1 = vertices_intvar.get(edge_key_m.group(1));
                    v2 = vertices_intvar.get(edge_key_m.group(2));
                    model.not(model.table(v1,v2,tuples.get(e_key))).post();
                }
            }
            
            myReader.close();
        } 
        // catch the exception
        catch(FileNotFoundException e) {
            e.printStackTrace();   
        }

        // Solve the problem
        long startTime = System.currentTimeMillis();

        if(model.getSolver().solve()){
            System.out.println("Choco found a solution :");
            // Print the solution
            /*
            for (IntVar v : vertices_intvar.values()){
                System.out.println(v);
            }
            */
        }
        else{
            System.out.println("Choco has not found a solution :-(");
        }
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Time taken: "+(double)totalTime*1.00/1000+" s");

        //------------------------------END---------------------------------------------
        /*
        // Variables: one variable per vertex, ranging in the colors of that vertex
        IntVar v1 = model.intVar("v1", 0, 2);
        IntVar v2 = model.intVar("v2", 0, 1);
        IntVar v3 = model.intVar("v3", 0, 2);
        // Constraints using table
        // TODO: try all the algorithms
        //       https://javadoc.io/static/org.choco-solver/choco-solver/4.0.1/org/chocosolver/solver/constraints/IIntConstraintFactory.html#table-org.chocosolver.solver.variables.IntVar-org.chocosolver.solver.variables.IntVar-org.chocosolver.solver.constraints.extension.Tuples-
        //       https://javadoc.io/static/org.choco-solver/choco-solver/4.0.1/org/chocosolver/solver/constraints/IIntConstraintFactory.html#table-org.chocosolver.solver.variables.IntVar:A-org.chocosolver.solver.constraints.extension.Tuples-java.lang.String-
        Tuples tuples;
        tuples = new Tuples();
        tuples.add(0,0);
        tuples.add(0,1);
        tuples.add(1,1);
        model.not(model.table(v1,v2,tuples)).post();
        tuples = new Tuples();
        tuples.add(0,0);
        tuples.add(1,0);
        tuples.add(1,1);
        tuples.add(2,2);
        model.not(model.table(v1,v3,tuples)).post();
        tuples = new Tuples();
        tuples.add(0,2);
        tuples.add(1,1);
        model.not(model.table(v2,v3,tuples)).post();
        // Solve the problem
        model.getSolver().solve();
        // Print the solution
        System.out.println(v1);
        System.out.println(v2);
        System.out.println(v3);
        */
    }
}
