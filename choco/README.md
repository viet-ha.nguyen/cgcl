Use Choco solver to find conflict coloring. Langage: ``.java``.

# Requirement

Nothing, Choco is included in the ``.jar``.

# Compile

There are three programs, each compiled with ``make name_of_program``:
* ``Color`` with one argument ``filepath.gv``
* ``Color_1bit`` with one argument ``filepath.gv``
* ``choco_output`` with three arguments ``filepath.gv`` ``filepath.output.txt`` and an integer ``1`` to find a coloring ``2`` to count the number of colorings

# Run

Each program has a ``run_name_of_program.sh`` script to run it (it calls ``java`` and includes Choco ``.jar`` as ``-classpath``).

They:
1. parse the ``.gv`` file to construct a Choco ``Model``
2. call Choco solver on the model (with ``model.getSolver().solve()``) and measure running time (with ``System.currentTimeMillis()``)

``Color_1bit`` is used by ``time_analysis/``.

``choco_output`` is used by ``python/doit.py`` and ``python/solver_output.cpp``, it prints the output to a specified txt file.
