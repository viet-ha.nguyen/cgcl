Conflict Graph Coloring Library (CGCL), in C++.
* ``doc/`` contains the documentation in LaTeX
* ``src/`` contains the source code for conflict coloring
* ``examples/`` contains examples of usage of CGCL
* ``choco/`` contains a modelization of conflict coloring to be solved by the CSP solver Choco (Java)
* ``minisat/`` contains a modelization of conflict coloring to be solved by the SAT solver Minisat
* ``time_analysis/`` runs experiments and compute average running times (plot+table)

