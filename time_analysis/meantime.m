function mean_t = meantime(X_t,ninstances,timeout)
% X_t a matrix of time values
% ninstances the number of instances of each parameter
% timeout the timeout
% Compute the mean for each group of ninstances values (in a row)

[r c]=size(X_t);
n=c/ninstances;
mean_t = zeros(r,n); % init mean_t = 0 everywhere
for i = 1:r
  for j = (ninstances:ninstances:size(X_t,2))
    t = X_t(i,(j-ninstances+1):j); % vector of ninstances running times of the same graph type
    if any(t==-1)
      mean_t(i,j/ninstances) = timeout; % if exist timeout, set mean time to timeout
    else
      mean_t(i,j/ninstances) = sum(t,2)/ninstances; % no timeout, give the mean time
    endif
  endfor
endfor
endfunction