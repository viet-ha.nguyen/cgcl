
%% Initialization
clear ; close all; clc
%% Load shared functions
addpath(".:..")

%%==================== Useful variables ================
name_xp = "xp-10-0.5-c-0.3"; % printed on graphics
pname = "number of colors"; % name of the parameter axis
p = []; % vector of values in parameter
for i = 10:5:50 % from 10 to 50, increasing by 5
  p = [p i];
endfor
ninstances = 10; % number of instances for each parameter value
timeout = 30;
pathfile = 'data.csv';

%% ========== Load Data =================
X = load(pathfile);
% take the number of rows (algos) and columns (examples)
[r c] = size(X);

%% =========== Parse the data =====================
% X_t contains running time (= even columns of X)
X_t = X(:,2:2:end);
% X_b constains boolean outputs (= odd columns of X)
X_b = X(:,1:2:end);
% the number of groups of ninstances examples
n = size(p,2);

%% =========== Check if algos give the same results ====================

fprintf('check if pos/neg results are coherent...\n')
if coherence(X_b)
  fprintf ('ok\n')
else
  fprintf ('ko (not the same results at column %d)\n', i)
  return;
endif
pause;
fprintf('\n')

%% ==================== Part 2: Compute average time ====================
% compute the average time for each 10 examples: save in the matrix mean_t of size n
% in case of timeout (-1), set the mean to timeout

fprintf('compute the means of runs...\n')
mean_t = meantime(X_t, ninstances, timeout)

%%============== Part 3: posneg percent computation ===============

posneg = posnegpercent(X_b,ninstances);

%%============== Part 4: Table ==================
printTable(mean_t, posneg, pname, p, timeout, name_xp);

%%============== Part 5: Plotting ==================
plotData(mean_t, pname, p, timeout, name_xp);
