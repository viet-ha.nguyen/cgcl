#include <iostream>
#include <string>
#include <csignal> 
#include "time.h" // execution time
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <array>

// execute a system command and get the result as a string
// source: https://stackoverflow.com/a/478960
std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

// * call my choco modelization and conflictColoring program (it parses the .gv file)
// * print:
//   * if coloring terminates: "b,t,"
//     with b=1 if there is a conflict coloring, b=0 otherwise, and t=the running time in seconds 
//   * if SIGINT received: "-1,-1," 

void signal_timeout(int signal_num) {
    // timeout 
    std::cout << "-1,-1," << std::flush; 
    exit(0);
} 

//  arguments:
// * path to a .gv file
int main(int argc, char *argv[]){
    // add signal handler
    signal(SIGINT, signal_timeout); 

    // read command line argument
    if(argc < 2){
        std::cout << "error: you need to provide a path to .gv file\n";
        return 0;
    }
    std::string pathFile = argv[1];

    // call algorithm
    std::string chococommand = "cd ../choco ; ./run_1bit.sh ../time_analysis/"+pathFile; 
    std::string res = exec(chococommand.c_str());

    // choco_1bit already formats the output
    // (to avoid counting the time for parsing the input)
    std::cout << res << std::flush;
    
    // done
    return 0;
}