#include <iostream>
#include <string>
#include <sstream> // for std::stringstream
#include "../src/datastructure.hpp"
#include "../src/import.hpp"
#include "time.h" // execution time
#include <sys/wait.h> // WEXITSTATUS

using namespace boost;

// * parse the .gv file
// * call conflictColoring_cut_set_cycle algorithm
// * print:
//   * if coloring terminates: "b,t,"
//     with b=1 if there is a conflict coloring, b=0 otherwise, and t=the running time in seconds 
//   * if SIGINT received: "-1,-1," 

void signal_timeout(int signal_num) {
    // timeout 
    std::cout << "-1,-1," << std::flush; 
    exit(0);
}

// command line arguments
// * the path file of .gv file
int main(int argc, char *argv[]){
    // add signal handler
    signal(SIGINT, signal_timeout);
    
    // read command line argument
    if(argc < 2){
        std::cout << "error: you need to provide a path to .gv file\n";
        return 0;
    }
    std::string pathFile = argv[1];

    // useful variables
    vertex_iter vi,vi_end;
    edge_iter ei,ei_end;
    int i;
    std::string clause;
    clock_t tStart;
    double t;

    // generate a ConflictGraph from a gv file
    ConflictGraph G;
    if (!import_conflict_graph_graphviz(pathFile,G)){
        std::cout << "error to generate G \n";
        return 0;
    }
    
    // create the cnf file for G
    std::ofstream cnffile;
    cnffile.open("/tmp/conflictColoring.cnf");
    cnffile << "c conflict coloring " << pathFile << "\n";
    // 1. compute the number of variables
    int num_var = num_vertices(G.conflict_g);
    // 2. compute the number of clauses
    int num_clauses = num_vertices(G.main_g) + num_edges(G.conflict_g);
    // 3. create a map from vertex descriptor in conflict graph to variable
    std::map<vertex_desc,int> map_vc2var;
    i = 1;
    for(tie(vi,vi_end)=vertices(G.conflict_g); vi != vi_end; ++vi){
        map_vc2var[*vi] = i++;
    }
    // 4. construct the cnf formula with one variable per (vertex,color)
    cnffile << "p cnf " << num_var << " " << num_clauses << "\n";
    // 4.a. create clauses enforcing at least one color per vertex
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        clause = "";
        for(i=0; i<G.main_g[*vi].num_colors; ++i){
            clause += std::to_string(map_vc2var[G.main_g[*vi].conflict_vertices[i]]) + " ";
        }
        clause += "0\n";
        cnffile << clause;
    }
    // 4.b. create clauses for conflict edges
    for(tie(ei,ei_end)=edges(G.conflict_g); ei != ei_end; ++ei){
        clause = "-" + std::to_string(map_vc2var[source(*ei,G.conflict_g)]) + " -" + std::to_string(map_vc2var[target(*ei,G.conflict_g)]) + " 0\n";
        cnffile << clause;
    }
    // 5. close file
    cnffile.close();

    // measure time: begin
    tStart = clock();
    // color solve cnf formula with minisat
    std::string minisatcommand = "minisat -verb=0 /tmp/conflictColoring.cnf >> /dev/null 2>> /dev/null";
    int ret = system(minisatcommand.c_str());
    // measure time: end
    t = (double)(clock() - tStart)/CLOCKS_PER_SEC;
    
    // print 
    if(WEXITSTATUS(ret)==10){
        // exit code 10 : satisfiable
        std::cout << "1," << t << "," << std::flush;
        return 0;
    }
    if(WEXITSTATUS(ret)==20){
        // exit code 20 : satisfiable
        std::cout << "0," << t << "," << std::flush;
        return 0;
    }

    // done
    return 0;
}