function retval=coherence(X_b)
% X_b is a 0/1/-1 matrix
% We check if all columns have the same bit (except -1)
[r c] = size(X_b);
for i=1 : c
  c_0 = 0;
  c_1 = 0;
  for j=1 : r
    if X_b(j,i)==0
      c_0 = c_0+1;
    endif
    if X_b(j,i)==1
      c_1 = c_1+1;
    endif
  endfor
  if c_0 != 0 && c_1 !=0
    retval=false;
    return;
  endif
endfor
retval=true;
endfunction