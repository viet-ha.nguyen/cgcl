function printTable(X, posneg, pname, p, timeout, name_xp)
% X: mean_t
% pname: a parameter (v, pv, c, pc)
% p: the vector of values in parameter
% timeout: the timeout value
% name_xp: name of the experiment (string)

% get arguments
[r c] = size(X);
% open file
fpwrite = fopen([name_xp "-table.tex"], "w");
% preamble
fprintf (fpwrite,"\\documentclass{article}\n");
fprintf (fpwrite,"\\usepackage[margin=1in]{geometry}\n\n");
fprintf (fpwrite,"\\begin{document}\n\n");
fprintf (fpwrite,["timeout of " int2str(timeout) "s\n\n"]);
% table
fprintf (fpwrite,"\\begin{tabular}{|c|c|c|c|c|c|c|c|c|}\n");
fprintf (fpwrite,"\\hline\n");
fprintf (fpwrite,"Param & \\multicolumn{7}{|c|}{Running time} & percent of\\\\\n");
fprintf (fpwrite,"\\cline{2-8}\n");
fprintf (fpwrite,[pname " & \\verb|BT| & \\verb|BTG| & \\verb|BTL| & \\verb|CCS| & \\verb|Choco| & \\verb|Minisat| & \\verb|Minisat<=1| & positive\\\\\n"]);
fprintf (fpwrite,"\\hline\n");
% data
for i = 1 : c
  fprintf (fpwrite,"%f&",p(i));
  for j = 1 : r
    if X(j,i)!=timeout
      fprintf (fpwrite,"%f",X(j,i));
    else
      fprintf (fpwrite,"-");
    endif
    if(j!=r)
      fprintf (fpwrite,"&");
    else
      fprintf (fpwrite,"&%.2f\\\\\n",posneg(i));
    endif
  endfor
endfor
% close
fprintf (fpwrite,"\\hline\n");
fprintf (fpwrite,"\\end{tabular}\n\n");
fprintf (fpwrite,"\\end{document}\n");
fflush(fpwrite);
fclose(fpwrite); 
endfunction
