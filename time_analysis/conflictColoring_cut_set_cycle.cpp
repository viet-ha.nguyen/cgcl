#include <iostream>
#include <string>
#include <csignal> 
#include "../src/datastructure.hpp"
#include "../src/import.hpp"
#include "../src/coloring.hpp"
#include "time.h" // execution time

using namespace boost;

// * parse the .gv file
// * call conflictColoring_cut_set_cycle algorithm
// * print:
//   * if coloring terminates: "b,t,"
//     with b=1 if there is a conflict coloring, b=0 otherwise, and t=the running time in seconds 
//   * if SIGINT received: "-1,-1," 

void signal_timeout(int signal_num) {
    // timeout 
    std::cout << "-1,-1," << std::flush; 
    exit(0);
} 

//  arguments:
// * path to a .gv file
int main(int argc, char *argv[]){
    // add signal handler
    signal(SIGINT, signal_timeout); 

    // read command line argument
    if(argc < 2){
        std::cout << "error: you need to provide a path to .gv file\n";
        return 0;
    }
    std::string pathFile = argv[1];

    // variables
    ConflictGraph G;
    clock_t tStart;
    double t;
    bool b;
    
    // generate a ConflictGraph from a gv file
    if (!import_conflict_graph_graphviz(pathFile,G)){
        std::cout << "error to generate G \n";
        return 0;
    }

    // measure time: begin
    tStart = clock();
    // call algorithm 
    b = conflictColoring_cut_set_cycle(G);
    // measure time: end
    t = (double)(clock() - tStart)/CLOCKS_PER_SEC;
    
    // print
    if (b){
        // coloring found
        std::cout << "1," << t << "," << std::flush;
        return 0;
    }
    // no coloring found but not timeout
    else {
        std::cout << "0, " << t << "," << std::flush;
        return 0;
    }
}