function plotData(X, pname, p, timeout, name_xp) 
% X: mean_t
% pname: a parameter (num vertices, pv, num colors, pc)
% p: the vector of values in parameter
% timeout: the timeout value
% name_xp: name of the experiment (string)

% plotting the data
fprintf(['plot data...\n']);

% Create New Figure
fig = figure; hold on;
%============== Useful variables and Setting ================
[r c] = size(X);
name_figure = [name_xp "-plot.pdf"];
%============== Plot
% Plot timeout
line ([p(1) p(end)], [timeout timeout], "linestyle", "--", "color", "b");
% Plot dots
plot(p, X(1,:),'k+','MarkerSize',7);
plot(p, X(2,:),'ro','MarkerSize',7);
plot(p, X(3,:),'g*','MarkerSize',7);
plot(p, X(4,:),'b.','MarkerSize',10);
plot(p, X(5,:),'mx','MarkerSize',7);
plot(p, X(6,:),'cs','MarkerSize',7);
plot(p, X(7,:),'cd','MarkerSize',7);
% Plot lines
plot(p, X(1,:),'k','MarkerSize',7);
plot(p, X(2,:),'r','MarkerSize',7);
plot(p, X(3,:),'g','MarkerSize',7);
plot(p, X(4,:),'b','MarkerSize',7);
plot(p, X(5,:),'m','MarkerSize',7);
plot(p, X(6,:),'c','MarkerSize',7);
plot(p, X(7,:),'c','MarkerSize',7);
% grid
grid on;
% Labels and Legend
title(name_xp);
legend({"timeout", "backtrack", "backtrack global", "backtrack local", "cut set cycle", "choco", "minisat", "minisat<=1"},"location","northwest");
ylabel("mean time");
xlabel(pname);
%============== Save
print(fig, name_figure);%, "-dpdflatexstandalone");
hold off;

endfunction
