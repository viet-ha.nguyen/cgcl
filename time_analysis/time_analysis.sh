#!/bin/bash
# argument: name of the folder containing gv files
if [ $# -lt 1 ]
then
    echo "error: one argument expected."
    exit
fi

# set the experiment time
t="1s"

# get the experiment string
xpname=$1

# prepare the .data and .csv files
rm -f ${xpname}/${xpname}-*.data ${xpname}/${xpname}.csv
echo -n "," >> ${xpname}/${xpname}-graphs.data
echo -n "conflictColoring_backtrack," >> ${xpname}/${xpname}-conflictColoring_backtrack.data
echo -n "conflictColoring_backtrack_degree_global," >> ${xpname}/${xpname}-conflictColoring_backtrack_degree_global.data
echo -n "conflictColoring_backtrack_degree_local," >> ${xpname}/${xpname}-conflictColoring_backtrack_degree_local.data
echo -n "conflictColoring_cut_set_cycle," >> ${xpname}/${xpname}-conflictColoring_cut_set_cycle.data
echo -n "conflictColoring_choco," >> ${xpname}/${xpname}-conflictColoring_choco.data
echo -n "conflictColoring_minisat," >> ${xpname}/${xpname}-conflictColoring_minisat.data
echo -n "conflictColoring_minisat-atmostone," >> ${xpname}/${xpname}-conflictColoring_minisat-atmostone.data

# run the experiments with timeouts
for file in ${xpname}/*.gv
do
    echo ${file}
    echo -n "${file},${file}," >> ${xpname}/${xpname}-graphs.data
    echo "run conflictColoring_backtrack"
    timeout -s SIGINT ${t} ./conflictColoring_backtrack.o ${file} | tee -a ${xpname}/${xpname}-conflictColoring_backtrack.data
    echo ""
    echo "run conflictColoring_backtrack_degree_global"
    timeout -s SIGINT ${t} ./conflictColoring_backtrack_degree_global.o ${file} | tee -a ${xpname}/${xpname}-conflictColoring_backtrack_degree_global.data
    echo ""
    echo "run conflictColoring_backtrack_degree_local"
    timeout -s SIGINT ${t} ./conflictColoring_backtrack_degree_local.o ${file} | tee -a ${xpname}/${xpname}-conflictColoring_backtrack_degree_local.data
    echo ""
    echo "run conflictColoring_backtrack_cut_set_cycle"
    timeout -s SIGINT ${t} ./conflictColoring_cut_set_cycle.o ${file} | tee -a ${xpname}/${xpname}-conflictColoring_cut_set_cycle.data
    echo ""
    echo "run conflictColoring_choco"
    timeout -s SIGINT ${t} ./conflictColoring_choco.o ${file} | tee -a ${xpname}/${xpname}-conflictColoring_choco.data
    echo ""
    echo "run conflictColoring_minisat"
    timeout -s SIGINT ${t} ./conflictColoring_minisat.o ${file} | tee -a ${xpname}/${xpname}-conflictColoring_minisat.data
    echo ""
    echo "run conflictColoring_minisat-atmostone"
    timeout -s SIGINT ${t} ./conflictColoring_minisat-atmostone.o ${file} | tee -a ${xpname}/${xpname}-conflictColoring_minisat-atmostone.data
    echo ""
done
echo "done, now concatenate .data to .csv"

# concatenate the files
echo "" >> ${xpname}/${xpname}-graphs.data
echo "" >> ${xpname}/${xpname}-conflictColoring_backtrack.data
echo "" >> ${xpname}/${xpname}-conflictColoring_backtrack_degree_global.data
echo "" >> ${xpname}/${xpname}-conflictColoring_backtrack_degree_local.data
echo "" >> ${xpname}/${xpname}-conflictColoring_cut_set_cycle.data
echo "" >> ${xpname}/${xpname}-conflictColoring_choco.data
echo "" >> ${xpname}/${xpname}-conflictColoring_minisat.data
echo "" >> ${xpname}/${xpname}-conflictColoring_minisat-atmostone.data
cat ${xpname}/${xpname}-graphs.data ${xpname}/${xpname}-conflictColoring_backtrack.data ${xpname}/${xpname}-conflictColoring_backtrack_degree_global.data ${xpname}/${xpname}-conflictColoring_backtrack_degree_local.data ${xpname}/${xpname}-conflictColoring_cut_set_cycle.data ${xpname}/${xpname}-conflictColoring_choco.data ${xpname}/${xpname}-conflictColoring_minisat.data ${xpname}/${xpname}-conflictColoring_minisat-atmostone.data > ${xpname}/${xpname}-results.csv 

# create data.csv for Octave
cat ${xpname}/${xpname}-results.csv | sed 's/\([^,]*\),\(.*\)/\2/' | tail -n +2 > ${xpname}/data.csv
# call octave to generate the pdf (plot and table) of results
cd ${xpname}
octave result.m
pdflatex ${xpname}-table.tex
rm *.aux *.log
cd ..
