function retval = posnegpercent(X_b,ninstances)
X_b
% X_b: the matrix of pos=1/neg=0/timeout=-1 values
% ninstances: number of instances for each parameter
% retval: a vector of percents of positive instances
[r c]=size(X_b);
b = zeros(1,c);
for i = 1:c
  if any(X_b(:,i)==0)
    b(i) = 0;
  elseif any(X_b(:,i)==1)
    b(i) = 1;
  else
    b(i) = -1;
  endif
endfor
retval = zeros(1,c/ninstances);
for i = 1:ninstances:c
  c1 = 0;
  ctotal = 0;
  for j = 1:ninstances
    if b(i+j-1)==1
      c1 += 1;
      ctotal += 1;
    elseif b(i+j-1)==0
      ctotal += 1;
    endif
  endfor
  if ctotal==0
    retval((i-1)/ninstances+1) = -1;
  else
    retval((i-1)/ninstances+1) = c1/ctotal*100;
  endif
endfor
endfunction