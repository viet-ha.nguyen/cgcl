#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream> // for std::stringstream
#include "../../src/datastructure.hpp"
#include "../../src/import.hpp"

using namespace boost;

// generate graphs with num_c in 10,15,20,25,30,35,40,45,50 (10 instances of each)
//  v = 10, pv = 0.5, pc=0.3
int main(int argc, char *argv[]){
    int v=10;
    float pv=0.5; 
    float pc=0.3;
    int c;
    // useful variables
    std::string filename = "";
    ConflictGraph G;
    int i;

    for(c=10; c <= 50; c+=5){
        std::cout << "generate 10 graphs with v=" << v << " pv=" << pv << " c=" << c << "pc=" << pc << "\n";
        for(i = 0; i < 10; i++){
            // construct filename
            filename = "xp-"+std::to_string(v)+"-0.5-"+std::to_string(c)+"-0.3-"+std::to_string(i)+".gv";
            // generate a random graph
            G.main_g.clear();
            G.conflict_g.clear();
            generate_random_conflict_graph_erdos_renyi(G, v, pv, c, pc);
            export_conflict_graph_graphviz(G, filename);
        }
    }
    return 0;
}