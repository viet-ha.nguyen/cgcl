#include <iostream>
#include <string>
#include <sstream> // for std::stringstream
#include "../src/datastructure.hpp"
#include "../src/import.hpp"
#include "../src/coloring.hpp"
#include "time.h" // execution time

using namespace boost;

int main(){
    // useful variables
    vertex_iter vi,vi_end;
    // measure time: begin
    clock_t tStart = clock();
    // generate a manual graph
    ConflictGraph G;
    // add 4 vertices and colors
    vertex_desc my_vertices[4];
    my_vertices[0] = add_conflict_vertex(3,G); // vertex 0 has 3 colors
    my_vertices[1] = add_conflict_vertex(3,G); // vertex 1 has 3 colors
    my_vertices[2] = add_conflict_vertex(3,G); // vertex 2 has 3 colors
    my_vertices[3] = add_conflict_vertex(3,G); // vertex 3 has 3 colors
        
    // print main graph
    for(tie(vi,vi_end)=vertices(G.main_g); vi != vi_end; ++vi){
        std::cout << "vertex " << *vi << " has " << G.main_g[*vi].num_colors << " colors\n" ;
    }

    // add edges to main graph and conflict edges to Cgraph
    add_conflict_edge(my_vertices[0], 0, my_vertices[1], 0, G);
    add_conflict_edge(my_vertices[0], 0, my_vertices[1], 1, G);
    add_conflict_edge(my_vertices[1], 2, my_vertices[2], 0, G);
    add_conflict_edge(my_vertices[1], 2, my_vertices[2], 1, G);
    add_conflict_edge(my_vertices[2], 2, my_vertices[3], 2, G);
    add_conflict_edge(my_vertices[2], 2, my_vertices[3], 1, G);
    add_conflict_edge(my_vertices[3], 0, my_vertices[0], 1, G);
    add_conflict_edge(my_vertices[3], 0, my_vertices[0], 2, G);
 
    // color
    if(conflictColoring_backtrack(G)){
        // found a coloring
        std::cout << "conflict-coloring\n";
        for(tie(vi,vi_end)=vertices(G.main_g); vi!=vi_end; ++vi){
            std::cout << "vertex " << *vi << " has color " << G.main_g[*vi].color << "\n";
        }
    }
    else{
        // no 3-coloring
        std::cout << "no conflict-coloring\n";
    }
    // measure time: end
    std::cout << "Time taken: " << (double)(clock() - tStart)/CLOCKS_PER_SEC <<" s\n";
    return 0;
}