#include <iostream>
#include <string>
#include <sstream> // for std::stringstream
#include "../src/datastructure.hpp"
#include "../src/import.hpp"
#include "../src/coloring.hpp"


using namespace boost;

// TODO:
// create two functions in coloring.cpp:
// 1. to prompt user to give a coloring in .color
// 2. to check the coloring in .color

bool check_coloring(ConflictGraph& G){
    vertex_iter vi, vi_end;
    vertex_desc main_u, main_v;
    vertex_desc conflict_u, conflict_v;
    edge_iter ei, ei_end;
    int color, color_u, color_v;
    // prompt user for the coloring
    for (tie(vi,vi_end) = vertices(G.main_g); vi != vi_end; vi++){
        std::cout << "enter the color of vertex " << *vi << "\n";
        std::cin >> color;
        G.main_g[*vi].color = color;
    }
    // check coloring
    for (tie(ei,ei_end) = edges(G.main_g); ei != ei_end; ei++){
        main_u = target(*ei,G.main_g);
        main_v = source(*ei,G.main_g);
        color_u = G.main_g[main_u].color;
        color_v = G.main_g[main_v].color;
        conflict_u = G.main_g[main_u].conflict_vertices[color_u];
        conflict_v = G.main_g[main_v].conflict_vertices[color_v];
        if (edge(conflict_u,conflict_v,G.conflict_g).second){
            // conflict here 
            std::cout << "there is a conflict of edge (" << main_u << "," << color_u << ")(" << main_v << "," << color_v << ")\n";
            return false;
        }
    }
    // no conflict found
    std::cout << "it is really a confict coloring ! \n";
    return true;
}

int main(){
    ConflictGraph G;
    generateCG_from_dotFile("my_graph.gv", G);
    check_coloring(G);
    return 0;
}