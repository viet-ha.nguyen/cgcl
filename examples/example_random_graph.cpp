#include <iostream>
#include <string>
#include <sstream> // for std::stringstream
#include "../src/datastructure.hpp"
#include "../src/import.hpp"
#include "../src/coloring.hpp"
#include "time.h" // execution time

using namespace boost;

// command line arguments
// * number of vertices (int)
// * probability of edges in main graph (float)
// * number of colors of each vertex (int)
// * probability of edges in conflict graph (float)
// * .gv file name
int main(int argc, char *argv[]){
    // recover arguments
    if(argc != 6){
        std::cout << "error: wrong number of arguments! expected:\n";
        std::cout << "* number of vertices (int)\n";
        std::cout << "* probability of edges in main graph (float)\n";
        std::cout << "* number of colors of each vertex (int)\n";
        std::cout << "* probability of edges in conflict graph (float)\n";
        std::cout << "* .gv file name\n";
        return 0;
    }
    int num_vertices = 0;
    std::stringstream convert1{argv[1]};
    if(!(convert1 >> num_vertices)){
        std::cout << "error: first argument expected to be int\n.";
        return 0;
    }
    float proba_main_edges = 0.0;
    std::stringstream convert2{argv[2]};
    if(!(convert2 >> proba_main_edges)){
        std::cout << "error: second argument expected to be float\n.";
        return 0;
    }
    int num_colors = 0;
    std::stringstream convert3{argv[3]};
    if(!(convert3 >> num_colors)){
        std::cout << "error: third argument expected to be int\n.";
        return 0;
    }
    float proba_conflict_edges = 0.0;
    std::stringstream convert4{argv[4]};
    if(!(convert4 >> proba_conflict_edges)){
        std::cout << "error: fourth argument expected to be float\n.";
        return 0;
    }
    std::string filename = argv[5];
    // useful variables
    vertex_iter vi,vi_end;
    // generate a random graph
    ConflictGraph G;
    int n = generate_random_conflict_graph_erdos_renyi(G, num_vertices, proba_main_edges, num_colors, proba_conflict_edges);
    // print
    std::cout << "number of edges: " << n << "\n";
    //print_conflict_graph(G);
    // export to graphviz
    export_conflict_graph_graphviz(G, filename);
    std::cout << "graph exported to " << filename << "\n";
    // measure time: begin
    clock_t tStart = clock();
    // color
    if(conflictColoring_backtrack(G)){
        // found a coloring
        std::cout << "conflict-coloring\n";
        for(tie(vi,vi_end)=vertices(G.main_g); vi!=vi_end; ++vi){
           std::cout << "vertex " << *vi << " has color " << G.main_g[*vi].color << "\n";
        }
    }
    else{
        // no conflict-coloring
        std::cout << "no conflict-coloring\n";
    }
    // measure time: end
    std::cout << "Time taken: " << (double)(clock() - tStart)/CLOCKS_PER_SEC <<" s\n";
    return 0;
}