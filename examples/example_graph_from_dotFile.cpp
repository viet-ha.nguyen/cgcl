#include <iostream>
#include <string>
#include <sstream> // for std::stringstream
#include "../src/datastructure.hpp"
#include "../src/import.hpp"
#include "../src/coloring.hpp"
#include "time.h" // execution time

using namespace boost;

// command line arguments
// * the path file of conflict graph you expect
int main(int argc, char *argv[]){
    // read command line argument
    if(argc < 2){
        std::cout << "error: you need to provide a path to .gv file\n";
        return 0;
    }
    std::string pathFile = argv[1];

    // useful variables
    vertex_iter vi,vi_end;
    clock_t tStart;

    // generate a ConflictGraph from a gv file
    ConflictGraph G;
    if (import_conflict_graph_graphviz(pathFile,G)){
        std::cout << "ConflictGraph successfully generated\n";
    }
    else{
        std::cout << "error to generate G \n";
        return 0;
    }

    //
    // BACKTRACK
    //
    std::cout << "BACKTRACK\n";
    // measure time: begin
    tStart = clock();
    // color
    if(conflictColoring_backtrack_cc(G)){
        // found a coloring
        std::cout << "conflict-coloring\n";
        for(tie(vi,vi_end)=vertices(G.main_g); vi!=vi_end; ++vi){
            std::cout << "vertex " << *vi << " has color " << G.main_g[*vi].color << "\n";
        }
    }
    else{
        // no 3-coloring
        std::cout << "no conflict-coloring\n";
    }
    // measure time: end
    std::cout << "Time taken: " << (double)(clock() - tStart)/CLOCKS_PER_SEC <<" s\n";
   
    // 
    // BACKTRACK GLOBAL
    //
    std::cout << "BACKTRACK GLOBAL\n";
    // measure time: begin
    tStart = clock();
    // color
    if(conflictColoring_backtrack_degree_global_cc(G)){
        // found a coloring
        std::cout << "conflict-coloring\n";
        for(tie(vi,vi_end)=vertices(G.main_g); vi!=vi_end; ++vi){
            std::cout << "vertex " << *vi << " has color " << G.main_g[*vi].color << "\n";
        }
    }
    else{
        // no 3-coloring
        std::cout << "no conflict-coloring\n";
    }
    // measure time: end
    std::cout << "Time taken: " << (double)(clock() - tStart)/CLOCKS_PER_SEC <<" s\n";
    
    //
    // BACKTRACK LOCAL
    //
    std::cout << "BACKTRACK LOCAL\n";
    // measure time: begin
    tStart = clock();
    // color
    if(conflictColoring_backtrack_degree_local_cc(G)){
        // found a coloring
        std::cout << "conflict-coloring\n";
        for(tie(vi,vi_end)=vertices(G.main_g); vi!=vi_end; ++vi){
            std::cout << "vertex " << *vi << " has color " << G.main_g[*vi].color << "\n";
        }
    }
    else{
        // no 3-coloring
        std::cout << "no conflict-coloring\n";
    }
    // measure time: end
    std::cout << "Time taken: " << (double)(clock() - tStart)/CLOCKS_PER_SEC <<" s\n";

    // 
    // BACKTRACK ENUMERATION
    //

    // measure time: begin
    tStart = clock();
    // color
    if(!conflictColoring_backtrack_enum(G)){
        std::cout << "enumeration done (returned false)\n";
        for(tie(vi,vi_end)=vertices(G.main_g); vi!=vi_end; ++vi){
            std::cout << "vertex " << *vi << " has color " << G.main_g[*vi].color << "\n";
        }
    }
    // measure time: end
    std::cout << "Time taken: " << (double)(clock() - tStart)/CLOCKS_PER_SEC <<" s\n";

    //export_main_graph_graphviz(G,"my_graph_main.gv");
    
    // 
    // CUT SET CYCLE
    //
    std::cout << "CUT SET CYCLE\n";
    // measure time: begin
    tStart = clock();
    if(conflictColoring_cut_set_cycle_cc(G)){
        std::cout << "conflict-coloring\n";
    }
    else{
        std::cout << "no conflict-coloring\n";
    }
    // measure time: end
    std::cout << "Time taken: " << (double)(clock() - tStart)/CLOCKS_PER_SEC <<" s\n";
    
    /*
    // TREE Polynomial time
    // measure time: begin
    clock_t tStart = clock();
    // color
    int max = conflictColoring_maxTree(G);
    // found a coloring
    std::cout << "max conflict-coloring" << max << "\n";
   
    // measure time: end
    std::cout << "Time taken: " << (double)(clock() - tStart)/CLOCKS_PER_SEC <<" s\n";
    */

    return 0;
}